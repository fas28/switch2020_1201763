public class Student {

    //Attributes
    private int number = -1;
    private String name = "";
    private int grade = -1;

    //Constructors

    public Student (int number, String name) {
        setNumber(number);
        setName(name);
    }

    public Student (int number, String name, int grade) {
        setNumber(number);
        setName(name);
        setGrade(grade);
    }

    //Getters and Setters

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public void setNumber(int number) {
        if (isValidNumber(number)) {
            this.number = number;
        } else {
            throw new IllegalArgumentException("Invalid number");
        }
    }

    public void setName(String name) {
        if (isValidName(name)) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Invalid name");
        }
    }

    public void setGrade(int grade) {
        if (isValidGrade(grade)) {
            this.grade = grade;
        } else {
            throw new IllegalArgumentException("Invalid grade");
        }

    }

    //Operations

    private boolean isValidName(String name) {
        if (name != null) {
            name = name.trim();
            if (name.length() > 2) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private boolean isValidNumber (int number) {
        if (number == Math.abs(number) && Integer.toString(number).length() == 7) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isValidGrade (int grade) {
        if (grade >= 0 && grade <= 20) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isApproved() {
        return (this.grade >= 10);
    }

    private boolean isEvaluated() {
        return (this.grade >= 0);
    }

    public boolean doEvaluation (int grade) {
        if (!isEvaluated() && isValidGrade(grade)) {
            this.grade = grade;
            return true;
        } else {
            return false;
        }
    }

    public int compareToByNumber (Student other) {

        if (this.number > other.number) {
            return 1;
        } else if (this.number < other.number) {
            return -1;
        } else {
            return 0;
        }

    }

    public int compareToByGrade (Student other) {

        if (this.grade > other.grade) {
            return 1;
        } else if (this.grade < other.grade) {
            return -1;
        } else {
            return 0;
        }

    }

    @Override //I don't know how to test different classes
    public boolean equals (Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        } else {
            Student student = (Student) o;
            return this.number == student.number;
        }
    }


}
