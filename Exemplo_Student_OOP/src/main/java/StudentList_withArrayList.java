import java.util.ArrayList;
import java.util.Comparator;

public class StudentList_withArrayList {

    //Attributes

    private ArrayList<Student> students;

    //Constructors

    public StudentList_withArrayList() {
        this.students = new ArrayList();
    }

    public StudentList_withArrayList(Student[] students) {
        if (students != null) {
            this.students = new ArrayList();
            for (Student st:students){
                this.add(st);
            }
        } else {
            throw new IllegalArgumentException("Students array should not be null");
        }
    }

    //Operations

    public void sortStudentsByNumberAsc() {
        this.students.sort(new SortByNumberAsc());
    }

    public void sortStudentsByGradeDesc() {
        this.students.sort(new SortByGradeDesc());
    }

    private class SortByNumberAsc implements Comparator<Student> {
        public int compare(Student st1, Student st2) {
            return st1.compareToByNumber(st2);
        }
    }

    private class SortByGradeDesc implements Comparator<Student> {
        public int compare(Student st1, Student st2) {
            return st1.compareToByGrade(st2)*(-1);
        }
    }

    public Student[] toArray() {
        Student[] array = new Student[this.students.size()];
        return this.students.toArray(array);
    }

    public boolean add (Student student) {
        if (student == null) {
            return false;
        }
        if (this.students.contains(student)) {
            return false;
        }
        return this.students.add(student);
    }

    public boolean remove (Student student) {
        if (student == null) {
            return false;
        }
        if (!this.students.contains(student)) {
            return false;
        }
        return this.students.remove(student);
    }

}
