public class StudentList {

    //Attributes
    private Student[] students;

    //Constructors

    public StudentList() {
        this.students = new Student[0];
    }

    ;

    public StudentList(Student[] students) {
        if (students != null) {
            this.students = copyStudentsFromArray(students, students.length);
        } else {
            throw new IllegalArgumentException("Students array should not be null");
        }
    }

    ;

    //Operations

    public void sortStudentsByNumberAsc() {

        Student temp = null;
        int i, j;
        for (i = 0; i < this.students.length; i++) {
            for (j = i + 1; j < this.students.length; j++) {
                if (this.students[i].compareToByNumber(this.students[j]) > 0) {
                    temp = this.students[i];
                    this.students[i] = this.students[j];
                    this.students[j] = temp;
                }
            }
        }

    }

    public void sortStudentsByGradeDesc() {

        Student temp = null;
        int i, j;
        for (i = 0; i < this.students.length; i++) {
            for (j = i + 1; j < this.students.length; j++) {
                if (this.students[i].compareToByGrade(this.students[j]) < 0) {
                    temp = this.students[i];
                    this.students[i] = this.students[j];
                    this.students[j] = temp;
                }
            }
        }

    }

    public Student[] toArray() {
        return this.copyStudentsFromArray(this.students, this.students.length);
    }

    private Student[] copyStudentsFromArray(Student[] students, int size) {

        Student[] copyArray = new Student[size];
        int i;

        for (i = 0; (i < size) && (i < students.length); i++) {
            copyArray[i] = students[i];
        }

        return copyArray;

    }

    private Student[] copyStudentsFromArrayWithStartPosition(Student[] students, int size, int startPosition) {

        Student[] copyArray = new Student[size];
        int i, k = 0;

        for (i = startPosition; i < students.length; i++) {
            copyArray[k] = students[i];
            k++;
        }

        return copyArray;

    }

    public boolean addStudent (Student student) {

        boolean result = false;

        if (student != null) {
            for (int i = 0; i < students.length; i++) {
                if (student.equals(students[i])) {
                    return false;
                }
            }
            this.students = copyStudentsFromArray(this.students, this.students.length+1);
            this.students[this.students.length-1] = student;
            result = true;
        }

        return result;

    }

    public boolean removeStudent (Student student) {

        boolean result = false;

        if (student != null) {
            for (int i = 0; i < students.length; i++) {
                if (students[i].equals(student)) { //it's only going to happen once
                    int index = i;
                    Student[] leftSide = copyStudentsFromArray(this.students, index);
                    Student[] rightSide = copyStudentsFromArrayWithStartPosition(this.students,this.students.length-i-1,index+1);
                    this.students = joinArray(leftSide,rightSide);

                    result = true;
                }
            }

        }

        return result;

    }

    public Student[] joinArray (Student[] array_1, Student[] array_2) {

        Student[] joinedArray = null;

        if (array_1 != null && array_2 != null) {
            joinedArray = new Student[array_1.length+array_2.length];
            int k = array_1.length;

            for (int i = 0; i < array_1.length; i++) {
                joinedArray[i] = array_1[i];
            }

            for (int j = 0; j < array_2.length; j++) {
                joinedArray[k] = array_2[j];
                k++;
            }
        }

        return joinedArray;

    }


}
