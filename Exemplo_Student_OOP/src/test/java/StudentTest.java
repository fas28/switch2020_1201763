import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    @Test
    void createValidStudent() {

        //Arrange
        int number = 1233321;
        String name = "Gustavo";
        int grade = 15;
        Student student = new Student(number,name,grade);

        //Act+Assert
        assertEquals(number,student.getNumber());
        assertEquals(name,student.getName());
        assertEquals(grade,student.getGrade());

    }

    @Test
    void createStudentInvalidShortNumber() {

        //Arrange
        int number = 526;
        String name = "António";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name);});

    }

    @Test
    void createStudentInvalidLongNumber() {

        //Arrange
        int number = 526151516;
        String name = "António";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name);});

    }

    @Test
    void createStudentInvalidNegativeNumber() {

        //Arrange
        int number = -5264111;
        String name = "António";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name);});

    }

    @Test ()
    void createStudentInvalidShortName() {

        //Arrange
        int number = 5264687;
        String name = "A";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name);});

    }

    @Test ()
    void createStudentInvalidEmptyName() {

        //Arrange
        int number = 5264687;
        String name = "";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name);});

    }

    @Test ()
    void createStudentInvalidWhiteSpacesOnlyName() {

        //Arrange
        int number = 5264687;
        String name = "     ";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name);});

    }

    @Test ()
    void createStudentInvalidNegativeGrade() {

        //Arrange
        int number = 5264687;
        String name = "Anabela";
        int grade = -1;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name,grade);});

    }

    @Test ()
    void createStudentInvalidHighGrade() {

        //Arrange
        int number = 5264687;
        String name = "Anabela";
        int grade = 21;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {new Student (number,name,grade);});

    }

    @Test
    void getNumber() {

        //Arrange
        int number = 1232214;
        String name = "Filipa";
        Student student = new Student (number,name);
        int expected = 1232214;

        //Act
        int result = student.getNumber();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getName() {

        //Arrange
        int number = 1254678;
        String name = "Sandra";
        Student student = new Student (number,name);
        String expected = "Sandra";

        //Act
        String result = student.getName();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getGrade() {

        //Arrange
        int number = 1567854;
        String name = "Ricardo";
        int grade = 17;
        Student student = new Student (number,name,grade);
        int expected = 17;

        //Act
        int result = student.getGrade();

        //Assert
        assertEquals(expected,result);

    }

    @org.junit.jupiter.api.Test
    void setNumber_IsValidNumber() {

        //Arrange
        int number = 5467887;
        String name = "Isabel";
        Student student = new Student (number, name);
        int newNumber = 5467888;

        //Act
        student.setNumber(newNumber);

        //Assert
        assertEquals(newNumber,student.getNumber());

    }

    @org.junit.jupiter.api.Test
    void setNumber_IsInvalidNumber() {

        //Arrange
        int number = 5467881;
        String name = "Igor";
        Student student = new Student (number, name);
        int newNumber = -5467881;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {student.setNumber(newNumber);});

    }

    @org.junit.jupiter.api.Test
    void setName_IsValidName() {

        //Arrange
        int number = 9586647;
        String name = "Vasco";
        Student student = new Student (number, name);
        String newName = "Pedro";

        //Act
        student.setName(newName);

        //Assert
        assertEquals(newName,student.getName());

    }

    @org.junit.jupiter.api.Test
    void setName_IsInvalidName() {

        //Arrange
        int number = 1255478;
        String name = "Luísa";
        Student student = new Student (number, name);
        String newName = "Lu";

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()-> {student.setName(newName);});

    }

    @org.junit.jupiter.api.Test
    void setGradeIsValidGrade() {

        //Arrange
        int number = 1445687;
        String name = "Gonçalo";
        int grade = 17;
        Student student = new Student (number, name, grade);
        int newGrade = 13;

        //Act
        student.setGrade(newGrade);

        //Assert
        assertEquals(newGrade,student.getGrade());

    }

    @org.junit.jupiter.api.Test
    void setGradeIsInvalidGrade() {

        //Arrange
        int number = 1745589;
        String name = "Joana";
        int grade = 17;
        Student student = new Student (number, name, grade);
        int newGrade = -14;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()->{student.setGrade(newGrade);});

    }

    @org.junit.jupiter.api.Test
    void doEvaluation_ValidEvaluation() {

        //Arrange
        int number = 1234567;
        String name = "Ana";
        Student student = new Student(number,name);
        int grade = 13;

        //Act
        boolean result = student.doEvaluation(grade);
        int resultGrade = student.getGrade();

        //Assert
        assertTrue(result);
        assertEquals(grade,resultGrade);

    }

    @org.junit.jupiter.api.Test
    void doEvaluation_InvalidEvaluationDueToInvalidGrade() {

        //Arrange
        int number = 1234577;
        String name = "Catarina";
        Student student = new Student(number,name);
        int grade = 23;

        //Act
        boolean result = student.doEvaluation(grade);

        //Assert
        assertFalse(result);

    }

    @org.junit.jupiter.api.Test
    void doEvaluation_InvalidEvaluationDueToExistingGrade() {

        //Arrange
        int number = 1554898;
        String name = "Carolina";
        int grade = 17;
        Student student = new Student(number,name,grade);
        int newGrade = 18;

        //Act
        boolean result = student.doEvaluation(newGrade);

        //Assert
        assertFalse(result);

    }

    @Test
    void compareToByNumberFirstHigherThanSecond() {

        //Arrange
        Student student = new Student(1232232,"Filipe Sousa");
        Student otherStudent = new Student(1223320,"Sara Reis");
        int expected = 1;

        //Act
        int result = student.compareToByNumber(otherStudent);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void compareToByNumberFirstLowerThanSecond() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa");
        Student otherStudent = new Student(1232232,"Sara Reis");
        int expected = -1;

        //Act
        int result = student.compareToByNumber(otherStudent);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void compareToByNumberFirstEqualsSecond() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa");
        Student otherStudent = new Student(1223320,"Sara Reis");
        int expected = 0;

        //Act
        int result = student.compareToByNumber(otherStudent);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void compareToByGradeFirstHigherThanSecond() {

        //Arrange
        Student student = new Student(1232232,"Filipe Sousa",16);
        Student otherStudent = new Student(1223320,"Sara Reis",15);
        int expected = 1;

        //Act
        int result = student.compareToByGrade(otherStudent);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void compareToByGradeFirstLowerThanSecond() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa",14);
        Student otherStudent = new Student(1232232,"Sara Reis",19);
        int expected = -1;

        //Act
        int result = student.compareToByGrade(otherStudent);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void compareToByGradeFirstEqualsSecond() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa",14);
        Student otherStudent = new Student(1223320,"Sara Reis",14);
        int expected = 0;

        //Act
        int result = student.compareToByGrade(otherStudent);

        //Assert
        assertEquals(expected,result);

    }


    @Test
    void testEquals_Null() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa",14);
        Object o = null;

        //Act
        boolean result = student.equals(o);

        //Assert
        assertFalse(result);

    }

    @Test
    void testEquals_SameObject() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa",14);
        Object o = student;

        //Act
        boolean result = student.equals(o);

        //Assert
        assertTrue(result);

    }
    @Test
    void testEquals_DifferentNumbers() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa",14);
        Object o = new Student(1223321,"Sara Reis",14);

        //Act
        boolean result = student.equals(o);

        //Assert
        assertFalse(result);

    }

    @Test
    void testEquals_SameNumbers() {

        //Arrange
        Student student = new Student(1223320,"Filipe Sousa",14);
        Object o = new Student(1223320,"Sara Reis",14);

        //Act
        boolean result = student.equals(o);

        //Assert
        assertTrue(result);

    }

}