import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentList_withArrayListTest {

    @Test
    void createEmptyStudentList() {

        //Arrange
        StudentList_withArrayList studentList = new StudentList_withArrayList();

        //Act
        Student[] result = studentList.toArray();

        //Assert
        assertEquals(0,result.length);

    }

    @Test
    void createValidStudentList() {

        //Arrange
        Student student_1 = new Student(1123212,"Manuel Silva",14);
        Student student_2 = new Student(1123343,"Jacinta Maia",18);
        Student student_3 = new Student(1122325,"João Nogueira",19);
        Student[] students = {student_1,student_2,student_3};
        Student[] expected = {student_1,student_2,student_3};

        //Act
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);
        assertNotEquals(students,result);

    }

    @Test
    void createValidStudentListArrayChanges() {

        //Arrange
        Student student_1 = new Student(1123212,"Manuel Silva",14);
        Student student_2 = new Student(1123343,"Jacinta Maia",18);
        Student student_3 = new Student(1122325,"João Nogueira",19);
        Student[] students = {student_1,student_2,student_3};
        Student[] expected = {student_1,student_2,student_3};

        //Act
        StudentList studentList = new StudentList(students);
        students[0] = student_3;
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);
        assertNotEquals(students,result);

    }

    @Test
    void createNullStudentList() {

        //Arrange
        Student[] students = null;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()->{new StudentList_withArrayList(students);});

    }

    @Test
    void sortStudentsByNumberAsc_EmptyVector() {

        //Arrange
        Student[] students = {};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {};

        //Act
        studentList.sortStudentsByNumberAsc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByNumberAsc_OneStudent() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);

        Student[] students = {student_1};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_1};

        //Act
        studentList.sortStudentsByNumberAsc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByNumberAsc_TwoOrderedStudents() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1556328,"João",12);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_1,student_2};

        //Act
        studentList.sortStudentsByNumberAsc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByNumberAsc_TwoUnorderedStudents() {

        //Arrange
        Student student_1 = new Student(1556328,"Filipa",16);
        Student student_2 = new Student(1233256,"João",12);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_2,student_1};

        //Act
        studentList.sortStudentsByNumberAsc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByNumberAsc_SeveralUnorderedStudents() {

        //Arrange
        Student student_1 = new Student(5469987,"Filipa",16);
        Student student_2 = new Student(1233256,"João",12);
        Student student_3 = new Student(5598741,"Rodrigo",17);
        Student student_4 = new Student(1556328,"Luísa",15);

        Student[] students = {student_1,student_2,student_3,student_4};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_2,student_4,student_1,student_3};

        //Act
        studentList.sortStudentsByNumberAsc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByGradeDesc_EmptyVector() {

        //Arrange
        Student[] students = {};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {};

        //Act
        studentList.sortStudentsByGradeDesc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByGradeDesc_OneStudent() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);

        Student[] students = {student_1};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_1};

        //Act
        studentList.sortStudentsByGradeDesc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByGradeDesc_TwoOrderedStudents() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1556328,"João",12);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_1,student_2};

        //Act
        studentList.sortStudentsByGradeDesc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByGradeDesc_TwoUnorderedStudents() {

        //Arrange
        Student student_1 = new Student(1556328,"Filipa",12);
        Student student_2 = new Student(1233256,"João",16);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_2,student_1};

        //Act
        studentList.sortStudentsByGradeDesc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void sortStudentsByGradeDesc_SeveralUnorderedStudents() {

        //Arrange
        Student student_1 = new Student(5469987,"Filipa",16);
        Student student_2 = new Student(1233256,"João",12);
        Student student_3 = new Student(5598741,"Rodrigo",17);
        Student student_4 = new Student(1556328,"Luísa",15);

        Student[] students = {student_1,student_2,student_3,student_4};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student[] expected = {student_3,student_1,student_4,student_2};

        //Act
        studentList.sortStudentsByGradeDesc();
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void toArray_OneElement() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);

        Student[] students = {student_1};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student[] expected = {student_1};

        //Act
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);
        assertNotSame(students,result);

    }

    @Test
    void toArray_TwoElements() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1243256,"João",12);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student[] expected = {student_1,student_2};

        //Act
        Student[] result = studentList.toArray();

        //Assert
        assertArrayEquals(expected,result);
        assertNotSame(students,result);

    }

    @Test
    void addStudent_Null() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student[] students = {student_1};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);
        Student student_2 = null;

        //Act
        boolean result = studentList.add(student_2);

        //Assert
        assertFalse(result);
        assertEquals(1,studentList.toArray().length);


    }

    @Test
    void addStudent_NumberAlreadyExists() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1243256,"João",12);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student student_3 = new Student(1243256,"José",16);

        //Act
        boolean result = studentList.add(student_3);

        //Assert
        assertFalse(result);
        assertEquals(2,studentList.toArray().length);

    }

    @Test
    void addStudent_ReturnsTrue() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1243256,"João",12);

        Student[] students = {student_1,student_2};
        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student student_3 = new Student(1243253,"José",16);

        Student[] expectedStudents = {student_1,student_2,student_3};
        StudentList_withArrayList expectedStudentList = new StudentList_withArrayList(expectedStudents);
        Student[] expectedArray = expectedStudentList.toArray();

        //Act
        boolean result = studentList.add(student_3);
        Student[] resultArray = studentList.toArray();

        //Assert
        assertTrue(result);
        assertEquals(3,studentList.toArray().length);
        assertArrayEquals(expectedArray,resultArray);

    }

    @Test
    void removeStudent_NullStudent() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1243256,"João",12);
        Student student_3 = new Student(5598741,"Rodrigo",17);
        Student student_4 = new Student(1556328,"Luísa",15);
        Student[] students = {student_1,student_2,student_3,student_4};

        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student student = null;

        //Act
        boolean result = studentList.remove(student);

        //Assert
        assertFalse(result);
        assertEquals(4,studentList.toArray().length);

    }

    @Test
    void removeStudent_StudentDoesNotExist() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1243256,"João",12);
        Student student_3 = new Student(5598741,"Rodrigo",17);
        Student student_4 = new Student(1556328,"Luísa",15);
        Student[] students = {student_1,student_2,student_3,student_4};

        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student student = new Student(1222212,"Daniel",16);

        //Act
        boolean result = studentList.remove(student);

        //Assert
        assertFalse(result);
        assertEquals(4,studentList.toArray().length);

    }

    @Test
    void removeStudent_StudentExists() {

        //Arrange
        Student student_1 = new Student(1233256,"Filipa",16);
        Student student_2 = new Student(1243256,"João",12);
        Student student_3 = new Student(5598741,"Rodrigo",17);
        Student student_4 = new Student(1556328,"Luísa",15);
        Student[] students = {student_1,student_2,student_3,student_4};

        StudentList_withArrayList studentList = new StudentList_withArrayList(students);

        Student[] expected = {student_1,student_3,student_4};

        //Act
        boolean result = studentList.remove(student_2);
        Student[] content = studentList.toArray();

        //Assert
        assertTrue(result);
        assertEquals(3,content.length);
        assertArrayEquals(expected,content);

    }

}