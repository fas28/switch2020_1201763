import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_4Test {

    @Test
    void getArrayOfEvens_PositiveIntegersArray() {

        //Arrange
        int[] array = {1,2,3,4,5,6};
        int[] expected = {2,4,6};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfEvens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayOfEvens_PositiveIntegersWithZeroArray() {

        //Arrange
        int[] array = {5,0,0,6,3,8};
        int[] expected = {6,8};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfEvens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayOfEvens_PositiveAndNegativeIntegersWithZero() {

        //Arrange
        int[] array = {6,-4,5,0,2,-5,0};
        int[] expected = {6,-4,2};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfEvens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayOfEvens_Zero() {

        //Arrange
        int[] array = {0};
        int[] expected = {0};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfEvens(array);

        //Assert
        assertArrayEquals(expected,result);

    }


    @Test
    void getArrayOfUnevens_PositiveIntegersArray() {

        //Arrange
        int[] array = {1,2,3,4,5,6};
        int[] expected = {1,3,5};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfUnevens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayOfUnevens_PositiveIntegersWithZeroArray() {

        //Arrange
        int[] array = {5,0,0,6,3,8};
        int[] expected = {5,3};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfUnevens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayOfUnevens_PositiveAndNegativeIntegersWithZero() {

        //Arrange
        int[] array = {6,-4,5,0,-33,5,0};
        int[] expected = {5,-33,5};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfUnevens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayOfUnevens_Zero() {

        //Arrange
        int[] array = {0};
        int[] expected = {0};

        //Act
        int[] result = Bloco_4_ex_4.getArrayOfUnevens(array);

        //Assert
        assertArrayEquals(expected,result);

    }

}