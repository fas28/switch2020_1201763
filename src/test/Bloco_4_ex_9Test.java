import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_9Test {

    @Test
    void isPalindrome_CheckIfTrue() {

        //Act
        boolean result = Bloco_4_ex_9.isPalindrome(131);

        //Assert
        assertTrue(result);

    }

    @Test
    void isPalindrome_CheckIfFalse() {

        //Act
        boolean result = Bloco_4_ex_9.isPalindrome(51351);

        //Assert
        assertFalse(result);

    }

    @Test
    void isPalindrome_Negative() {

        //Act
        boolean result = Bloco_4_ex_9.isPalindrome(-535);

        //Assert
        assertFalse(result);

    }

    @Test
    void isPalindrome_Zero() {

        //Act
        boolean result = Bloco_4_ex_9.isPalindrome(0);

        //Assert
        assertFalse(result);

    }

    @Test
    void getReverseArray_GeneralUse() {

        //Arrange
        int[] array = {0,0,2,3,-1};
        int[] expected = {-1,3,2,0,0};

        //Act
        int[] result = Bloco_4_ex_9.getReverseArray(array);

        //Assert
        assertArrayEquals(expected,result);

    }
}