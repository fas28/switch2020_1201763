import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_2Test {

    @Test
    void getArrayFromPositiveInteger_PositiveIntegerWithoutZeros() {

        //Arrange
        int[] expected = {1,2,3,4,5,6};

        //Act
        int[] result = Bloco_4_ex_2.getArrayFromPositiveInteger(123456);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayFromPositiveInteger_PositiveIntegerWithZeros() {

        //Arrange
        int[] expected = {1,0,0,0,5,9};

        //Act
        int[] result = Bloco_4_ex_2.getArrayFromPositiveInteger(100059);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayFromPositiveInteger_NegativeInteger() {

        //Arrange
        int[] expected = {0};

        //Act
        int[] result = Bloco_4_ex_2.getArrayFromPositiveInteger(-98762);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getArrayFromPositiveInteger_Zero() {

        //Arrange
        int[] expected = {0};

        //Act
        int[] result = Bloco_4_ex_2.getArrayFromPositiveInteger(0);

        //Assert
        assertArrayEquals(expected,result);

    }
}