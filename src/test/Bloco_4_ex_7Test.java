import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_7Test {

    @Test
    void exercise_VII_PositiveOrderedLimits() {

        //Arrange
        int[] expected = {6,9};

        //Act
        int[] result = Bloco_4_ex_7.exercise_VII(4,10);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void exercise_VII_PositiveUnorderedLimits() {

        //Arrange
        int[] expected = {3,6,9,12};

        //Act
        int[] result = Bloco_4_ex_7.exercise_VII(12,0);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void exercise_VII_NegativeLimits() {

        //Act
        int[] result = Bloco_4_ex_7.exercise_VII(-2,-25);

        //Assert
        assertNull(result);

    }

    @Test
    void getMultiples_PositiveLimitsPositiveN() {

        //Arrange
        int[] expected = {4,6,8,10};

        //Act
        int[] result = Bloco_4_ex_7.getMultiples(4,10,2);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getMultiples_UnorderedLimitsPositiveN() {

        //Arrange
        int[] expected = {5,10};

        //Act
        int[] result = Bloco_4_ex_7.getMultiples(12,0,5);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getMultiples_NegativeLimitPositiveN() {

        //Act
        int[] result = Bloco_4_ex_7.getMultiples(-2,25,7);

        //Assert
        assertNull(result);

    }

    @Test
    void getMultiples_NegativeLimitPositiveN_2() {

        //Act
        int[] result = Bloco_4_ex_7.getMultiples(3,-10,1);

        //Assert
       assertNull(result);

    }

    @Test
    void getMultiples_NegativeN() {

       //Act
        int[] result = Bloco_4_ex_7.getMultiples(8,0,-9);

        //Assert
        assertNull(result);

    }

    @Test
    void get_Multiples_NoMultiples() {

        //Act
        int[] result = Bloco_4_ex_7.getMultiples(1,3,5);

        //Assert
        assertNull(result);

    }

}