import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_16Test {

    @Test
    void getDeterminantOfMatrix_3X3() {

        //Arrange
        int[][] matrix = {{1,2,3},
                          {4,5,6},
                          {7,8,9}};
        Double expected = 0.0;

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOfMatrix(matrix);

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getDeterminantOfMatrix_4X4() {

        //Arrange
        int[][] matrix = {{1,2,3,4},
                          {-5,9,2,6},
                          {5,3,5,3},
                          {2,0,2,0}};
        Double expected = 120.0;

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOfMatrix(matrix);

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getDeterminantOfMatrix_5X5() {

        //Arrange
        int[][] matrix = {{1,2,3,4,8},
                          {1,4,-2,3,10},
                          {-5,9,2,6,1},
                          {5,3,5,3,4},
                          {2,0,2,0,1}};
        Double expected = -1500.0;

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOfMatrix(matrix);

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getDeterminantOfMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOfMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getDeterminantOfMatrix_NonSquareMatrix() {

        //Arrange
        int[][] matrix = {{1,2,3},{4,5,6}};

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOfMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getFirstRowOfMatrix_3X3() {

        //Arrange
        int[][] matrix = {{1,2,3},
                          {4,5,6},
                          {7,8,9}};
        int[] expected = {1,2,3};

        //Act
        int[] result = Bloco_4_ex_16.getFirstRowOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getFirstRowOfMatrix_4X4() {

        //Arrange
        int[][] matrix = {{1,2,3,4},
                          {-5,9,2,6},
                          {5,3,5,3},
                          {2,0,2,0}};
        int[] expected = {1,2,3,4};

        //Act
        int[] result = Bloco_4_ex_16.getFirstRowOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getFirstRowOfMatrix_5X5() {

        //Arrange
        int[][] matrix = {{1,2,3,4,8},
                          {1,4,-2,3,10},
                          {-5,9,2,6,1},
                          {5,3,5,3,4},
                          {2,0,2,0,1}};
        int[] expected = {1,2,3,4,8};

        //Act
        int[] result = Bloco_4_ex_16.getFirstRowOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getFirstRowOfMatrix_Null() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_16.getFirstRowOfMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void reduceMatrix_3X3() {

        //Arrange
        int[][] matrix = {{1,2,3},
                          {4,5,6},
                          {7,8,9}};
        int iPosition = 0;
        int jPosition = 0;
        int[][] expected = {{5,6},
                            {8,9}};

        //Act
        int[][] result = Bloco_4_ex_16.reduceMatrix(matrix, iPosition, jPosition);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void reduceMatrix_4X4() {

        //Arrange
        int[][] matrix = {{1,2,3,4},
                          {5,6,7,8},
                          {9,10,11,12},
                          {13,14,15,16}};
        int iPosition = 1;
        int jPosition = 1;
        int[][] expected = {{1,3,4},
                            {9,11,12},
                            {13,15,16}};

        //Act
        int[][] result = Bloco_4_ex_16.reduceMatrix(matrix, iPosition, jPosition);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void reduceMatrix_5X5() {

        //Arrange
        int[][] matrix = {{1,2,3,4,8},
                          {1,4,-2,3,10},
                          {-5,9,2,6,1},
                          {5,3,5,3,4},
                          {2,0,2,0,1}};
        int iPosition = 2;
        int jPosition = 2;
        int[][] expected = {{1,2,4,8},{1,4,3,10},{5,3,3,4},{2,0,0,1}};

        //Act
        int[][] result = Bloco_4_ex_16.reduceMatrix(matrix, iPosition, jPosition);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void reduceMatrix_ReturnsNull() {

        //Arrange
        int[][] matrix = {{1,2,3,4,8},
                {1,4,-2,3,10},
                {-5,9,2,6,1},
                {5,3,5,3,4},
                {2,0,2,0,1}};
        int iPosition = 0;
        int jPosition = 5;

        //Act
        int[][] result = Bloco_4_ex_16.reduceMatrix(matrix, iPosition, jPosition);

        //Assert
        assertNull(result);

    }

    @Test
    void getDeterminantOf2x2Matrix() {

        //Arrange
        int[][] matrix = {{1,2},{0,-3}};
        Double expected = -3.0;

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOf2x2Matrix(matrix);

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getDeterminantOf2x2Matrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOf2x2Matrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getDeterminantOf2x2Matrix_Non2X2Matrix() {

        //Arrange
        int[][] matrix = {{1,2,3,4},
                          {-5,9,2,6},
                          {5,3,5,3},
                          {2,0,2,0}};

        //Act
        Double result = Bloco_4_ex_16.getDeterminantOf2x2Matrix(matrix);

        //Assert
        assertNull(result);

    }

}