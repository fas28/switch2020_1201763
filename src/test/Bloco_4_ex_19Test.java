import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_19Test {


    @Test
    void checkIfThereAreBlankCells_ReturnsTrue() {

        //Arrange
        int[][] matrix = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        //Act
        boolean result = Bloco_4_ex_19.checkIfThereAreBlankCells(matrix);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfThereAreBlankCells_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        boolean result = Bloco_4_ex_19.checkIfThereAreBlankCells(matrix);

        //Assert
        assertFalse(result);

    }

    @Test
    void getMaskMatrix() {

        //Arrange
        int[][] matrix = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int number = 3;
        int[][] expected = {{0, 1, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0}};

        //Act
        int[][] result = Bloco_4_ex_19.getMaskMatrix(matrix, number);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void getMaskMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;
        int number = 5;


        //Act
        int[][] result = Bloco_4_ex_19.getMaskMatrix(matrix, number);

        //Assert
        assertNull(result);

    }

    @Test
    void getMaskMatrix_WrongNumber() {

        //Arrange
        int[][] matrix = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                {6, 0, 0, 1, 9, 5, 0, 0, 0},
                {0, 9, 8, 0, 0, 0, 0, 6, 0},
                {8, 0, 0, 0, 6, 0, 0, 0, 3},
                {4, 0, 0, 8, 0, 3, 0, 0, 1},
                {7, 0, 0, 0, 2, 0, 0, 0, 6},
                {0, 6, 0, 0, 0, 0, 2, 8, 0},
                {0, 0, 0, 4, 1, 9, 0, 0, 5},
                {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int number = 0;


        //Act
        int[][] result = Bloco_4_ex_19.getMaskMatrix(matrix, number);

        //Assert
        assertNull(result);

    }

    @Test
    void insertNumberInMatrix() {

        //Arrange
        int[][] matrix = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                         {6, 0, 0, 1, 9, 5, 0, 0, 0},
                         {0, 9, 8, 0, 0, 0, 0, 6, 0},
                         {8, 0, 0, 0, 6, 0, 0, 0, 3},
                         {4, 0, 0, 8, 0, 3, 0, 0, 1},
                         {7, 0, 0, 0, 2, 0, 0, 0, 6},
                         {0, 6, 0, 0, 0, 0, 2, 8, 0},
                         {0, 0, 0, 4, 1, 9, 0, 0, 5},
                         {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int number = 6;
        int iPosition = 7;
        int jPosition = 1;
        int[][] expected = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                           {6, 0, 0, 1, 9, 5, 0, 0, 0},
                           {0, 9, 8, 0, 0, 0, 0, 6, 0},
                           {8, 0, 0, 0, 6, 0, 0, 0, 3},
                           {4, 0, 0, 8, 0, 3, 0, 0, 1},
                           {7, 0, 0, 0, 2, 0, 0, 0, 6},
                           {0, 6, 0, 0, 0, 0, 2, 8, 0},
                           {0, 6, 0, 4, 1, 9, 0, 0, 5},
                           {0, 0, 0, 0, 8, 0, 0, 7, 9}};

        //Act
        int[][] result = Bloco_4_ex_19.insertNumberInMatrix(matrix,number,iPosition,jPosition);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void insertNumberInMatrix_NotABlankCell() {

        //Arrange
        int[][] matrix = {{5, 3, 0, 0, 7, 0, 0, 0, 0},
                         {6, 0, 0, 1, 9, 5, 0, 0, 0},
                         {0, 9, 8, 0, 0, 0, 0, 6, 0},
                         {8, 0, 0, 0, 6, 0, 0, 0, 3},
                         {4, 0, 0, 8, 0, 3, 0, 0, 1},
                         {7, 0, 0, 0, 2, 0, 0, 0, 6},
                         {0, 6, 0, 0, 0, 0, 2, 8, 0},
                         {0, 0, 0, 4, 1, 9, 0, 0, 5},
                         {0, 0, 0, 0, 8, 0, 0, 7, 9}};
        int number = 6;
        int iPosition = 0;
        int jPosition = 4;

        //Act
        int[][] result = Bloco_4_ex_19.insertNumberInMatrix(matrix,number,iPosition,jPosition);

        //Assert
        assertNull(result);

    }
}