import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_1Test {

    @Test
    void getNumberOfDigits_OneDigitPositiveInteger() {

        //Arrange
        int expected = 1;

        //Act
        int result = Bloco_4_ex_1.getNumberOfDigits(1);

        //Assert
        assertEquals(expected,result);
    }

    @Test
    void getNumberOfDigits_MultipleDigitsWith0PositiveInteger() {

        int expected = 4;
        int result = Bloco_4_ex_1.getNumberOfDigits(5000);
        assertEquals(expected,result);
    }

    @Test
    void getNumberOfDigits_MultipleDigitsWithout0PositiveInteger() {

        int expected = 7;
        int result = Bloco_4_ex_1.getNumberOfDigits(1168920);
        assertEquals(expected,result);
    }


    @Test
    void getNumberOfDigits_NegativeInteger() {

        int expected = -1;
        int result = Bloco_4_ex_1.getNumberOfDigits(-50);
        assertEquals(expected,result);
    }

    @Test
    void getNumberOfDigits_Zero() {

        int expected = -1;
        int result = Bloco_4_ex_1.getNumberOfDigits(0);
        assertEquals(expected,result);
    }

}