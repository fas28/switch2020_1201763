import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_1Test {

    @Test
    void exercicio_I_Processamento() {

        //Arrange
        double[] expected = {40.0, 60.0};

        //Act
        double[] result = Bloco_1.exercicio_I_Processamento(12, 18);

        //Assert
        assertArrayEquals(expected, result, 0.01);
    }

    @Test
    void exercicio_II_Processamento() {

        //Arrange
        double expected = 42.5;

        //Act
        double result = Bloco_1.exercicio_II_Processamento(5, 10, 2.5, 3);

        //Assert
        assertEquals(expected, result, 0.1);
    }

    @Test
    void exercicio_III_Processamento() {

        //Arrange
        double expected = 50265.48;

        //Act
        double result = Bloco_1.exercicio_III_Processamento(2, 4);

        //Assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_IV_Processamento() {

        //Arrange
        double segundos = 100;
        double expected = 34000;

        //Act
        double result = Bloco_1.exercicio_IV_Processamento(segundos);

        //Assert
        assertEquals(expected, result, 0.00001);

    }

    @Test
    void exercicio_V_Processamento() {

        //Arrange
        int segundosPedra = 10;
        double vInicial = 0;
        final double GRAVIDADE = 9.8;
        double expected = 490.0;

        //Act
        double result = Bloco_1.exercicio_V_Processamento(segundosPedra, vInicial, GRAVIDADE);

        //Assert
        assertEquals(expected, result, 0.1);

    }

    @Test
    void exercicio_VI_Processamento() {

        //Arrange
        double expected = 5.04;

        //Act
        double result = Bloco_1.exercicio_VI_Processamento(1.68, 6, 2);

        //Assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_VII_Processamento() {

        //Arrange
        double distManel = 42195;
        int horasManel = 4;
        int minManel = 2;
        int segManel = 10;
        int horasZe = 1;
        int minZe = 5;
        int segZe = 0;
        double expected = 11.32556;

        //Act
        double result = Bloco_1.exercicio_VII_Processamento(distManel, horasManel, minManel, segManel, horasZe, minZe, segZe);

        //Assert
        assertEquals(expected, result, 0.00001);

    }

    @Test
    void exercicio_VIII_Processamento() {

        //Arrange
        double compAC = 40;
        double compCB = 60;
        double angulo = 60;
        double expected = 52.91503;

        //Act
        double result = Bloco_1.exercicio_VIII_Processamento(compAC, compCB, angulo);

        //Assert
        assertEquals(expected, result, 0.00001);

    }

    @Test
    void exercicio_IX_Processamento() {

        //Arrange
        double expected = 28.0;

        //Act
        double result = Bloco_1.exercicio_IX_Processamento(5.5, 8.5);

        //Assert
        assertEquals(expected, result, 0.1);

    }

    @Test
    void exercicio_X_Processamento() {

        //Arrange
        double expected = 3.354;

        //Act
        double result = Bloco_1.exercicio_X_Processamento(1.5, 3);

        //Assert
        assertEquals(expected, result, 0.001);

    }

    @Test
    void exercicio_XI_Processamento() {

        //Arrange
        double[] expected = {2.618, 0.382};

        //Act
        double[] result = Bloco_1.exercicio_XI_Processamento(1, -3, 1);

        //Assert
        assertArrayEquals(expected, result, 0.001);

    }

    @Test
    void exercicio_XII_Processamento() {

        //Arrange
        double expected = 89.6;

        //Act
        double result = Bloco_1.exercicio_XII_Processamento(32);

        //Assert
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_XIII_Processamento() {

        //Arrange
        int expected = 963;

        //Act
        int result = Bloco_1.exercicio_XIII_Processamento(16, 03);

        //Assert
        assertEquals(expected, result);

    }

}