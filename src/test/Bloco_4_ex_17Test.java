import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_17Test {

    @Test
    void getProductOfMatrixAndScalar() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{0,2,3,2},{1,2,3,4}};
        Integer number = 3;
        int[][] expected = {{3,6,9,12},{0,6,9,6},{3,6,9,12}};

        //Act
        int[][] result = Bloco_4_ex_17.getProductOfMatrixAndScalar_IntMatrixAndScalar(matrix,number);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getProductOfMatrixAndScalar_NullMatrix() {

        //Arrange
        int[][] matrix = null;
        Integer number = 3;

        //Act
        int[][] result = Bloco_4_ex_17.getProductOfMatrixAndScalar_IntMatrixAndScalar(matrix,number);

        //Assert
        assertNull(result);

    }

    @Test
    void getProductOfMatrixAndScalar_NullNumber() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{0,2,3,2},{1,2,3,4}};
        Integer number = null;

        //Act
        int[][] result = Bloco_4_ex_17.getProductOfMatrixAndScalar_IntMatrixAndScalar(matrix,number);

        //Assert
        assertNull(result);

    }

    @Test
    void checkIfMatrixesHaveSameDimensions_ReturnsTrue() {

        //Arrange
        int[][] matrix_1 = {{1,2,3},{4,5,6}};
        int[][] matrix_2 = {{0,0,1},{2,4,-2}};

        //Act
        boolean result = Bloco_4_ex_17.checkIfMatrixesHaveSameDimensions(matrix_1,matrix_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfMatrixesHaveSameDimensions_ReturnsFalse() {

        //Arrange
        int[][] matrix_1 = {{1,2,3},{4,5,6}};
        int[][] matrix_2 = {{0,0,1},{2,4,-2},{1,2,3}};

        //Act
        boolean result = Bloco_4_ex_17.checkIfMatrixesHaveSameDimensions(matrix_1,matrix_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfMatrixesHaveSameDimensions_MatrixIsNull() {

        //Arrange
        int[][] matrix_1 = {{1,2,3},{4,5,6}};
        int[][] matrix_2 = null;

        //Act
        boolean result = Bloco_4_ex_17.checkIfMatrixesHaveSameDimensions(matrix_1,matrix_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void getSumOfMatrixes_GeneralUse() {

        //Arrange
        int[][] matrix_1 = {{1,2,3},
                            {4,5,6},
                            {0,-3,-4}};
        int[][] matrix_2 = {{0,0,1},
                            {2,4,-2},
                            {1,2,3}};
        int[][] expected = {{1,2,4},
                            {6,9,4},
                            {1,-1,-1}};

        //Act
        int[][] result = Bloco_4_ex_17.getSumOfMatrixes(matrix_1,matrix_2);

        //Arrange
        assertArrayEquals(expected,result);

    }

    @Test
    void getSumOfMatrixes_NullMatrix() {

        //Arrange
        int[][] matrix_1 = {{1,2,3},
                {4,5,6},
                {0,-3,-4}};
        int[][] matrix_2 = null;

        //Act
        int[][] result = Bloco_4_ex_17.getSumOfMatrixes(matrix_1,matrix_2);

        //Arrange
        assertNull(result);

    }

    @Test
    void getProductOfMatrixes_2X2For2X4() {

        //Arrange
        int[][] matrix_1 = {{1,2},{3,4}};
        int[][] matrix_2 = {{1,2,3,4},{5,6,7,8}};
        int[][] expected = {{11,14,17,20},{23,30,37,44}};

        //Act
        int[][] result = Bloco_4_ex_17.getProductOfMatrixes(matrix_1,matrix_2);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getProductOfMatrixes_1X3For3X2() {

        //Arrange
        int[][] matrix_1 = {{1,2,0}};
        int[][] matrix_2 = {{1,2},{-52,2},{0,12}};
        int[][] expected = {{-103,6}};

        //Act
        int[][] result = Bloco_4_ex_17.getProductOfMatrixes(matrix_1,matrix_2);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getProductOfMatrixes_NullMatrixes() {

        //Arrange
        int[][] matrix_1 = null;
        int[][] matrix_2 = null;

        //Act
        int[][] result = Bloco_4_ex_17.getProductOfMatrixes(matrix_1,matrix_2);

        //Assert
        assertNull(result);

    }

    @Test
    void checkIfMatrixesCanBeMultiplied_ReturnsTrue() {

        //Arrange
        int[][] matrix_1 = {{1,2},{3,4}};
        int[][] matrix_2 = {{1,2,3,4},{5,6,7,8}};

        //Act
        boolean result = Bloco_4_ex_17.checkIfMatrixesCanBeMultiplied(matrix_1,matrix_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfMatrixesCanBeMultiplied_ReturnsFalse() {

        //Arrange
        int[][] matrix_1 = {{1,2},{3,4}};
        int[][] matrix_2 = {{1,2,3,4},{5,6,7,8},{0,-7,4,5}};

        //Act
        boolean result = Bloco_4_ex_17.checkIfMatrixesCanBeMultiplied(matrix_1,matrix_2);

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfMatrixesCanBeMultiplied_NullMatrix() {

        //Arrange
        int[][] matrix_1 = {{1,2},{3,4}};
        int[][] matrix_2 = null;

        //Act
        boolean result = Bloco_4_ex_17.checkIfMatrixesCanBeMultiplied(matrix_1,matrix_2);

        //Assert
        assertFalse(result);

    }

}