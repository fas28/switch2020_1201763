import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_3_ex_20Test {

    @Test
    void exercise_XX_Run_Test1() {

        String expected = "perfeito";
        String result = Bloco_3_ex_20.exercise_XX_Run(6);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XX_Run_Test2() {

        String expected = "abundante";
        String result = Bloco_3_ex_20.exercise_XX_Run(12);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XX_Run_Test3() {

        String expected = "reduzido";
        String result = Bloco_3_ex_20.exercise_XX_Run(9);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XX_Run_Test4() {

        String expected = "reduzido";
        String result = Bloco_3_ex_20.exercise_XX_Run(5);
        assertEquals(expected,result);

    }

}