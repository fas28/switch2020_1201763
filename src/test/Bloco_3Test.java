import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_3Test {

    @Test
    void exercicio_I_Processamento_Teste1() {

        int expected = 120;
        int result = Bloco_3.exercicio_I_Processamento(5);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_I_Processamento_Teste2() {

        int expected = 3628800;
        int result = Bloco_3.exercicio_I_Processamento(10);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_II_Percentagem_Teste1() {

        double[] notas = {10,8.5,6,15,18,20};
        double expected = 66.67;
        double result = Bloco_3.exercicio_II_Percentagem(notas);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_II_Percentagem_Teste2() {

        double[] notas = {18.5,9.6,14.1,15};
        double expected = 75;
        double result = Bloco_3.exercicio_II_Percentagem(notas);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_II_Media_Teste1() {

        double[] notas = {10,8.5,6,15,18,20};
        double expected = 7.25;
        double result = Bloco_3.exercicio_II_Media(notas);
        assertEquals(expected,result,0.01);

    }


    @Test
    void exercicio_II_Media_Teste2() {

        double[] notas = {18.5,9.6,14.1,15};
        double expected = 9.6;
        double result = Bloco_3.exercicio_II_Media(notas);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_III_Percentagem_Teste1() {

        int[] sequencia = {1,6,5,4,-3,4,8};
        double expected = 0.50;
        double result = Bloco_3.exercicio_III_Percentagem(sequencia);
        assertEquals(expected,result,0.0001);

    }

    @Test
    void exercicio_III_Percentagem_Teste2() {

        int[] sequencia = {1,6,5,4,6,4,-8};
        double expected = 0.6667;
        double result = Bloco_3.exercicio_III_Percentagem(sequencia);
        assertEquals(expected,result,0.0001);

    }

    @Test
    void exercicio_III_Media_Teste1() {

        int[] sequencia = {1,6,5,4,-3,4,8};
        double expected = 3.00;
        double result = Bloco_3.exercicio_III_Media(sequencia);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_III_Media_Teste2() {

        int[] sequencia = {5,3,9,4,6,4,-8};
        double expected = 5.67;
        double result = Bloco_3.exercicio_III_Media(sequencia);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_IV_a_Processamento() {

        int expected = 8;
        int result = Bloco_3.exercicio_IV_a_Processamento(3,25);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_IV_b_Processamento() {

        int expected = 4;
        int result = Bloco_3.exercicio_IV_b_Processamento(5,30,7);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_IV_e_Processamento_Teste1() {

        int expected = 86;
        int result = Bloco_3.exercicio_IV_e_Processamento(2,22,5,6);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_IV_e_Processamento_Teste2() {

        int expected = 93;
        int result = Bloco_3.exercicio_IV_e_Processamento(3,19,5,3);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_SomaParOuImpar_Teste1() {

        int expected = 56;
        int result = Bloco_3.exercicio_V_SomaParOuImpar(2,15,"par");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_SomaParOuImpar_Teste2() {

        int expected = 228;
        int result = Bloco_3.exercicio_V_SomaParOuImpar(8,30,"par");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_SomaParOuImpar_Teste3() {

        int expected = 63;
        int result = Bloco_3.exercicio_V_SomaParOuImpar(2,15,"ímpar");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_SomaParOuImpar_Teste4() {

        int expected = 209;
        int result = Bloco_3.exercicio_V_SomaParOuImpar(8,30,"ímpar");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_QtdParOuImpar_Teste1() {

        int expected = 7;
        int result = Bloco_3.exercicio_V_QtdParOuImpar(2,15,"par");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_QtdParOuImpar_Teste2() {

        int expected = 12;
        int result = Bloco_3.exercicio_V_QtdParOuImpar(8,30,"par");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_QtdParOuImpar_Teste3() {

        int expected = 7;
        int result = Bloco_3.exercicio_V_QtdParOuImpar(2,15,"ímpar");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_QtdParOuImpar_Teste4() {

        int expected = 11;
        int result = Bloco_3.exercicio_V_QtdParOuImpar(8,30,"ímpar");
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_e_Processamento_Teste1() {

        int expected = 45;
        int result = Bloco_3.exercicio_V_e_Processamento(16,3,3);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_e_Processamento_Teste2() {

        int expected = 75;
        int result = Bloco_3.exercicio_V_e_Processamento(3,28,5);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_f_Processamento_Teste1() {

        int expected = 29160;
        int result = Bloco_3.exercicio_V_f_Processamento(16,3,3);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_V_f_Processamento_Teste2() {

        int expected = 375000;
        int result = Bloco_3.exercicio_V_f_Processamento(3,28,5);
        assertEquals(expected,result);

    }

    @Test
    void exercise_V_f_Run_Test1() {

        double expected = 9.00;
        double result = Bloco_3.exercise_V_g_Run(16,2,3);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_V_f_Run_Test2() {

        double expected = 14.00;
        double result = Bloco_3.exercise_V_g_Run(22,0,7);
        assertEquals(expected,result);

    }

    @Test
    void exercise_V_f_Run_Test3() {

        double expected = 10.00;
        double result = Bloco_3.exercise_V_g_Run(5,15,2);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_a_Run_Test1() {

        int expected = 10;
        int result = Bloco_3.exercise_VI_a_Run(1568897412);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_a_Run_Test2() {

        int expected = 6;
        int result = Bloco_3.exercise_VI_a_Run(112500);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_b_Run_Test1() {

        int expected = 3;
        int result = Bloco_3.exercise_VI_b_Run(6320014);
        assertEquals(expected,result);
    }

    @Test
    void exercise_VI_b_Run_Test2() {

        int expected = 2;
        int result = Bloco_3.exercise_VI_b_Run(55988700);
        assertEquals(expected,result);
    }

    @Test
    void exercise_VI_b_Run_Test3() {

        int expected = 4;
        int result = Bloco_3.exercise_VI_b_Run(416022);
        assertEquals(expected,result);
    }

    @Test
    void exercise_VI_c_Run_Test1() {

        int expected = 2;
        int result = Bloco_3.exercise_VI_c_Run(6320014);
        assertEquals(expected,result);
    }

    @Test
    void exercise_VI_c_Run_Test2() {

        int expected = 4;
        int result = Bloco_3.exercise_VI_c_Run(55988700);
        assertEquals(expected,result);
    }

    @Test
    void exercise_VI_c_Run_Test3() {

        int expected = 1;
        int result = Bloco_3.exercise_VI_c_Run(416022);
        assertEquals(expected,result);
    }

    @Test
    void exercise_VI_d_Run_Test1() {

        int expected = 11;
        int result = Bloco_3.exercise_VI_d_Run(51000230);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_d_Run_Test2() {

        int expected = 44;
        int result = Bloco_3.exercise_VI_d_Run(51698771);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_d_Run_Test3() {

        int expected = 29;
        int result = Bloco_3.exercise_VI_d_Run(123698);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_e_Run_Test1() {

        int expected = 0;
        int result = Bloco_3.exercise_VI_e_Run(55000);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_e_Run_Test2() {

        int expected = 6;
        int result = Bloco_3.exercise_VI_e_Run(55024);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_e_Run_Test3() {

        int expected = 18;
        int result = Bloco_3.exercise_VI_e_Run(90804425);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_f_Run_Test1() {

        int expected = 10;
        int result = Bloco_3.exercise_VI_f_Run(55000);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_f_Run_Test2() {

        int expected = 10;
        int result = Bloco_3.exercise_VI_f_Run(55024);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_f_Run_Test3() {

        int expected = 14;
        int result = Bloco_3.exercise_VI_f_Run(90804425);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_g_Run_Test1() {

        double expected = 1.4;
        double result = Bloco_3.exercise_VI_g_Run(60001);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_g_Run_Test2() {

        double expected = 4.83;
        double result = Bloco_3.exercise_VI_g_Run(984026);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_g_Run_Test3() {

        double expected = 2.60;
        double result = Bloco_3.exercise_VI_g_Run(70060);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_h_Run_Test1() {

        double expected = 6;
        double result = Bloco_3.exercise_VI_h_Run(60001);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_h_Run_Test2() {

        double expected = 5;
        double result = Bloco_3.exercise_VI_h_Run(984026);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_h_Run_Test3() {

        double expected = 6;
        double result = Bloco_3.exercise_VI_h_Run(568460);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_i_Run_Test1() {

        double expected = 1;
        double result = Bloco_3.exercise_VI_i_Run(60001);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_i_Run_Test2() {

        double expected = 7;
        double result = Bloco_3.exercise_VI_i_Run(984056);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_VI_i_Run_Test3() {

        double expected = 2.33;
        double result = Bloco_3.exercise_VI_i_Run(568110);
        assertEquals(expected,result,0.01);

    }
/*
   @Test
    void exercise_VI_j_Run_Test1() {

        long expected = 011865L;
        long result = Bloco_3.exercise_VI_j_Run(568110L);
        assertEquals(expected,result);

    }*/

    @Test
    void exercise_VI_j_Run_Test2() {

        long expected = 158459988L;
        long result = Bloco_3.exercise_VI_j_Run(889954851L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VI_j_Run_Test3() {

        long expected = 4059851L;
        long result = Bloco_3.exercise_VI_j_Run(1589504L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_a_Run_Test1() {

        String expected = "Number 107313701 is a palindrome.";
        String result = Bloco_3.exercise_VII_a_Run(107313701);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_a_Run_Test2() {

        String expected = "Number 4224 is a palindrome.";
        String result = Bloco_3.exercise_VII_a_Run(4224);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_a_Run_Test3() {

        String expected = "Number 4263 is not a palindrome.";
        String result = Bloco_3.exercise_VII_a_Run(4263);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_b_Run_Test1() {

        String expected = "yes";
        String result = Bloco_3.exercise_VII_b_Run(153);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_b_Run_Test2() {

        String expected = "yes";
        String result = Bloco_3.exercise_VII_b_Run(407);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_b_Run_Test3() {

        String expected = "no";
        String result = Bloco_3.exercise_VII_b_Run(1500);
        assertEquals(expected,result);

    }


    @Test
    void exercise_VII_c_Run_Test1() {

        long expected = 303L;
        long result = Bloco_3.exercise_VII_c_Run(500L,300L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_c_Run_Test2() {

        long expected = 1331L;
        long result = Bloco_3.exercise_VII_c_Run(1600L,1255L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_c_Run_Test3() {

        long expected = 3L;
        long result = Bloco_3.exercise_VII_c_Run(3L,5L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_d_Run_Test1() {

        long expected = 494L;
        long result = Bloco_3.exercise_VII_d_Run(500L,300L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_d_Run_Test2() {

        long expected = 1551L;
        long result = Bloco_3.exercise_VII_d_Run(1600L,1255L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_d_Run_Test3() {

        long expected = 5L;
        long result = Bloco_3.exercise_VII_d_Run(3L,5L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_e_Run_Test1() {

        int expected = 20;
        long result = Bloco_3.exercise_VII_e_Run(500L,300L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_e_Run_Test2() {

        int expected = 3;
        long result = Bloco_3.exercise_VII_e_Run(3L,5L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_e_Run_Test3() {

        int expected = 19;
        long result = Bloco_3.exercise_VII_e_Run(0L,100L);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_f_Run_Test1() {

        int expected = 370;
        int result = Bloco_3.exercise_VII_f_Run(500,300);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_f_Run_Test2() {

        int expected = 0;
        int result = Bloco_3.exercise_VII_f_Run(3,5);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_f_Run_Test3() {

        int expected = 0;
        int result = Bloco_3.exercise_VII_f_Run(0,100);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_g_Run_Test1() {

        int expected = 3;
        int result = Bloco_3.exercise_VII_g_Run(500,300);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_g_Run_Test2() {

        int expected = 0;
        int result = Bloco_3.exercise_VII_g_Run(3,5);
        assertEquals(expected,result);

    }

    @Test
    void exercise_VII_g_Run_Test3() {

        int expected = 2;
        int result = Bloco_3.exercise_VII_g_Run(0,100);
        assertEquals(expected,result);

    }

    @Test
    void exercise_IX_Run_Test1() {

        double expected = 1040;
        double result = Bloco_3.exercise_IX_Run(1000,2);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_IX_Run_Test2() {

        double expected = 5000;
        double result = Bloco_3.exercise_IX_Run(5000,0);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_IX_Run_Test3() {

        double expected = 2220;
        double result = Bloco_3.exercise_IX_Run(1850,10);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_XI_Run_Test1() {

        double expected = 4;
        double result = Bloco_3.exercise_XI_Run(6);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XI_Run_Test2() {

        double expected = 5;
        double result = Bloco_3.exercise_XI_Run(8);
        assertEquals(expected,result);

    }


    @Test
    void exercise_XI_Run_Test3() {

        double expected = 1;
        double result = Bloco_3.exercise_XI_Run(20);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XI_Run_Test4() {

        double expected = 3;
        double result = Bloco_3.exercise_XI_Run(16);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XI_Run_Test5() {

        double expected = 2;
        double result = Bloco_3.exercise_XI_Run(2);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XII_Run_Test1() {

        String expected = "Solution: -1.0000";
        String result = Bloco_3.exercise_XII_Run(1,2,1);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XII_Run_Test2() {

        String expected = "Solutions: 1.0697 and -1.8697";
        String result = Bloco_3.exercise_XII_Run(5,4,-10);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XII_Run_Test3() {

        String expected = "Only imaginary solutions that can't be processed in this program.";
        String result = Bloco_3.exercise_XII_Run(1,1,1);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIII_Run_Test1() {

        String expected = "Vestuário";
        String result = Bloco_3.exercise_XIII_Run(6);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIII_Run_Test2() {

        String expected = "Limpeza e utensílios domésticos";
        String result = Bloco_3.exercise_XIII_Run(13);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIII_Run_Test3() {

        String expected = "Código inválido";
        String result = Bloco_3.exercise_XIII_Run(100);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIII_Run_Test4() {

        String expected = "";
        String result = Bloco_3.exercise_XIII_Run(0);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIV_Run_Test1() {

        Double expected = 8.51;
        Double result = Bloco_3.exercise_XIV_Run(5.55,"D");
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_XIV_Run_Test2() {

        Double expected = 80.74;
        Double result = Bloco_3.exercise_XIV_Run(0.50,"I");
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_XIV_Run_Test3() {

        Double expected = 7.74;
        Double result = Bloco_3.exercise_XIV_Run(10,"L");
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_XIV_Run_Test4() {

        Double expected = 57.56;
        Double result = Bloco_3.exercise_XIV_Run(6,"CS");
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_XIV_Run_Test5() {

        Double expected = 19.19;
        Double result = Bloco_3.exercise_XIV_Run(2,"CS");
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercise_XV_Run_Test1() {

        String expected = "Mau";
        String result = Bloco_3.exercise_XV_Run(4);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XV_Run_Test2() {

        String expected = "Muito Bom";
        String result = Bloco_3.exercise_XV_Run(18);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XV_Run_Test3() {

        String expected = "A mark cannot be greater than 20.";
        String result = Bloco_3.exercise_XV_Run(21);
        assertEquals(expected,result);

    }
}


