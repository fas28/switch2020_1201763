import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_3_ex_17Test {

    @Test
    void exercise_XVII_a_Run_Test1() {

        String expected = "The amount of food isn't adequate to a small dog";
        String result = Bloco_3_ex_17.exercise_XVII_a_Run(10,300);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_a_Run_Test2() {

        String expected = "The amount of food is adequate to a medium-size dog";
        String result = Bloco_3_ex_17.exercise_XVII_a_Run(13,250);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_a_Run_Test3() {

        String expected = "The amount of food isn't adequate to a big dog";
        String result = Bloco_3_ex_17.exercise_XVII_a_Run(30,250);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_a_Run_Test4() {

        String expected = "The amount of food is adequate to a gigantic dog";
        String result = Bloco_3_ex_17.exercise_XVII_a_Run(48,500);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_a_Run_Test5() {

        String expected = "The dog weight should be greater than zero";
        String result = Bloco_3_ex_17.exercise_XVII_a_Run(-5,500);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_a_Run_Test6() {

        String expected = "The dog weight should be greater than zero";
        String result = Bloco_3_ex_17.exercise_XVII_a_Run(0,0);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_b_Run_Test1() {

        String expected = "The amount of food isn't adequate to a small dog";
        String result = Bloco_3_ex_17.exercise_XVII_b_Run(10,100.5);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_b_Run_Test2() {

        String expected = "The amount of food is adequate to a medium-size dog";
        String result = Bloco_3_ex_17.exercise_XVII_b_Run(15,250);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_b_Run_Test3() {

        String expected = "The amount of food isn't adequate to a big dog";
        String result = Bloco_3_ex_17.exercise_XVII_b_Run(30,0);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XVII_b_Run_Test4() {

        String expected = "The amount of food is adequate to a gigantic dog";
        String result = Bloco_3_ex_17.exercise_XVII_b_Run(60,500);
        assertEquals(expected,result);

    }


    @Test
    void exercise_XVII_b_Run_Test5() {

        String expected = "The dog weight should be greater than zero";
        String result = Bloco_3_ex_17.exercise_XVII_b_Run(0,500);
        assertEquals(expected,result);

    }
}