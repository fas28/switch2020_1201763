import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_10Test {

    @Test
    void getLowest_Positives() {

        //Arrange
        int[] array = {5,6,4,3,5,6};
        int expected = 3;

        //Act
        int result = Bloco_4_ex_10.getLowest(array);

        //Assert
        assertEquals(result, expected);

    }

    @Test
    void getLowest_Negatives() {

        //Arrange
        int[] array = {2,6,3,-10,-6,0,11};
        int expected = -10;

        //Act
        int result = Bloco_4_ex_10.getLowest(array);

        //Assert
        assertEquals(result, expected);

    }

    @Test
    void getHighest_Positives() {

        //Arrange
        int[] array = {5,6,4,3,5,6};
        int expected = 6;

        //Act
        int result = Bloco_4_ex_10.getHighest(array);

        //Assert
        assertEquals(result, expected);

    }

    @Test
    void getHighest_Negatives() {

        //Arrange
        int[] array = {2,6,3,-10,-6,0,11};
        int expected = 11;

        //Act
        int result = Bloco_4_ex_10.getHighest(array);

        //Assert
        assertEquals(result, expected);

    }

    @Test
    void getAverage_Positives() {

        //Arrange
        int[] array = {5,6,4,3,5,6};
        double expected = 4.83;

        //Act
        double result = Bloco_4_ex_10.getAverage(array);

        //Assert
        assertEquals(result, expected,0.01);

    }

    @Test
    void getAverage_Negatives() {

        //Arrange
        int[] array = {2,6,3,-10,-6,0,11};
        double expected = 0.86;

        //Act
        double result = Bloco_4_ex_10.getAverage(array);

        //Assert
        assertEquals(result, expected,0.01);

    }

    @Test
    void getProduct_Positives() {

        //Arrange
        int[] array = {5,6,4,3,5,6};
        int expected = 10800;

        //Act
        int result = Bloco_4_ex_10.getProduct(array);

        //Assert
        assertEquals(result, expected);

    }

    @Test
    void getProduct_Negatives() {

        //Arrange
        int[] array = {2,6,3,-10,-6,1,11};
        int expected = 23760;

        //Act
        int result = Bloco_4_ex_10.getProduct(array);

        //Assert
        assertEquals(result, expected);

    }

    @Test
    void getUnique_Positives() {

        //Arrange
        int[] array = {5,3,6,4,6,3};
        int[] expected = {5,4};

        //Act
        int[] result = Bloco_4_ex_10.getUnique(array);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void getUnique_Negatives() {

        //Arrange
        int[] array = {1,0,0,-3,2,1,-5};
        int[] expected = {-3,2,-5};

        //Act
        int[] result = Bloco_4_ex_10.getUnique(array);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void getUnique_AllUniques() {

        //Arrange
        int[] array = {1,2,3,4,5};
        int[] expected = {1,2,3,4,5};

        //Act
        int[] result = Bloco_4_ex_10.getUnique(array);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void getUnique_NoUniques() {

        //Arrange
        int[] array = {1,1,1,1,1};

        //Act
        int[] result = Bloco_4_ex_10.getUnique(array);

        //Assert
        assertNull(result);

    }

    @Test
    void getReverse_GeneralUse() {

        //Arrange
        int[] array = {5,6,7};
        int[] expected = {7,6,5};

        //Act
        int[] result = Bloco_4_ex_10.getReverse(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getPrimeNumbers_Positives() {

        //Arrange
        int[] array = {1,8,2,10,12,5};
        int[] expected = {2,5};

        //Act
        int[] result = Bloco_4_ex_10.getPrimeNumbers(array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getPrimeNumbers_Negatives() {

        //Arrange
        int[] array = {-5,10,7,13,-100,0,1};
        int[] expected = {7,13};

        //Act
        int[] result = Bloco_4_ex_10.getPrimeNumbers(new int[] {-5,10,7,13,-100,0,1});

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getPrimeNumbers_Ones() {

        //Arrange
        int[] array = {1,1,1,1};

        //Act
        int[] result = Bloco_4_ex_10.getPrimeNumbers(array);

        //Assert
        assertNull(result);

    }

    @Test
    void getPrimeNumbers_NoPrimeNumbers() {

        //Arrange
        int[] array = {4,6,1,8,60};

        //Act
        int[] result = Bloco_4_ex_10.getPrimeNumbers(array);

        //Assert
        assertNull(result);

    }

    @Test
    void getPrimeNumbers_RepeatedPrimeNumbers() {

        //Arrange
        int[] array = {1,8,2,10,12,5,5,2};
        int[] expected = {2,5};

        //Act
        int[] result = Bloco_4_ex_10.getPrimeNumbers(array);

        //Assert
        assertArrayEquals(expected,result);

    }
}