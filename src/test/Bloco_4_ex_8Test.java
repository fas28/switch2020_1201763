import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_8Test {

    @Test
    void getCommonMultiples_PositivesN() {

        //Arrange
        int lim_1 = 12, lim_2 = 4;
        int[] array = new int[] {2,3};
        int[] expected = {6,12};

        //Act
        int[] result = Bloco_4_ex_8.getCommonMultiples(lim_1,lim_2,array);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void getCommonMultiples_NegativesN() {

        //Arrange
        int lim_1 = 1, lim_2 = 13;
        int[] array = new int[] {-2,3};

        //Act
        int[] result = Bloco_4_ex_8.getCommonMultiples(lim_1,lim_2,array);

        //Assert
        assertNull(result);

    }

    @Test
    void getCommonMultiples_NoMultiples() {

        //Arrange
        int lim_1 = 5, lim_2 = 10;
        int[] array = new int[] {4,5};

        //Act
        int[] result = Bloco_4_ex_8.getCommonMultiples(lim_1,lim_2,array);

        //Assert
        assertNull(result);

    }


    @Test
    void reduceArray_GeneralUse() {

        //Arrange
        int[] array = {-1,-1,3,0,-1,2,5};
        int number = -1;
        int[] expected = {3,0,2,5};

        //Act
        int[] result = Bloco_4_ex_8.reduceArray(array, number);

        //Assert
        assertArrayEquals(expected,result);

    }


    @Test
    void reduceArray_NumberDoesNotExistInArray() {

        //Arrange
        int[] array = {-1,-3,2,5,0,2,1};
        int number = 6;
        int[] expected = {-1,-3,2,5,0,2,1};

        //Act
        int[] result = Bloco_4_ex_8.reduceArray(array, number);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void reduceArray_RemoveAllElements() {

        //Arrange
        int[] array = {5};
        int number = 5;

        //Act
        int[] result = Bloco_4_ex_8.reduceArray(array, number);

        //Assert
        assertNull(result);

    }
}