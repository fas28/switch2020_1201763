import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_12Test {

    @Test
    void checkIfLinesHaveSameNumberOfColumns_DoubleMatrix_ReturnsNrOfColumns() {

        //Arrange
        double[][] matrix = {{2,3,1},{-2.4,2,0}};
        int expected = 3;

        //Act
        int result = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_DoubleMatrix(matrix);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void checkIfLinesHaveSameNumberOfColumns_DoubleMatrix_ReturnsNegativeNumber() {

        //Arrange
        double[][] matrix = {{2,3,1},{-2.4},{1}};
        int expected = -1;

        //Act
        int result = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_DoubleMatrix(matrix);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void checkIfLinesHaveSameNumberOfColumns_IntMatrix_ReturnsNrOfColumns() {

        //Arrange
        int[][] matrix = {{2,3,1},{-3,2,0}};
        int expected = 3;

        //Act
        int result = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_IntMatrix(matrix);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void checkIfLinesHaveSameNumberOfColumns_IntMatrix_ReturnsNegativeNumber() {

        //Arrange
        int[][] matrix = {{2,3,1},{-3},{1}};
        int expected = -1;

        //Act
        int result = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_IntMatrix(matrix);

        //Assert
        assertEquals(expected,result);

    }
}