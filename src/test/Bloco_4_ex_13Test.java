import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_13Test {

    @Test
    void isSquareMatrix_DoubleMatrix_ReturnsTrue() {

        //Arrange
        double[][] matrix = {{1,2,3,4},{2,4,2,1},{5,6,8,1},{2,-9.5,9,0}};

        //Act
        boolean result = Bloco_4_ex_13.isSquareMatrix_DoubleMatrix(matrix);

        //Assert
        assertTrue(result);

    }

    @Test
    void isSquareMatrix_DoubleMatrix_ReturnsFalse() {

        //Arrange
        double[][] matrix = {{1,2},{2,4,2,1},{5,6,8,1},{2,-9.5,9,0}};

        //Act
        boolean result = Bloco_4_ex_13.isSquareMatrix_DoubleMatrix(matrix);

        //Assert
        assertFalse(result);

    }

    @Test
    void isSquareMatrix_DoubleMatrix_ReturnsFalse2() {

        //Arrange
        double[][] matrix = {{1,2,3,4},{2,4,2,1},{5,6,8,1}};

        //Act
        boolean result = Bloco_4_ex_13.isSquareMatrix_DoubleMatrix(matrix);

        //Assert
        assertFalse(result);

    }


    @Test
    void isSquareMatrix_IntMatrix_ReturnsTrue() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{2,4,2,1},{5,6,8,1},{2,-10,9,0}};

        //Act
        boolean result = Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix);

        //Assert
        assertTrue(result);

    }

    @Test
    void isSquareMatrix_IntMatrix_ReturnsFalse() {

        //Arrange
        int[][] matrix = {{1,2},{2,4,2,1},{5,6,8,1},{2,-10,9,0}};

        //Act
        boolean result = Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix);

        //Assert
        assertFalse(result);

    }

    @Test
    void isSquareMatrix_IntMatrix_ReturnsFalse2() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{2,4,2,1},{5,6,8,1}};

        //Act
        boolean result = Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix);

        //Assert
        assertFalse(result);

    }
}