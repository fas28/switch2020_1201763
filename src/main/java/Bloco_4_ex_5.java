import java.util.Scanner;

public class Bloco_4_ex_5 {

    /**
     * Bloco 4, Ex. 5 - obtain the sum of the even digits of a positive integer
     * @param number a positive integer
     * @return the sum (of int type) of the even digits of a positive integer
     */

    public static int getSumOfEvens(int number) {

        int[] array = Bloco_4_ex_2.getArrayFromPositiveInteger(number);
        int[] evenArray = Bloco_4_ex_4.getArrayOfEvens(array);
        int i, sum = 0;

        for (i = 0; i < evenArray.length; i++) {
            sum+=evenArray[i];
        }

        return sum;

    }

    /**
     * Bloco 4, Ex. 5 - obtain the sum of the uneven digits of a positive integer
     * @param number a positive integer
     * @return the sum (of int type) of the uneven digits of a positive integer
     */

    public static int getSumOfUnevens(int number) {

        int[] array = Bloco_4_ex_2.getArrayFromPositiveInteger(number);
        int[] unevenArray = Bloco_4_ex_4.getArrayOfUnevens(array);
        int i, sum = 0;

        for (i = 0; i < unevenArray.length; i++) {
            sum+=unevenArray[i];
        }

        return sum;

    }
}
