public class Bloco_4_ex_16 {

    /**
     * Bloco 4, Ex.16 - obtains the determinant of a given matrix according to Laplace's theorem
     * @param matrix a 2D int square matrix
     * @return the determinant of the given int matrix according to Laplace's theorem (Double type)
     */

    public static Double getDeterminantOfMatrix (int[][] matrix) {

        Double determinant = null;

        if (matrix != null) {
            if (Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix)) {

                //Initialize variables

                int i, j = 0, k = 1;
                determinant = 0.0;
                int[][] reducedMatrix = reduceMatrix(matrix,0,j);
                int[] firstRow = getFirstRowOfMatrix(matrix);

                for (i = 0; i < firstRow.length; i++) {
                    if (reducedMatrix.length > 2) {
                        reducedMatrix = reduceMatrix(matrix,0,j);
                        determinant += Math.pow(-1,1+k)*firstRow[i]*getDeterminantOfMatrix(reducedMatrix); //Recursive expression
                        k++; //Increase the power of the expression Math.pow(-1,1+k)
                        j++; //Iterate through the elements of firstRow so that at each one a new reduced matrix is determined
                    } else {
                        reducedMatrix = reduceMatrix(matrix,0,j);
                        determinant += Math.pow(-1,1+k)*firstRow[i]*getDeterminantOf2x2Matrix(reducedMatrix);
                        k++; //Increase the power of the expression Math.pow(-1,1+k)
                        j++; //Iterate through the elements of firstRow so that at each one a new reduced matrix is determined
                    }
                }
            }
        }

        return determinant;

    }

    /**
     * Method to obtain the first row of a given matrix
     * @param matrix a 2D int square matrix
     * @return a 1D int array that is the first row of a given matrix
     */

    public static int[] getFirstRowOfMatrix (int[][] matrix) {

        int j;
        int[] firstRow = null;

        if (matrix != null) {
            firstRow = new int[matrix[0].length];
            for (j = 0; j < matrix[0].length; j++) {
                firstRow[j] = matrix[0][j];
            }
        }

        return  firstRow;

    }

    /**
     * Method to obtain the reduced version of a given matrix (considering the first row is always eliminated)
     * @param matrix a 2D int square matrix
     * @param jPosition an int that specifies the column that must be eliminated from the original matrix
     * @return an int square matrix that is the reduced version of a given matrix
     */

    public static int[][] reduceMatrix (int[][] matrix, int iPosition, int jPosition) {

        int[][] reducedMatrix = null;

        if (iPosition < matrix.length && jPosition < matrix[0].length) {

            reducedMatrix = new int[matrix.length-1][matrix[0].length-1];
            int i, j, k = 0, l = 0;

            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[0].length; j++) {
                    if (i != iPosition && j != jPosition) {
                        reducedMatrix[k][l] = matrix[i][j];
                        if (l < matrix.length - 2) {
                            l++;
                        } else {
                            l = 0;
                            k++;
                        }
                    }
                }
            }
        }

        return reducedMatrix;

    }

    /**
     * Method to obtain the determinant of a given 2X2 int matrix
     * @param matrix a 2D 2X2 int square matrix
     * @return the determinant of the given 2x2 int matrix (Double type)
     */

    public static Double getDeterminantOf2x2Matrix (int[][] matrix) {

        Double determinant2X2Matrix = null;

        if (matrix != null) {
            if (Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix) && Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_IntMatrix(matrix) == 2)
            determinant2X2Matrix = (double)((matrix[1][1]*matrix[0][0]) - (matrix[1][0]*matrix[0][1]));
        }

        return determinant2X2Matrix;

    }

}
