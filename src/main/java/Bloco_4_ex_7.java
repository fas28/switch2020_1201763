import java.util.Arrays;

public class Bloco_4_ex_7 {

    /**
     * Bloco 4, Ex. 7 - obtain an array with the multiples of 3 within a given interval
     * @param lim_1 a positive integer or zero
     * @param lim_2 a positive integer or zero
     * @return a 1D array of int type with the multiples of 3 within a given interval
     */

    public static int[] exercise_VII(int lim_1, int lim_2) {

        int i, j = 0, count = 0;
        int[] array = null;

        //Order the limits of the interval
        int[] lim = orderLimits(lim_1, lim_2);

        if (lim != null) {
            //Count the number of multiples of 3
            for (i = lim[0]; i <= lim[1]; i++) {
                if (i % 3 == 0 && i != 0) {
                    count += 1;
                }
            }

            //Fill in the array with the multiples of 3
            if (count != 0) {
                array = new int[count];

                for (i = lim[0]; i <= lim[1]; i++) {
                    if (i % 3 == 0 && i != 0) {
                        array[j] = i;
                        j++;
                    }
                }

            }
        }

        return array;

    }

    /**
     * Bloco 4, Ex. 7 - obtain an array with the multiples of a given positive integer within a given interval
     * @param lim_1 a positive integer or zero
     * @param lim_2 a positive integer or zero
     * @param N a positive integer
     * @return a 1D array of int type with the multiples of a given positive integer within a given interval
     */

    public static int[] getMultiples(int lim_1, int lim_2, int N) {

        int i, j = 0, count = 0;
        int[] array = null;

        //Order the limits of the interval
        int[] lim = orderLimits(lim_1, lim_2);

        if (lim != null) {
            //Count the number of multiples of N
            for (i = lim[0]; i <= lim[1]; i++) {
                if (i % N == 0 && i != 0) {
                    count += 1;
                }
            }

        //Fill in the array with the multiples of N
        if (N > 0) {
            if (count != 0) {
                array = new int[count];

                for (i = lim[0]; i <= lim[1]; i++) {
                    if (i % N == 0 && i != 0) {
                        array[j] = i;
                        j++;
                    }
                }
            }
        }
    }
        return array;
    }

    /**
     * Method to obtain an array with the ordered limits of an interval
     * @param lim_1 a positive integer or zero
     * @param lim_2 a positive integer or zero
     * @return int 1D array in which the first value is the lowest number and the second is the highest
     */

    public static int[] orderLimits(int lim_1, int lim_2) {

        int[] lim = null;

        if (lim_1 >= 0 && lim_2 >= 0) {
            lim = new int[2];
            lim[0] = lim_1;
            lim[1] = lim_2;

            //Revert the positions if necessary
            if (lim_1 > lim_2) {
                lim[0] = lim_2;
                lim[1] = lim_1;
            }

        }

        return lim;

    }


}
