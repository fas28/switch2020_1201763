public class Bloco_4_ex_19 {

    /**
     * Bloco 4, Ex.19-b) verifies if there are blank cells in a sudoku matrix
     * @param matrix a 2D int matrix
     * @return true if there are blank cells in a sudoku matrix, false if there aren't
     */

    public static boolean checkIfThereAreBlankCells (int[][] matrix) {

        boolean check = false;

        if (matrix != null) {
            int i, j;
            do {
                for (i = 0; i < 9; i++) {
                    for (j = 0; j < 9; j++) {
                        if (matrix[i][j] == 0) {
                            check = true;
                        }
                    }
                }
            } while (!check);
        }


        return check;

    }

    /**
     * Bloco 4, Ex.19-c) returns a mask matrix that contains 1 where the given matrix has a specified number, and 0 where it hasn't
     * @param matrix a 2D int matrix
     * @param number an int number
     * @return a 2D int matrix (mask matrix) that contains 1 where the given matrix has a specified number, and 0 where it hasn't
     */

    public static int[][] getMaskMatrix (int[][] matrix, int number) {

        int[][] maskMatrix = null;

        if (matrix != null && (number >= 1 && number <= 9)) {
            int i, j;
            maskMatrix = new int[9][9];

            for (i = 0; i < 9; i ++) {
                for (j = 0; j < 9; j++) {
                    if (matrix[i][j] == number) {
                        maskMatrix[i][j] = 1;
                    } else {
                        maskMatrix[i][j] = 0;
                    }
                }
            }

        }

        return maskMatrix;

    }

    /**
     * Bloco 4, Ex.19-d) inserts a number in a blank cell of the matrix
     * @param matrix a 2D int matrix
     * @param number an int number
     * @param iPosition the column of the cell where the player wants to insert the number (int type)
     * @param jPosition the line of the cell where the player wants to insert the number (int type)
     * @return a 2D int matrix with the inserted number
     */

    public static int[][] insertNumberInMatrix (int[][] matrix, int number, int iPosition, int jPosition) {

        int[][] updatedMatrix = null;

        if (matrix != null && (number >= 1 && number <= 9) && (iPosition >= 0 && iPosition <= 8) && (jPosition >= 0 && jPosition <= 8)) {
            boolean check = checkIfThereAreBlankCells(matrix);
            if (check) {

                if (matrix[iPosition][jPosition] == 0) {
                    updatedMatrix = matrix;
                    updatedMatrix[iPosition][jPosition] = number;
                }

            }
        }

        return updatedMatrix;

    }


}
