public class Bloco_4_ex_9 {

    /**
     * Bloco 4, Ex. 9 - determine if a given number is a palindrome
     * @param number a positive integer
     * @return false or true (boolean type)
     */

    public static boolean isPalindrome(int number) {

        int[] array = Bloco_4_ex_2.getArrayFromPositiveInteger(number);
        int len = array.length, i = 0, j = 0;
        boolean isPalindrome = true;

        if (number > 0) {
            int[] reverse = getReverseArray(array);
            while (i < len && isPalindrome) {
                if (array[i] != reverse[i]) {
                    isPalindrome = false;
                }
                i++;
            }
        } else {
            isPalindrome = false;
        }

        return isPalindrome;

    }

    /**
     * Method that returns the reverse of a given array
     * @param array a 1D int array
     * @return a 1D int array that is the reverse of the original array
     */

    public static int[] getReverseArray(int[] array) {

        int i, len = array.length, j = 0;
        int[] reverse = new int[len];

        for (i = 0; i < len; i++) {
            reverse[i] = array[len-1-j];
            j++;
        }

        return reverse;
    }
}