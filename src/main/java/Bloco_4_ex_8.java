import java.util.Arrays;

public class Bloco_4_ex_8 {

    /**
     * Bloco 4, Ex. 8 - determine an array of common multiples of an array of integers within a given interval
     * @param lim_1 a positive integer or zero
     * @param lim_2 a positive integer or zero
     * @param N a 1D integer array of integers
     * @return a 1D array of int type with the common multiples of an array of integers within a given interval
     */

    public static int[] getCommonMultiples(int lim_1, int lim_2, int[] N) {

        int i, j = 0;
        int[] reducedArray = null;

        //Get the multiples of the first element of the N array
        int[] mult = Bloco_4_ex_7.getMultiples(lim_1,lim_2,N[0]);

        //Verify if the multiples of N[0] are also multiples of the other elements of N; if not, they become equal to -1
        if (mult != null) {
            for (i = 1; i < N.length; i++) {
                for (j = 0; j < mult.length; j++) {
                    if (mult[j] % N[i] != 0) {
                        mult[j] = -1;
                    }
                }
            }
            reducedArray = reduceArray(mult,-1);
        }

        //Reduce the array so that -1 values are removed
        return reducedArray;

    }

    /**
     * Method to remove from an array a specified int value
     * @param array a 1D array of int type
     * @param number an integer
     * @return the reduced array, a 1D array of int type
     */

    public static int[] reduceArray(int[] array, int number) {

        int i, j = 0, count = array.length, count_2 = 0;
        int[] reducedArray = null;

        //Count the number of elements of the reduced array
        for (i = 0; i < count; i++) {
            if (array[i] != number) {
                count_2 += 1;
            }
        }

        //Fill in the reduced array
        if (count_2 > 0) {
        reducedArray = new int[count_2];
            for (i = 0; i < count; i++) {
                if (array[i] != number) {
                    reducedArray[j] = array[i];
                    j++;
                }
            }
        }

        return reducedArray;
    }
}

