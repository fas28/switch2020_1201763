import java.util.Scanner;

public class Bloco_3_ex_17 {

    public static void main(String[] args) {
        exercise_XVII_a();
        exercise_XVII_b();
    }

    public static void exercise_XVII_a() {

        double dogWeight, foodAmount;
        String result;
        Scanner read = new Scanner(System.in);

        System.out.println("Insert the dog's weight in kg: ");
        dogWeight = read.nextDouble();
        System.out.println("Insert the dog's daily amount of food in g: ");
        foodAmount = read.nextDouble();

        result = exercise_XVII_a_Run(dogWeight, foodAmount);

        System.out.println(result);

    }

    public static void exercise_XVII_b() {

        double dogWeight = 0, foodAmount;
        String result;
        Scanner read = new Scanner(System.in);

        while (dogWeight >=0) {
            System.out.println("Insert the dog's weight in kg: ");
            dogWeight = read.nextDouble();
            if (dogWeight >= 0) {
                System.out.println("Insert the dog's daily amount of food in g: ");
                foodAmount = read.nextDouble();
                result = exercise_XVII_b_Run(dogWeight, foodAmount);
                System.out.println(result);
            } else {
                System.out.println("Finished.");
            }
        }

    }

    public static String exercise_XVII_a_Run(double dogWeight, double foodAmount) {

        String result;

        if (dogWeight <= 0) {
            result = "The dog weight should be greater than zero";
        } else if (dogWeight <= 10) {
            if (foodAmount == 100) {
                result = "The amount of food is adequate to a small dog";
            } else {
                result = "The amount of food isn't adequate to a small dog";
            }
        } else if (dogWeight <= 25) {
            if (foodAmount == 250) {
                result = "The amount of food is adequate to a medium-size dog";
            } else {
                result = "The amount of food isn't adequate to a medium-size dog";
            }
        } else if (dogWeight <= 45) {
            if (foodAmount == 300) {
                result = "The amount of food is adequate to a big dog";
            } else {
                result = "The amount of food isn't adequate to a big dog";
            }
        } else {
            if (foodAmount == 500) {
                result = "The amount of food is adequate to a gigantic dog";
            } else {
                result = "The amount of food isn't adequate to a gigantic dog";
            }
        }

        return result;
    }

    public static String exercise_XVII_b_Run(double dogWeight, double foodAmount) {

        String result;

        if (dogWeight == 0) {
            result = "The dog weight should be greater than zero";
        } else if (dogWeight <= 10) {
            if (foodAmount == 100) {
                result = "The amount of food is adequate to a small dog";
            } else {
                result = "The amount of food isn't adequate to a small dog";
            }
        } else if (dogWeight <= 25) {
            if (foodAmount == 250) {
                result = "The amount of food is adequate to a medium-size dog";
            } else {
                result = "The amount of food isn't adequate to a medium-size dog";
            }
        } else if (dogWeight <= 45) {
            if (foodAmount == 300) {
                result = "The amount of food is adequate to a big dog";
            } else {
                result = "The amount of food isn't adequate to a big dog";
            }
        } else {
            if (foodAmount == 500) {
                result = "The amount of food is adequate to a gigantic dog";
            } else {
                result = "The amount of food isn't adequate to a gigantic dog";
            }
        }

        return result;
    }


}
