import java.util.Arrays;
import java.util.Scanner;

public class Bloco_4_ex_3 {

    /**
     * Bloco 4, Ex.3 - obtain the sum of the elements of an array
     * @param array a 1D array of double type
     * @return the sum (double type) of the elements of a 1D array
     */

    public static double getArraySum(double[] array) {

        int i;
        double sum = 0;

        for (i = 0; i < array.length; i++) {
            sum+=array[i];
        }

        return sum;

    }
}
