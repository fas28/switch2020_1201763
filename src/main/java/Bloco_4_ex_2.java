import java.util.Arrays;
import java.util.Scanner;

public class Bloco_4_ex_2 {

    /**
     * Bloco 4, Ex. 2 - create an array in which its elements are the digits of a positive integer
     * @param number a positive integer
     * @return a 1D array in which its elements are the digits of a positive integer
     */

    public static int[] getArrayFromPositiveInteger(int number) {

        int numDigits = Bloco_4_ex_1.getNumberOfDigits(number);
        int partial = number, digit, i = numDigits - 1;

        if (numDigits == -1) {
            int[] array = new int[]{0};
            return array;
        } else {
            int[] array = new int[numDigits];

            while (partial > 0) {
                digit = partial % 10;
                array[i] = digit;
                i--;
                partial /= 10;
            }
            return array;
        }

    }
}
