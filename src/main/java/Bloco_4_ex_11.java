public class Bloco_4_ex_11 {

    /**
     * Bloco 4, Ex.11 - determine the scalar product of two arrays with the same length
     * @param array_1 a 1D array of double type
     * @param array_2 a 1D array of double type
     * @return the scalar product (of double type) of two arrays with the same length
     */

    public static Double getScalarProduct (double[] array_1, double[] array_2) {

        int i, j, len_1 = array_1.length, len_2 = array_2.length;
        Double product = null;

        if (len_1 == len_2) {
            product = 0.0;
            for (i = 0; i < len_1; i++) {
                product = product + (array_1[i] * array_2[i]);
            }
        }

        return product;

    }
}
