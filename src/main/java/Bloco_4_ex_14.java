public class Bloco_4_ex_14 {

    /**
     * Bloco 4, Ex.14 - checks if a matrix is rectangular
     * @param matrix a 2D matrix of double type
     * @return true if it is a rectangular matrix, false if it's not (boolean type)
     */

    public static boolean isRectangularMatrix_DoubleMatrix(double[][] matrix) {

        boolean isSquareMatrix = Bloco_4_ex_13.isSquareMatrix_DoubleMatrix(matrix);
        boolean result = false;

        if (!isSquareMatrix) {
            int columns = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_DoubleMatrix(matrix);
            if (columns != -1) {
                result = true;
            }
        }

        return result;

    }

    public static boolean isRectangularMatrix_IntMatrix(int[][] matrix) {

        boolean isSquareMatrix = Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix);
        boolean result = false;

        if (!isSquareMatrix) {
            int columns = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_IntMatrix(matrix);
            if (columns != -1) {
                result = true;
            }
        }

        return result;

    }

}
