import java.util.Arrays;

public class Bloco_4_ex_15 {

    /**
     * Bloco 4, Ex.15-a), determine the lowest value of a matrix of integers
     *
     * @param matrix matrix of integers
     * @return value of the lowest element of a matrix of integers
     */

    public static int getLowestValueMatrix(int[][] matrix) {

        int numLines = matrix.length, lowest = matrix[0][0], i, j;

        for (i = 0; i < numLines; i++) {
            for (j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] < lowest) {
                    lowest = matrix[i][j];
                }
            }

        }

        return lowest;

    }

    /**
     * Bloco 4, Ex.15-b), determine the highest value of a matrix of integers
     *
     * @param matrix matrix of integers
     * @return value of the highest element of a matrix of integers
     */

    public static int getHighestValueMatrix(int[][] matrix) {

        int numLines = matrix.length, highest = matrix[0][0], i, j;

        for (i = 0; i < numLines; i++) {
            for (j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] > highest) {
                    highest = matrix[i][j];
                }
            }

        }

        return highest;

    }

    /**
     * Bloco 4, Ex.15-c), determine the average value of a matrix of integers
     *
     * @param matrix matrix of integers
     * @return value of the average value of a matrix of integers
     */

    public static double getAverageValueMatrix(int[][] matrix) {

        int numLines = matrix.length, i, j;
        double sum = 0, count = 0;

        for (i = 0; i < numLines; i++) {
            for (j = 0; j < matrix[i].length; j++) {
                sum += matrix[i][j];
                count += 1;
            }
        }

        return (sum / count);

    }

    /**
     * Bloco 4, Ex.15-d), determine the product of the values of a matrix of integers
     *
     * @param matrix matrix of integers
     * @return value of the product of the values of a matrix of integers
     */
    public static double getProductValueMatrix(int[][] matrix) {

        int numLines = matrix.length, i, j;
        double product = 1;

        for (i = 0; i < numLines; i++) {
            for (j = 0; j < matrix[i].length; j++) {
                product *= matrix[i][j];
            }
        }

        return product;

    }

    /**
     * Bloco 4, Ex.15-e), deliver an array of the elements of the matrix without repetitions
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the elements of the matrix without repetitions
     */

    public static int[] getNonRepeatedValuesInMatrix(int[][] matrix) {

        int[] unique = null;

        if (matrix != null) {

            int i, j, numLines = matrix.length, numColumns = matrix[0].length;
            unique = new int[0];

            for (i = 0; i < numLines; i++) {
                for (j = 0; j < numColumns; j++) {
                    if (!findValueInArray(matrix[i][j], unique)) {
                        unique = addValueToArray(matrix[i][j], unique);
                    }
                }
            }
        }

        return unique;

    }

    /**
     * Bloco 4, Ex.15-e) alternative interpretation, delivers an array of the elements that only appear once in the
     * matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the elements that only appear once in the matrix
     */

    public static int[] getUniqueValuesInMatrix(int[][] matrix) {

        int[] unique = null;

        if (matrix != null) {
            int[] array = matrixToArray(matrix);
            unique = Bloco_4_ex_10.getUnique(array);
        }

        return unique;

    }

    /**
     * Method that converts a matrix to an array
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains all the elements of the matrix
     */

    public static int[] matrixToArray(int[][] matrix) {

        int[] array = null;

        if (matrix != null) {

            int i, j, k = 0, len = getArrayLengthWhenTurningMatrixToArray(matrix);
            array = new int[len];

            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[i].length; j++) {
                    array[k] = matrix[i][j];
                    k++;
                }
            }
        }

        return array;

    }

    /**
     * Method that determines the length of an array to which a matrix is being turned into
     *
     * @param matrix a 2D int matrix
     * @return an int that is the length of the array
     */


    public static int getArrayLengthWhenTurningMatrixToArray(int[][] matrix) {

        int numLines = matrix.length;
        int numColumns = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_IntMatrix(matrix);
        int len = 0, i, j;

        if (numColumns == -1) {
            for (i = 0; i < numLines; i++) {
                len += matrix[i].length;
            }
        } else {
            len = numLines * numColumns;
        }

        return len;

    }

    /**
     * Method to turn an int matrix to a double matrix
     *
     * @param matrix a 2D int matrix
     * @return a 2D double matrix that has the same content of the given matrix
     */

    public static double[][] intToDoubleMatrix(int[][] matrix) {

        int i, j;
        double[][] doubleMatrix = new double[matrix.length][];

        for (i = 0; i < matrix.length; i++) {
            doubleMatrix[i] = new double[matrix[i].length];
        }

        copyIntMatrixToDouble(matrix, doubleMatrix);

        return doubleMatrix;

    }


    public static void copyIntMatrixToDouble(int[][] m1, double[][] m2) {

        for (int i = 0; i < m1.length; i++) {
            for (int j = 0; j < m1[i].length; j++) {
                m2[i][j] = m1[i][j];
            }
        }

    }

    /**
     * Method that indicates whether a given value exists in a given array
     *
     * @param value an integer
     * @param array a 1D int array
     * @return true if the value exists in the array, false if it doesn't exist (boolean)
     */

    public static boolean findValueInArray(int value, int[] array) {

        boolean valueInMatrix = false;

        if (array != null) {
            int i = 0, len = array.length;

            while (i < len && !valueInMatrix) {
                if (array[i] == value) {
                    valueInMatrix = true;
                }
                i++;
            }
        }

        return valueInMatrix;

    }

    /**
     * Method that adds a given value to a given array
     *
     * @param value an integer
     * @param array a 1D int array
     * @return a 1D int array that contains the values of the given array plus the given value
     */

    public static int[] addValueToArray(int value, int[] array) {

        int[] newArray = null;

        if (array != null) {
            int i, len = array.length;
            newArray = new int[len + 1];

            for (i = 0; i < len; i++) {
                newArray[i] = array[i];
            }

            newArray[len] = value;
        }

        return newArray;

    }

    /**
     * Bloco 4, Ex.15-f) determines the prime numbers in a given matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array with the prime numbers in a given matrix (without repetitions)
     */

    public static int[] getPrimeNumbersInMatrix(int[][] matrix) {

        int[] primeNumbers = null;

        if (matrix != null) {
            int[] array = matrixToArray(matrix);
            primeNumbers = Bloco_4_ex_10.getPrimeNumbers(array);
        }

        return primeNumbers;

    }

    /**
     * Bloco 4, Ex.15-g) returns an array with the main diagonal of a square or rectangular matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the main diagonal of a square or rectangular matrix
     */

    public static int[] getMainDiagonalOfMatrix(int[][] matrix) {

        int[] mainDiagonal = null;

        if (matrix != null) {
            if (Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix)) {
                int numLines = matrix.length;
                mainDiagonal = getMainDiagonalOfSquareMatrix(matrix, numLines);
            } else if (Bloco_4_ex_14.isRectangularMatrix_IntMatrix(matrix)) {
                mainDiagonal = getMainDiagonalOfRectangularMatrix(matrix);
            }
        }

        return mainDiagonal;

    }

    /**
     * Bloco 4, Ex.15-g) returns an array with the main diagonal of a rectangular matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the main diagonal of a rectangular matrix
     */

    public static int[] getMainDiagonalOfRectangularMatrix(int[][] matrix) {

        int i, j, k = 0;
        int count = getCountOfDiagonalInRectangularMatrix(matrix);
        int[] mainDiagonal = new int[count];

        for (i = 0; i < count; i++) {
            for (j = 0; j < count; j++) {
                if (i == j) {
                    mainDiagonal[k] = matrix[i][j];
                    k++;
                }
            }
        }
        return mainDiagonal;
    }

    /**
     * Bloco 4, Ex.15-g) returns an array with the main diagonal of a square matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the main diagonal of a square matrix
     */

    public static int[] getMainDiagonalOfSquareMatrix(int[][] matrix, int numLines) {

        int i, j, count = 0;
        int[] mainDiagonal = new int[numLines];

        for (i = 0; i < numLines; i++) {
            for (j = 0; j < numLines; j++) {
                if (i == j) {
                    mainDiagonal[count] = matrix[i][j];
                    count++;
                }
            }
        }
        return mainDiagonal;
    }

    /**
     * Method that returns the number of elements of the diagonal in a rectangular matrix
     *
     * @param matrix a 2D int matrix
     * @return an Integer that is the number of elements of the diagonal in a rectangular matrix
     */

    public static Integer getCountOfDiagonalInRectangularMatrix(int[][] matrix) {

        Integer count = null;

        if (matrix != null) {
            int numLines = matrix.length;
            int numColumns = matrix[0].length;
            count = numLines;

            if (numLines > numColumns) {
                count = numColumns;
            }

        }

        return count;

    }

    /**
     * Bloco 4, Ex.15-h) returns an array with the second diagonal of a square or rectangular matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the second diagonal of a square or rectangular matrix
     */

    public static int[] getSecondDiagonalOfMatrix(int[][] matrix) {

        int[] secondDiagonal = null;

        if (matrix != null) {
            if (Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix)) {
                int numLines = matrix.length;
                secondDiagonal = getSecondDiagonalOfSquareMatrix(matrix, numLines);
            } else if (Bloco_4_ex_14.isRectangularMatrix_IntMatrix(matrix)) {
                secondDiagonal = getSecondDiagonalOfRectangularMatrix(matrix);
            }
        }

        return secondDiagonal;

    }

    /**
     * Bloco 4, Ex.15-h) returns an array with the second diagonal of a square matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the second diagonal of a square matrix
     */

    public static int[] getSecondDiagonalOfSquareMatrix(int[][] matrix, int numLines) {

        int i, j, count = 0;
        int[] secondDiagonal = new int[numLines];

        for (i = 0; i < numLines; i++) {
            for (j = 0; j < numLines; j++) {
                if (i + j == numLines - 1) {
                    secondDiagonal[count] = matrix[i][j];
                    count++;
                }
            }
        }
        return secondDiagonal;
    }

    /**
     * Bloco 4, Ex.15-h) returns an array with the second diagonal of a rectangular matrix
     *
     * @param matrix a 2D int matrix
     * @return a 1D int array that contains the second diagonal of a rectangular matrix
     */

    public static int[] getSecondDiagonalOfRectangularMatrix(int[][] matrix) {

        int count = getCountOfDiagonalInRectangularMatrix(matrix);
        int[] secondDiagonal = new int[count];

        int numLines = matrix.length;
        int numColumns = matrix[0].length;

        if (numLines > numColumns) {
            secondDiagonal = getSecondDiagonalOfRectangularMatrixMoreLinesThanColumns(matrix, count, numColumns);
        } else {
            secondDiagonal = getSecondDiagonalOfRectangularMatrixMoreColumnsThanLines(matrix, count, numLines,
                    numColumns);
        }

        return secondDiagonal;
    }

    /**
     * Bloco 4, Ex.15-h) returns an array with the second diagonal of a rectangular matrix that has more lines than
     * columns
     *
     * @param matrix     a 2D int matrix
     * @param count      the number of elements of the second diagonal (int type)
     * @param numColumns the number of columns of the matrix (int type)
     * @return a 1D array of int type that is the second diagonal of the matrix
     */

    public static int[] getSecondDiagonalOfRectangularMatrixMoreLinesThanColumns(int[][] matrix, int count,
                                                                                 int numColumns) {
        int i, j, k = 0;
        int[] secondDiagonal = new int[count];

        for (i = 0; i < count; i++) {
            for (j = numColumns - 1; j >= 0; j--) {
                if (i + j == count - 1) {
                    secondDiagonal[k] = matrix[i][j];
                    k++;
                }
            }
        }

        return secondDiagonal;
    }

    /**
     * Bloco 4, Ex.15-h) returns an array with the second diagonal of a rectangular matrix that has more columns than
     * lines
     *
     * @param matrix     a 2D int matrix
     * @param count      the number of elements of the second diagonal (int type)
     * @param numLines   the number of lines of the matrix (int type)
     * @param numColumns the number of columns of the matrix (int type)
     * @return a 1D array of int type that is the second diagonal of the matrix
     */

    public static int[] getSecondDiagonalOfRectangularMatrixMoreColumnsThanLines(int[][] matrix, int count,
                                                                                 int numLines, int numColumns) {
        int i, j, k = 0;
        int[] secondDiagonal = new int[count];

        for (i = 0; i < numLines; i++) {
            for (j = numColumns - 1; j >= numColumns - count; j--) {
                if (i + j == count) {
                    secondDiagonal[k] = matrix[i][j];
                    k++;
                }
            }
        }

        return secondDiagonal;
    }

    /**
     * Bloco 4, Ex.15-i) check if a matrix is an identity matrix
     *
     * @param matrix a 2D int matrix
     * @return true if it's an identity matrix, false if it's not
     */

    public static boolean isIdentityMatrix(int[][] matrix) {

        boolean isIdentityMatrix = false;

        if (matrix != null) {
            if (Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix)) {
                isIdentityMatrix = isIdentityMatrix_CheckValues(matrix);
            }
        }

        return isIdentityMatrix;
    }


    /**
     * Bloco 4, Ex.15-i) check if a matrix is an identity matrix given that it is a square matrix
     *
     * @param matrix a 2D int square matrix
     * @return true if it's an identity matrix, false if it's not
     */

    public static boolean isIdentityMatrix_CheckValues(int[][] matrix) {

        boolean isIdentityMatrix = true;
        int i, j;

        for (i = 0; i < matrix.length; i++) {
            for (j = 0; j < matrix[0].length; j++) {
                if (i == j) {
                    if (matrix[i][j] != 1) {
                        isIdentityMatrix = false;
                    }
                } else {
                    if (matrix[i][j] != 0) {
                        isIdentityMatrix = false;
                    }
                }
            }
        }


        return isIdentityMatrix;
    }

    /**
     * Bloco 4, Ex.15-j) determine the inverse matrix of a given matrix using the cofactor matrix
     * @param matrix a 2D int matrix
     * @return a double 2D matrix that is the inverse of the given matrix
     */

    public static double[][] getInverseMatrix(int[][] matrix) {

        double[][] inverseMatrix = null;

        if (matrix != null) {
            if (Bloco_4_ex_13.isSquareMatrix_IntMatrix(matrix)) {
                double[][] cofactorMatrix = getCofactorMatrix(matrix);

                double[][] transpose = getTransposeMatrix_DoubleMatrix(cofactorMatrix);

                if (matrix.length > 2) {
                    Double determinant = Bloco_4_ex_16.getDeterminantOfMatrix(matrix);
                    inverseMatrix = Bloco_4_ex_17.getProductOfMatrixAndScalar_DoubleMatrixAndScalar(transpose,
                            (1 / determinant));
                } else {
                    Double determinant = Bloco_4_ex_16.getDeterminantOf2x2Matrix(matrix);
                    inverseMatrix = Bloco_4_ex_17.getProductOfMatrixAndScalar_DoubleMatrixAndScalar(transpose,
                            (1 / determinant));
                }

            }
        }

        return inverseMatrix;

    }

    /**
     * Method that determines the cofactor matrix of a given matrix
     * @param matrix a 2D int matrix
     * @return a 2D double type matrix that has the cofactor elements of a given matrix
     */

    public static double[][] getCofactorMatrix(int[][] matrix) {

        double[][] cofactorMatrix = null;

        if (matrix != null) {
            cofactorMatrix = new double[matrix.length][matrix[0].length];
            int i, j;

            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[0].length; j++) {
                    if ((i + 1 + j + 1) % 2 == 0) {
                        cofactorMatrix[i][j] = Math.round(getCofactorElement(matrix, i, j) * (100.000 / 100.000));
                    } else {
                        cofactorMatrix[i][j] = -Math.round(getCofactorElement(matrix, i, j) * (100.000 / 100.000));
                    }
                }
            }
        }

        return cofactorMatrix;

    }

    /**
     * Method that obtains the cofactor element within a cofactor matrix
     * @param matrix a 2D int matrix
     * @param iPosition the line of the cofactor element in the cofactor matrix
     * @param jPosition the column of the cofactor element in the cofactor matrix
     * @return a double type cofactor element within a cofactor matrix
     */

    public static double getCofactorElement(int[][] matrix, int iPosition, int jPosition) { //2x2, 0, 0

        double determinant = 0;

        if (matrix != null) {
            int[][] reducedMatrix = Bloco_4_ex_16.reduceMatrix(matrix, iPosition, jPosition); //1x1
            determinant = getDeterminantForCofactor(reducedMatrix); //determinante da 1x1
        }

        return determinant;

    }

    /**
     * Method that obtains the determinant of a specific cofactor element
     * @param matrix a 2D int matrix
     * @return double type determinant of a specific cofactor element
     */

    public static double getDeterminantForCofactor(int[][] matrix) {

        double determinant = 0;

        if (matrix.length > 2) { //true (4) -> true (3)
            int[] firstRow = Bloco_4_ex_16.getFirstRowOfMatrix(matrix); //7,8,9,10 -> 13,14,15
            int j, k = 1;

            for (j = 0; j < firstRow.length; j++) {
                determinant += Math.pow(-1, k + 1) * firstRow[j] * getDeterminantForCofactor(Bloco_4_ex_16.reduceMatrix(matrix, 0, j));//3X3 -->2X2
                k++;
            }

        } else if (matrix.length == 2){
            determinant += Bloco_4_ex_16.getDeterminantOf2x2Matrix(matrix);
        } else {
            determinant += matrix[0][0];
        }

        return determinant;

    }

    /**
     * Bloco 4, Ex.15-k) get the transpose matrix of a given matrix
     * @param matrix a 2D int matrix
     * @return a 2D int matrix that is the transpose of a given matrix
     */

    public static int[][] getTransposeMatrix(int[][] matrix) {

        int[][] transposeMatrix = null;

        if (matrix != null) {
            //Define the dimensions of the transpose matrix
            int numLines = matrix[0].length;
            int numColumns = matrix.length;
            transposeMatrix = new int[numLines][numColumns];

            //Fill in the transpose matrix
            int i, j;

            for (i = 0; i < numLines; i++) {
                for (j = 0; j < numColumns; j++) {
                    transposeMatrix[i][j] = matrix[j][i];
                }
            }
        }

        return transposeMatrix;

    }

    /**
     * Method to obtain the transpose matrix of a given matrix
     * @param matrix a 2D double matrix
     * @return a 2D double matrix that is the transpose of a given matrix
     */

    public static double[][] getTransposeMatrix_DoubleMatrix(double[][] matrix) {

        double[][] transposeMatrix = null;

        if (matrix != null) {
            //Define the dimensions of the transpose matrix
            int numLines = matrix[0].length;
            int numColumns = matrix.length;
            transposeMatrix = new double[numLines][numColumns];

            //Fill in the transpose matrix
            int i, j;

            for (i = 0; i < numLines; i++) {
                for (j = 0; j < numColumns; j++) {
                    transposeMatrix[i][j] = matrix[j][i];
                }
            }
        }

        return transposeMatrix;

    }

}
