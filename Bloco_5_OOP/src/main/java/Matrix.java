public class Matrix {

    //ATTRIBUTES

    private Vector[] vectorList;


    //CONSTRUCTORS

    /**
     * Bloco 5, 3-a) empty constructor
     */
    public Matrix() {
        this.vectorList = new Vector[0];
    }

    /**
     * Bloco 5, 3-b) non-empty constructor
     *
     * @param entryList an object of Vector type
     */
    public Matrix(Vector[] entryList) {

        if (entryList == null) {
            throw new IllegalArgumentException("Cannot create a matrix with a null vector list.");
        } else if (entryList.length == 0) {
            throw new IllegalArgumentException("Cannot create a matrix with an empty vector list.");
        } else {
            this.vectorList = returnCopyVectorArray(entryList, entryList.length);
        }

    }


    //OPERATIONS - PUBLIC METHODS

    /**
     * Bloco 5, 3-c) add a value to a specified line in the vector list of the Matrix class
     *
     * @param value      an int value
     * @param lineNumber an int that is the line number
     */
    public void addValueToMatrixLine(int value, int lineNumber) {

        if (lineNumber > vectorList.length - 1 || lineNumber < 0) {
            throw new IndexOutOfBoundsException("Line number is out of bounds of matrix.");
        } else {
            int[] matrixLine = getLineOfMatrix(lineNumber);
            Vector aux = new Vector(matrixLine);
            aux.addValue(value);
            this.vectorList[lineNumber] = aux;
        }

    }

    /**
     * Bloco 5, ex.3-d) removes the first match of a given value from the matrix in Matrix class
     *
     * @param value an int value
     * @return true if there was a successful removal, false if there wasn't a match and therefore no removal
     */
    public boolean removeValueFromMatrixAtFirstMatch(int value) {

        boolean found = false;

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            for (int j = 0; j < line.toArray().length && !found; j++) {
                if (line.toArray()[j] == value) {
                    line.removeValueAtFirstMarch(value);
                    this.vectorList[i] = line;
                    found = true;
                }
            }
        }

        return found;

    }

    /**
     * Bloco 5, 3-e)
     *
     * @return true if the matrix object of the Matrix class is empty, false if it isn't
     */
    public boolean isEmptyMatrix() {

        return this.vectorList.length == 0;

    }

    /**
     * Bloco 5, 3-f)
     *
     * @return the highest value (int type) in the matrix object of the Matrix class
     */
    public int highestElementOfMatrix() {

        int highestElement = this.vectorList[0].highestElement();

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            for (int j = 0; j < line.toArray().length; j++) {
                if (line.toArray()[j] > highestElement) {
                    highestElement = line.returnValueByIndex(j);
                }
            }
        }

        return highestElement;
    }

    /**
     * Bloco 5, 3-g)
     *
     * @return the lowest value (int type) in the matrix object of the Matrix class
     */
    public int lowestElementOfMatrix() {

        int lowestElement = this.vectorList[0].lowestElement();

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            for (int j = 0; j < line.toArray().length; j++) {
                if (line.toArray()[j] < lowestElement) {
                    lowestElement = line.returnValueByIndex(j);
                }
            }
        }

        return lowestElement;
    }

    /**
     * Bloco 5, 3-h)
     *
     * @return the average (double type) of the elements in the matrix object of the Matrix class
     */
    public double getAverageOfMatrix() {

        int count = 0, sum = 0;

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            count += line.returnNumberOfElements();
            for (int j = 0; j < line.toArray().length; j++) {
                sum += line.toArray()[j];
            }
        }

        return (double) sum / count;

    }

    /**
     * Bloco 5, 3-i)
     *
     * @return an array of Vector type that contains for each position the sum of the elements of the corresponding
     * line of the matrix in the Matrix class
     */
    public Vector vectorWithSumOfMatrixLines() {

        if (isEmptyMatrix()) {
            return new Vector();
        } else {
            int[] values = new int[this.vectorList.length];

            for (int i = 0; i < this.vectorList.length; i++) {
                int sum = 0;
                Vector line = new Vector(this.vectorList[i].toArray());
                for (int j = 0; j < line.toArray().length; j++) {
                    sum += line.returnValueByIndex(j);
                }
                values[i] = sum;
            }

            Vector vector = new Vector(values);

            return vector;
        }

    }

    /**
     * Bloco 5, 3-j)
     *
     * @return an array of Vector type that contains for each position the sum of the elements of the corresponding
     * column of the matrix in the Matrix class
     */
    public Vector vectorWithSumOfMatrixColumns() {

        if (isEmptyMatrix()) {
            return new Vector();
        } else {
            int numColumns = this.getMaxNumberOfColumns();
            int[] values = new int[numColumns];

            for (int i = 0; i < numColumns; i++) {
                int sum = 0;
                for (int j = 0; j < this.vectorList.length; j++) {
                    if (i < this.vectorList[j].returnNumberOfElements()) {
                        sum += this.vectorList[j].returnValueByIndex(i);
                    }
                }
                values[i] = sum;
            }

            Vector vector = new Vector(values);

            return vector;
        }

    }

    /**
     * Bloco 5, 3-k)
     *
     * @return the index of the line of the matrix object in the Matrix class that has the highest sum of elements
     */
    public int getLineIndexWithHighestSum() {

        if (isEmptyMatrix()) {
            return -1;
        } else {
            int[] values = new int[this.vectorList.length];
            Vector auxVector = new Vector(values);
            Vector[] aux = {auxVector};
            Matrix auxMatrix = new Matrix(aux);

            Vector sumOfLines = auxMatrix.vectorWithSumOfMatrixLines();
            int highest = sumOfLines.returnValueByIndex(0);

            for (int i = 0; i < sumOfLines.returnNumberOfElements(); i++) {
                if (sumOfLines.returnValueByIndex(i) > highest) {
                    highest = sumOfLines.returnValueByIndex(i);
                }
            }

            return highest;
        }
    }

    /**
     * Bloco 5, 3-l)
     *
     * @return true if the matrix object in the Matrix class is a square matrix
     */
    public boolean isSquareMatrix() {

        if (!checkIfMatrixLinesHaveSameNumberOfColumns()) {
            return false;
        } else {
            int numLines = this.vectorList.length;
            int numColumns = this.vectorList[0].returnNumberOfElements();

            if (numLines == numColumns) {
                return true;
            } else {
                return false;
            }
        }

    }

    /**
     * Bloco 5, 3-m)
     *
     * @return true if the matrix object in the Matrix class is a square symmetric matrix, false if it isn't
     */
    public boolean isSquareSymmetricMatrix() {

        if (isSquareMatrix()) {
            if (this.isEqualToMatrix(this.getTransposeMatrix())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    /**
     * Bloco 5, 3-n)
     *
     * @return the number (int type) of elements not equal to zero from the main diagonal of the matrix object in the
     * Matrix class
     */
    public int getNumberOfNonZeroElementsFromMatrixMainDiagonal() {

        int count = 0;

        if (!this.isSquareMatrix()) {
            return -1;
        } else {
            Vector mainDiagonal = this.getMainDiagonal();

            for (int i = 0; i < mainDiagonal.returnNumberOfElements(); i++) {
                if (mainDiagonal.returnValueByIndex(i) != 0) {
                    count += 1;
                }
            }
        }

        return count;

    }

    /**
     * Bloco 5, 1-o)
     *
     * @return true if the main and secondary diagonal of the matrix object of the Matrix class are equal, false if
     * they aren't
     */
    public boolean checkIfMainAndSecondaryDiagonalAreEqual() {

        boolean result = false;

        if (this.isSquareMatrix()) {
            if (this.getSecondaryDiagonal().isEqualToVector(this.getMainDiagonal())) {
                result = true;
            }
        }

        return result;

    }

    /**
     * Bloco 5, 3-p)
     *
     * @return a Vector array that contains the elements of the matrix object in the Matrix class which have more
     * digits than the average number of digits of the matrix
     */
    public Vector getElementsFromMatrixWithNrOfDigitsHigherThanAverage() {

        Vector aux = new Vector();

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            for (int j = 0; j < line.returnNumberOfElements(); j++) {
                if (line.getNrOfDigitsArray()[j] > this.getAverageNrOfDigitsOfMatrix()) {
                    aux.addValue(line.returnValueByIndex(j));
                }
            }

        }

        Vector vector = aux.removeRepeatedElements();

        return vector;

    }

    /**
     * Bloco 5, 3-q)
     *
     * @return a Vector array that contains the elements of the matrix object in the Matrix class which have a
     * percentage of even digits higher than the average percentage of even digits of the matrix
     */
    public Vector getElementsFromMatrixWithPercentageOfEvensHigherThanAverage() {

        Vector aux = new Vector();

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            for (int j = 0; j < line.returnNumberOfElements(); j++) {
                if (line.getPercentageOfEvenDigitsArray()[j] > this.getAveragePercentageOfEvenDigitsOfMatrix()) {
                    aux.addValue(line.returnValueByIndex(j));
                }
            }

        }

        Vector vector = aux.removeRepeatedElements();

        return vector;

    }

    /**
     * Bloco 5, 3-r) inverts each line of the matrix object in the Matrix class
     */
    public void invertOrderOfMatrixLines() {

        if (!isEmptyMatrix()) {
            for (int i = 0; i < this.vectorList.length; i++) {
                Vector line = new Vector(this.vectorList[i].toArray());
                Vector newLine = line.revertOrder();
                this.vectorList[i] = newLine;
            }
        }

    }

    /**
     * Bloco 5, 3-s) inverts each column of the matrix object in the Matrix class
     */
    public void invertOrderOfMatrixColumns() {

        if (!isEmptyMatrix()) {
            Vector[] lines = new Vector[this.vectorList.length];
            int k = 0;

            for (int i = this.vectorList.length - 1; i >= 0; i--) {
                lines[k] = new Vector();
                for (int j = 0; j < this.vectorList[i].returnNumberOfElements(); j++) {
                    lines[k].addValue(this.vectorList[i].returnValueByIndex(j));
                }
                k++;
            }

            this.vectorList = lines;
        }

    }

    /**
     * Bloco 5, 3-t) rotate 90 degrees positive (counter-clockwise) the matrix object in the Matrix class
     */
    public void rotateNinetyDegreesPositive() {

        if (this.isSquareMatrix()) {
            Matrix matrix =  this.getTransposeMatrix();
            matrix.invertOrderOfMatrixLines();
            this.vectorList = matrix.vectorList;
        }

    }

    /**
     * Bloco 5, 3-u) rotate 180 degrees the matrix object in the Matrix class
     */
    public void rotateOneHundredAndEightyDegrees() {

        if (this.isSquareMatrix()) {
            this.rotateNinetyDegreesPositive();
            this.rotateNinetyDegreesPositive();

        }

    }

    /**
     * Bloco 5, 3-v) rotate 90 degrees negative (clockwise) the matrix object in the Matrix class
     */
    public void rotateNinetyDegreesNegative() {

        if (this.isSquareMatrix()) {
            Matrix matrix =  this.getTransposeMatrix();
            matrix.invertOrderOfMatrixColumns();
            this.vectorList = matrix.vectorList;
        }

    }

    /**
     * Public method that gets the vector list from the object of the Matrix class
     *
     * @return the vector list (Vector[]) from the object of the Matrix class
     */
    public Vector[] toVectorArray() {

        return this.returnCopyVectorArray(this.vectorList, this.vectorList.length);

    }

    /**
     * Public method that gets the bidimensional int array from the object of the Matrix class
     *
     * @return the bidimensional array (int[][]) from the object of the Matrix class
     */
    public int[][] toIntBiArray() {

        return this.returnCopyIntBiArray(this.vectorList, this.vectorList.length);

    }

    /**
     * Public method that assesses if a given object is equal to the matrix object of the Matrix class
     *
     * @param object any type of object (object type)
     * @return true if the given object is equal to the matrix object of the Matrix class, false if it isn't
     */
    public boolean isEqualToMatrix(Object object) {

        if (object == null) {
            return false;
        } else if (object == this) {
            return true;
        } else {
            if (object.getClass() != this.getClass()) {
                return false;
            } else {
                Matrix otherMatrix = (Matrix) object;
                if (this.vectorList.length != otherMatrix.toVectorArray().length) {
                    return false;
                } else {
                    boolean isEqual = true;
                    for (int i = 0; i < this.vectorList.length && isEqual; i++) {
                        if (!otherMatrix.toVectorArray()[i].isEqualToVector(this.vectorList[i])) {
                            isEqual = false;
                        }
                    }
                    return isEqual;
                }
            }
        }
    }


    //OPERATIONS - PRIVATE METHODS

    /**
     * Private method that copies values from an array to a copy array
     *
     * @param entryList an array of Vector type
     * @param size      the length of the copy array (int value)
     * @return a Vector array that is a copy of the array that was passed through this method
     */
    private Vector[] returnCopyVectorArray(Vector[] entryList, int size) {

        Vector[] copyList = new Vector[size];

        for (int i = 0; i < size && i < entryList.length; i++) {
            Vector line = new Vector(entryList[i].toArray());
            copyList[i] = line;
        }

        return copyList;

    }

    /**
     * Private method that copies values from an array to a copy array
     *
     * @param entryList an array of Vector type
     * @param size      the length of the copy array (int value)
     * @return a bidimensional int array (int[][]) that is a copy of the array that was passed through this method
     */
    private int[][] returnCopyIntBiArray(Vector[] entryList, int size) {

        int[][] biArray = new int[entryList.length][];

        for (int i = 0; i < size && i < entryList.length; i++) {
            Vector line = new Vector(getLineOfMatrix(i));
            biArray[i] = new int[line.returnNumberOfElements()];

            for (int j = 0; j < biArray[i].length; j++) {
                biArray[i][j] = line.returnValueByIndex(j);
            }
        }

        return biArray;

    }

    /**
     * Private method that returns a specified line of the matrix object in Matrix class
     *
     * @param lineNumber an integer that is the line number
     * @return an int[] array that is the specified line of the matrix
     */
    private int[] getLineOfMatrix(int lineNumber) {

        int length = this.vectorList[lineNumber].returnNumberOfElements();
        int[] lineOfMatrix = new int[length];

        for (int i = 0; i < length; i++) {
            lineOfMatrix[i] = this.vectorList[lineNumber].returnValueByIndex(i);
        }

        return lineOfMatrix;

    }

    /**
     * Private method that returns the maximum number of columns of the lines of the matrix object in the Matrix
     * class
     *
     * @return an int that is the maximum
     */
    private int getMaxNumberOfColumns() {

        int[] aux = new int[this.vectorList.length];

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            aux[i] = line.returnNumberOfElements();
        }

        Vector auxVector = new Vector(aux);

        return auxVector.highestElement();

    }

    /**
     * Private method that checks if all the lines in the matrix object of the Matrix class have the same number of
     * columns
     *
     * @return true if all the lines have the same number of columns, false if not
     */
    private boolean checkIfMatrixLinesHaveSameNumberOfColumns() {

        boolean check = true;
        boolean result = true;
        int numColumns = this.vectorList[0].returnNumberOfElements();

        for (int i = 0; i < this.vectorList.length && check; i++) {
            if (this.vectorList[i].returnNumberOfElements() != numColumns) {
                check = false;
                result = false;
            }
        }

        return result;
    }


    /**
     * A private method that returns the transpose matrix of the matrix object in the Matrix class
     *
     * @return a Matrix type of array that is the transpose matrix
     */
    private Matrix getTransposeMatrix() {

        int numLines = this.vectorList[0].returnNumberOfElements();

        Vector[] vectorTransposed = new Vector[numLines];

        for (int i = 0; i < numLines; i++) {
            vectorTransposed[i] = getColumnFromMatrix(i);
        }

        Matrix transposeMatrix = new Matrix(vectorTransposed);

        return transposeMatrix;

    }

    /**
     * Private method that extracts a column from the matrix object in the Matrix class
     *
     * @param index an int that is the index of the column
     * @return a Vector array that contains the elements of the column of the specified index
     */
    private Vector getColumnFromMatrix(int index) {

        int[] valuesColumn = new int[this.vectorList.length];

        for (int i = 0; i < this.vectorList.length; i++) {
            valuesColumn[i] = this.vectorList[i].returnValueByIndex(index);
        }

        Vector column = new Vector(valuesColumn);

        return column;

    }

    /**
     * Private method that returns the main diagonal of the matrix object in the Matrix class
     *
     * @return a Vector array that contains the elements of the main diagonal
     */
    private Vector getMainDiagonal() {

        int[] values = new int[this.vectorList.length];

        for (int i = 0; i < this.vectorList.length; i++) {
            for (int j = 0; j < this.vectorList.length; j++) {
                if (i == j) {
                    values[i] = this.vectorList[i].returnValueByIndex(j);
                }
            }
        }

        Vector mainDiagonal = new Vector(values);

        return mainDiagonal;

    }

    /**
     * Private method that returns the secondary diagonal of the matrix object in the Matrix class
     *
     * @return a Vector array that contains the elements of the secondary diagonal
     */
    private Vector getSecondaryDiagonal() {

        int[] values = new int[this.vectorList.length];

        for (int i = 0; i < this.vectorList.length; i++) {
            for (int j = 0; j < this.vectorList.length; j++) {
                if (i + j == this.vectorList.length - 1) {
                    values[i] = this.vectorList[i].returnValueByIndex(j);
                }
            }
        }

        Vector secondaryDiagonal = new Vector(values);

        return secondaryDiagonal;

    }

    /**
     * Private method that determines the average number of digits of the elements of the matrix object in the Matrix
     * class
     *
     * @return a double type that is the average
     */
    private double getAverageNrOfDigitsOfMatrix() {

        int count = 0, sum = 0;

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            count += line.returnNumberOfElements();
            int[] nrDigits = line.getNrOfDigitsArray();

            for (int j = 0; j < nrDigits.length; j++) {
                sum += nrDigits[j];
            }
        }

        return Math.ceil((double) sum / count);

    }

    /**
     * Private method that determines the average number of even digits of the elements of the matrix object in the
     * Matrix class
     *
     * @return a double type that is the average
     */
    private double getAveragePercentageOfEvenDigitsOfMatrix() {

        int count = 0, sum = 0;

        for (int i = 0; i < this.vectorList.length; i++) {
            Vector line = new Vector(this.vectorList[i].toArray());
            count += line.returnNumberOfElements();
            double[] percentageOfEvenDigits = line.getPercentageOfEvenDigitsArray();

            for (int j = 0; j < percentageOfEvenDigits.length; j++) {
                sum += percentageOfEvenDigits[j];
            }
        }

        return Math.ceil(((double) sum / count));

    }


}




