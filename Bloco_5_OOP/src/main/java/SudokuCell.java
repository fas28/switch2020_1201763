public class SudokuCell {

    //Attributes

    private int value;
    private String type;


    //Constructor

    /**
     * Non-empty constructor that is called when we want to insert a value in the initial matrix
     * @param value an int value that we want to insert in the cell
     */
    public SudokuCell (int value) {

        if (!this.checkIfValueIsValid(value)) {
            throw new IllegalArgumentException("The value you're trying to insert isn't valid.");
        } else {
            this.value = copyValue(value);
            this.type = "fixed";
        }

    }

    //Methods

    /**
     * Public method that adds a given value to a non-fixed cell
     * @param value an int value
     */
    public void addValueToCell (int value) {

        if (this.type == "fixed" || !this.checkIfValueIsValid(value) ) {
            throw new IllegalArgumentException("The value you're trying to insert isn't valid.");
        } else {
            this.value = copyValue(value);
        }

    }

    /**
     * A private method that checks whether the value of the cell is valid
     * @param value a value of int type
     * @return true if it is valid (ranges from 1 to 9), false if it isn't
     */
    private boolean checkIfValueIsValid(int value) {

        if (value > 9 || value < 1) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Private method that creates a copy of a given value
     * @param value an int value
     * @return an integer that is a copy of the given value
     */
    private int copyValue (int value) {

        int copy = value;
        return copy;

    }


}
