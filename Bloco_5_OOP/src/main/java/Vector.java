public class Vector {

    //ATTRIBUTES

    private int[] values;


    //CONSTRUCTORS

    public Vector() {
        this.values = new int[0];
    }

    public Vector(int[] values) {
        if (values == null) {
            throw new IllegalArgumentException("Array shouldn't be null.");
        } else if (values.length == 0) {
            throw new IllegalArgumentException("Array shouldn't be empty.");
        } else {
            this.values = copyValuesFromArray(values, values.length);
        }

    }


    //OPERATIONS - PUBLIC METHODS

    /**
     * Bloco 5, ex.1-c) adds a value to the object of class Vector
     *
     * @param value a value of int type
     */

    public void addValue(int value) {
        this.values = copyValuesFromArray(this.values, this.values.length + 1);
        this.values[this.values.length - 1] = value;
    }

    /**
     * Bloco 5, ex.1-d) removes the first match of a given value from the object in Vector class
     *
     * @param value an int value
     */

    public void removeValueAtFirstMarch(int value) {

        boolean found = false;

        for (int i = 0; (i < this.values.length) && !found; i++) {
            if (this.values[i] == value) {
                int index = i;
                found = true;
                int[] leftSide = copyValuesFromArray(this.values, index);
                int[] rightSide = copyValuesFromArrayWithStartPosition(this.values, this.values.length - i - 1,
                        index + 1);
                this.values = joinArray(leftSide, rightSide);
            }
        }

    }

    /**
     * Auxiliary method to return a copy of the object of Vector class
     * No entry parameters
     *
     * @return an int[] array that is a copy of the object of Vector class
     */

    public int[] toArray() {
        return this.copyValuesFromArray(this.values, this.values.length);
    }

    /**
     * Bloco 5, ex.1-e)
     *
     * @param index a value of int type that is the index of the value we want to retrieve
     * @return the value in a specified position within the object of the Vector class
     */

    public int returnValueByIndex(int index) {

        if (index > this.values.length || index < 0) {
            throw new ArrayIndexOutOfBoundsException("Index is out of the array's boundaries.");
        } else {
            return this.values[index];
        }

    }

    /**
     * Bloco 5, ex.1-f)
     *
     * @return the number of elements of an object of the Vector class
     */

    public int returnNumberOfElements() {
        return this.values.length;
    }

    /**
     * Bloco 5, ex.1-g)
     *
     * @return the highest element within an object of the Vector class
     */
    public int highestElement() {

        int highest = this.values[0];

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] > highest) {
                highest = this.values[i];
            }
        }
        return highest;

    }


    /**
     * Bloco 5, ex.1-h)
     *
     * @return the lowest element within an object of the Vector class
     */

    public int lowestElement() {

        int lowest = this.values[0];

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] < lowest) {
                lowest = this.values[i];
            }
        }

        return lowest;

    }

    /**
     * Bloco 5, ex.1-i)
     *
     * @return the average (double type) of the elements of the object in Vector class
     */

    public double getAverage() {

        int count = 0, sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            count += 1;
            sum += this.values[i];
        }

        return (double) sum / count;

    }

    /**
     * Bloco 5, ex.1-j)
     *
     * @return the average (double type) of the even elements of the object in Vector class
     */

    public double getAverageOfEvens() {

        int count = 0, sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] % 2 == 0 && this.values[i] != 0) {
                count += 1;
                sum += this.values[i];
            }
        }

        if (count != 0) {
            return (double) sum / count;
        } else {
            throw new ArithmeticException("No even elements found, therefore cannot divide by zero.");
        }

    }

    /**
     * Bloco 5, ex.1-k)
     *
     * @return the average (double type) of the odd elements of the object in Vector class
     */

    public double getAverageOfOdds() {

        int count = 0, sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] % 2 != 0) {
                count += 1;
                sum += this.values[i];
            }
        }

        if (count != 0) {
            return (double) sum / count;
        } else {
            throw new ArithmeticException("No even elements found, therefore cannot divide by zero.");
        }
    }

    /**
     * Bloco 5, ex.1-l)
     *
     * @param value an int value
     * @return the average (double type) of the multiples of a given value in the object in Vector class
     */

    public double getAverageOfMultiples(int value) {

        int count = 0, sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] % value == 0) {
                count += 1;
                sum += this.values[i];
            }
        }
        if (count != 0) {
            return (double) sum / count;
        } else {
            return -1;
        }

    }

    /**
     * Bloco 5, ex.1-m)
     *
     * @return an int[] array that has the elements of the object in the Vector class in an ascending order
     */

    public int[] getAscendingArray() {

        int[] ascendingArray = new int[this.values.length];

        for (int i = 0; i < ascendingArray.length; i++) {
            ascendingArray[i] = this.lowestElement();
            this.removeValueAtFirstMarch(this.lowestElement());
        }
        return ascendingArray;

    }

    /**
     * Bloco 5, ex.1-m)
     *
     * @return an int[] array that has the elements of the object in the Vector class in a descending order
     */

    public int[] getDescendingArray() {

        int[] descendingArray = new int[this.values.length];

        for (int i = 0; i < descendingArray.length; i++) {
            descendingArray[i] = this.highestElement();
            this.removeValueAtFirstMarch(this.highestElement());
        }
        return descendingArray;

    }

    /**
     * Bloco 5, ex.1-n)
     *
     * @return true if the object in the Vector class is empty, false if it's not
     */

    public boolean isEmptyArray() {
        if (this.values.length == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Bloco 5, ex.1-o)
     *
     * @return true if the object in the Vector class has only one element, and false if it hasn't
     */

    public boolean arrayHasOneElement() {

        if (this.values.length == 1) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Bloco 5, ex.1-p)
     *
     * @return true if the object in the Vector class has only even numbers, and false if it hasn't
     */

    public boolean arrayHasOnlyEvenNumbers() {

        int count = this.countNumberOfEvenElements();
        if (count == this.values.length) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Bloco 5, ex.1-q)
     *
     * @return true if the object in the Vector class has only odd numbers, and false if it hasn't
     */

    public boolean arrayHasOnlyOddNumbers() {

        int count = this.countNumberOfOddElements();

        if (count == this.values.length) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Bloco 5, ex.1-r)
     *
     * @return true if the object in the Vector class has repeated elements, and false if it hasn't
     */

    public boolean arrayHasRepeatedValues() {

        if (this.countUniqueElements() != this.values.length) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Bloco 5, ex.1-s)
     *
     * @return a Vector object that contains elements which number of digits is higher than the average number of
     * digits of an existing object in the Vector class
     */

    public Vector getElementsWithNrOfDigitsHigherThanAverage() {

        Vector vector = new Vector();
        int count = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.getNrOfDigitsArray()[i] > this.getAverageOfNrOfDigits()) {
                vector.addValue(this.values[i]);
            }
        }
        return vector;

    }

    /**
     * Bloco 5, ex.1-t)
     *
     * @return a Vector object that contains elements which percentage of even digits is higher than the average
     * percentage of even digits of an existing object in the Vector class
     */

    public Vector getElementsWithPercentageOfEvensHigherThanAverage() {

        Vector vector = new Vector();

        for (int i = 0; i < this.values.length; i++) {
            if (this.getPercentageOfEvenDigitsArray()[i] > this.getAverageOfPercentageOfEvenDigits()) {
                vector.addValue(this.values[i]);
            }
        }
        return vector;

    }

    /**
     * Bloco 5, ex.1-u)
     *
     * @return a Vector object that contains elements with only even digits and which were retrieved from an existing
     * object in the Vector class
     */

    public Vector getElementsWithOnlyEvenDigits() {

        Vector vector = new Vector();
        for (int i = 0; i < this.values.length; i++) {
            if (this.getPercentageOfEvenDigitsArray()[i] == 100) {
                vector.addValue(this.values[i]);
            }
        }
        return vector;

    }

    /**
     * Bloco 5, ex.1-v)
     *
     * @return a Vector object that contains elements which digits are ascending, and which were retrieved from an
     * existing object in the Vector class
     */

    public Vector getElementsThatAreAscending() {

        Vector vector = new Vector();

        for (int i = 0; i < this.values.length; i++) {
            if (this.checkIfElementIsAscending()[i]) {
                vector.addValue(this.values[i]);
            }
        }
        return vector;

    }

    /**
     * Bloco 5, ex.1-w)
     *
     * @return a Vector object that contains palindromes which were retrieved from an existing object in the Vector
     * class
     */

    public Vector getElementsThatArePalindromes() {
        int[] aux = new int[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            if (this.isPalindromeArray()[i]) {
                aux[i] = this.values[i];
            }
        }

        Vector beforeRemoveValues = new Vector(aux);
        int[] afterRemoveValuesVector = beforeRemoveValues.removeMultipleValues(0);

        if (afterRemoveValuesVector.length > 0) {
            Vector vector = new Vector(afterRemoveValuesVector);
            return vector;
        } else {
            Vector vector = new Vector();
            return vector;
        }

    }

    /**
     * Bloco 5, ex.1-x)
     *
     * @return a Vector object that contains elements which digits are the same, and which were retrieved from an
     * existing object in the Vector
     * class
     */

    //modularizar a parte de remove values (segunda parte deste algoritmo)
    public Vector getElementsWithTheSameDigits() {

        int[] aux = new int[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            if (this.hasOnlySameDigitArray()[i]) {
                aux[i] = this.values[i];
            }
        }

        Vector beforeRemoveValues = new Vector(aux);
        int[] afterRemoveValuesVector = beforeRemoveValues.removeMultipleValues(0);

        if (afterRemoveValuesVector.length > 0) {
            Vector vector = new Vector(afterRemoveValuesVector);
            return vector;
        } else {
            Vector vector = new Vector();
            return vector;
        }

    }

    /**
     * Bloco 5, 1-y)
     *
     * @return a Vector object that contains elements that are Armstrong numbers, and which were retrieved from an
     * existing object in the Vector class
     */

    public Vector getElementsWithArmstrongNumbers() {

        int[] aux = new int[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            if (this.hasArmstrongNumberArray()[i]) {
                aux[i] = this.values[i];
            }
        }

        Vector beforeRemoveValues = new Vector(aux);
        int[] afterRemoveValuesVector = beforeRemoveValues.removeMultipleValues(0);

        if (afterRemoveValuesVector.length > 0) {
            Vector vector = new Vector(afterRemoveValuesVector);
            return vector;
        } else {
            Vector vector = new Vector();
            return vector;
        }

    }

    /**
     * Bloco 5, 1-z)
     *
     * @param n an int value that is the minimum number of ascending digits
     * @return a Vector object that contains elements which digits are ascending (at least n digits), and which were
     * retrieved from an
     * * existing object in the Vector class
     */

    public Vector getElementsThatAreAscendingWithNDigits(int n) {

        Vector vector = new Vector();

        for (int i = 0; i < this.values.length; i++) {
            if (this.checkIfElementIsAscendingWithNDigits(n)[i]) {
                vector.addValue(this.values[i]);
            }
        }
        return vector;


    }

    /**
     * Bloco 5, 1-aa)
     *
     * @param object any type of object (object type)
     * @return true if the given object is equal to the vector object of the Vector class, false if it isn't
     */
    public boolean isEqualToVector(Object object) {

        if (object == null) {
            return false;
        } else if (object == this) {
            return true;
        } else {
            if (object.getClass() != this.getClass()) {
                return false;
            } else {
                Vector otherVector = (Vector) object;
                if (this.values.length != otherVector.returnNumberOfElements()) {
                    return false;
                } else {
                    boolean isEqual = true;
                    for (int i = 0; i < this.values.length && isEqual; i++) {
                        if (otherVector.returnValueByIndex(i) != this.values[i]) {
                            isEqual = false;
                        }
                    }
                    return isEqual;
                }
            }
        }
    }

    /**
     * Public method that removes the repeated elements of a vector object in the Vector class, keeping only one
     * instance of the repeated element
     *
     * @return a Vector type of array
     */
    public Vector removeRepeatedElements() {

        Vector vector = new Vector();

        for (int i = 0; i < this.values.length; i++) {

            boolean found = false;

            for (int j = 0; j < vector.returnNumberOfElements() && !found; j++) {
                if (this.values[i] == vector.returnValueByIndex(j)) {
                    found = true;
                }
            }

            if (!found) {
                vector.addValue(this.values[i]);
            }

        }

        return vector;

    }


    /**
     * A public method that returns an array with the number of digits of each corresponding element in the vector
     * object of the Vector class
     *
     * @return an int array
     */
    public int[] getNrOfDigitsArray() {
        int[] array = new int[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            int partial = Math.abs(this.values[i]), countDigits = 0;

            if (partial > 0) {
                while (partial > 0) {
                    countDigits += 1;
                    partial /= 10;
                }
            } else if (partial == 0) {
                countDigits += 1;
            }

            array[i] = countDigits;
        }

        return array;
    }

    public Vector revertOrder() {

        Vector newLine = new Vector();

        for (int i = this.values.length-1; i >= 0; i--) {
            newLine.addValue(this.values[i]);
        }

        return newLine;

    }


    //OPERATIONS - PRIVATE METHODS

    //modularizar algoritmo de extração e contagem de dígitos

    /**
     * Private method that copies values from an array to a copy array
     *
     * @param values an array of int type
     * @param length the length of the copy array (int value)
     * @return an int array that is a copy of the array that was passed through this method
     */
    private int[] copyValuesFromArray(int[] values, int length) {

        int[] aux = new int[length];

        for (int i = 0; (i < length) && (i < values.length); i++) {
            aux[i] = values[i];
        }

        int[] copyArray = new int[aux.length];

        for (int i = 0; i < aux.length; i++) {
            copyArray[i] = aux[i];
        }

        return copyArray;
    }

    private int[] copyValuesFromArrayWithStartPosition(int[] values, int length, int startPosition) {

        int[] copyArray = new int[length];
        int i, k = 0;

        for (i = startPosition; i < values.length; i++) {
            copyArray[k] = values[i];
            k++;
        }

        return copyArray;

    }

    private int[] joinArray(int[] array_1, int[] array_2) {
        int[] joinedArray = null;

        if (array_1 != null && array_2 != null) {
            joinedArray = new int[array_1.length + array_2.length];
            int k = array_1.length;

            for (int i = 0; i < array_1.length; i++) {
                joinedArray[i] = array_1[i];
            }

            for (int j = 0; j < array_2.length; j++) {
                joinedArray[k] = array_2[j];
                k++;
            }
        }

        return joinedArray;
    }

    private int countNumberOfEvenElements() {
        int count = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] % 2 == 0 && this.values[i] != 0) {
                count += 1;
            }
        }

        return count;
    }

    private int countNumberOfOddElements() {
        int count = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] % 2 != 0) {
                count += 1;
            }
        }

        return count;
    }

    private int countUniqueElements() {
        int count = 0;

        for (int i = 0; i < this.values.length; i++) {
            for (int j = 0; j < this.values.length; j++) {
                if (this.values[i] == this.values[j]) {
                    count += 1;
                }
            }
        }

        return count;
    }

    private double getAverageOfNrOfDigits() {
        int sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            int partial = Math.abs(this.values[i]), countDigits = 0;

            if (partial > 0) {
                while (partial > 0) {
                    countDigits += 1;
                    partial /= 10;
                }
            } else if (partial == 0) {
                countDigits += 1;
            }

            sum += countDigits;
        }

        double average = Math.ceil((double) sum / this.values.length);

        return average;

    }

    private double getAverageOfPercentageOfEvenDigits() {
        double sum = 0;

        for (int i = 0; i < this.values.length; i++) {
            int partial = this.values[i], countDigits = 0, digit, countEvenDigits = 0;

            if (partial > 0) {
                while (partial > 0) {
                    countDigits += 1;
                    digit = partial % 10;
                    if (digit % 2 == 0 && digit != 0) {
                        countEvenDigits += 1;
                    }
                    partial /= 10;
                }
            } else if (partial == 0) {
                countDigits = 1;
                countEvenDigits = 0;
            } else {
                while (partial < 0) {
                    countDigits += 1;
                    digit = partial % 10;
                    if (digit % 2 == 0 && digit != 0) {
                        countEvenDigits += 1;
                    }
                    partial /= 10;
                }
            }
            sum += (double) countEvenDigits / countDigits;
        }

        double average = Math.ceil((sum / this.values.length) * 100);

        return average;
    }

    public double[] getPercentageOfEvenDigitsArray() {
        double[] array = new double[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            int partial = Math.abs(this.values[i]), countDigits = 0, digit, countEvenDigits = 0;

            if (partial > 0) {
                while (partial > 0) {
                    countDigits += 1;
                    digit = partial % 10;
                    if (digit % 2 == 0 && digit != 0) {
                        countEvenDigits += 1;
                    }
                    partial /= 10;
                }
            } else if (partial == 0) {
                countDigits = 1;
                countEvenDigits = 0;
            }

            array[i] = Math.ceil(((double) countEvenDigits / countDigits) * 100);
        }

        return array;

    }

    private int[] removeMultipleValues(int value) {
        int count = 0;

        for (int i = 0; i < this.values.length; i++) {
            if (this.values[i] != value) {
                count += 1;
            }
        }

        if (count > 0) {
            int[] reducedArray = new int[count];
            int j = 0;
            for (int i = 0; i < this.values.length; i++) {
                if (this.values[i] != value) {
                    reducedArray[j] = this.values[i];
                    j++;
                }
            }
            return reducedArray;
        } else {
            int[] reducedArray = new int[0];
            return reducedArray;
        }
    }

    private Vector valueToArray(int value) {

        int partial = Math.abs(value);
        Vector digitsArray = new Vector();

        if (partial > 0) {
            while (partial > 0) {
                digitsArray.addValue(partial % 10);
                partial /= 10;
            }
        } else if (partial == 0) {
            digitsArray.addValue(0);
        }

        return digitsArray;

    }

    private boolean[] checkIfElementIsAscending() {

        boolean[] result = new boolean[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            result[i] = true;
            Vector digitsArray = this.valueToArray(this.values[i]);

            if (digitsArray.values.length > 1) {
                for (int j = 0; j < digitsArray.values.length - 1 && result[i]; j++) {
                    if (Math.abs(digitsArray.values[j]) <= Math.abs(digitsArray.values[j + 1])) {
                        result[i] = false;
                    }
                }
            } else {
                result[i] = false;
            }
        }

        return result;
    }

    private boolean[] isPalindromeArray() {

        boolean[] result = new boolean[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            result[i] = true;
            Vector array = this.valueToArray(this.values[i]);
            int[] reverseArray = array.getReverseArray();

            if (array.values.length == 1) {
                result[i] = true;
            } else {
                for (int j = 0; j < array.values.length && result[i]; j++) {
                    if (array.values[j] != reverseArray[j]) {
                        result[i] = false;
                    }
                }
            }
        }

        return result;
    }

    private int[] getReverseArray() {

        int[] reverse = new int[this.values.length];
        int j = 0;

        for (int i = 0; i < this.values.length; i++) {
            reverse[i] = this.values[this.values.length - 1 - j];
            j++;
        }

        return reverse;
    }

    private boolean[] hasOnlySameDigitArray() {

        boolean[] result = new boolean[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            Vector digitsArray = this.valueToArray(this.values[i]);

            if (digitsArray.checkIfElementHasOnlySameDigit()) {
                result[i] = true;
            } else {
                result[i] = false;
            }
        }

        return result;
    }

    private boolean checkIfElementHasOnlySameDigit() {
        boolean result = true;
        int firstValue = this.values[0];

        for (int i = 0; i < this.values.length && result; i++) {
            if (this.values[i] != this.values[0]) {
                result = false;
            }
        }

        return result;
    }

    private boolean[] hasArmstrongNumberArray() {

        boolean[] result = new boolean[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            if (this.checkIfElementIsAmstrongNumber(this.values[i])) {
                result[i] = true;
            } else {
                result[i] = false;
            }
        }

        return result;

    }

    private boolean checkIfElementIsAmstrongNumber(int value) {

        int remainder, sum_pow = 0, partial = Math.abs(value);

        if (partial == 0) {
            return false;
        } else {
            while (partial > 0) {
                remainder = partial % 10;
                partial /= 10.0;
                sum_pow += Math.pow(remainder, 3);
            }

            if (sum_pow == Math.abs(value)) {
                return true;
            } else {
                return false;
            }
        }

    }

    private boolean[] checkIfElementIsAscendingWithNDigits(int value) {

        boolean[] result = new boolean[this.values.length];

        for (int i = 0; i < this.values.length; i++) {
            result[i] = true;
            Vector digitsArray = this.valueToArray(this.values[i]);

            if (digitsArray.values.length >= value) {
                for (int j = 0; j < digitsArray.values.length - 1 && result[i]; j++) {
                    if (Math.abs(digitsArray.values[j]) <= Math.abs(digitsArray.values[j + 1])) {
                        result[i] = false;
                    }
                }
            } else {
                result[i] = false;
            }
        }

        return result;


    }

}
