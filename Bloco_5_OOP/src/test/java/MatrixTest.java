import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class MatrixTest {

    @Test
    void callEmptyConstructor() {

        //Arrange
        int expectedLength = 0;
        Vector[] expectedVector = new Vector[0];

        //Act
        Matrix result = new Matrix();

        //Assert
        assertEquals(expectedLength, result.toVectorArray().length);
        assertArrayEquals(expectedVector,result.toVectorArray());

    }

    @Test
    void callNonEmptyConstructor() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});
        Vector[] vectorList = {vector_1,vector_2,vector_3};

        Vector[] expected = {vector_1,vector_2,vector_3};
        Matrix expectedMatrix = new Matrix(expected);


        //Act
        Matrix matrix_1 = new Matrix(vectorList);
        Vector[] result = matrix_1.toVectorArray();
        Matrix resultMatrix = new Matrix(result);

        //Assert
        assertEquals(expected.length,result.length);
        assertArrayEquals(expectedMatrix.toIntBiArray(),resultMatrix.toIntBiArray());
        assertTrue(resultMatrix.isEqualToMatrix(expectedMatrix));
    }

    @Test
    void createMatrixWithEmptyVector() {

        //Arrange
        Vector[] vector = new Vector[0];

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()->{new Matrix(vector);});

    }

    @Test
    void createMatrixWithNullVector() {

        //Arrange
        Vector[] vector = null;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()->{new Matrix(vector);});

    }

    @Test
    void addValueToMatrixLine_OutOfBoundsBecauseIsHigherThanMatrixLength() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int value = 8;
        int lineNumber = 3;

        //Act+Assert
        assertThrows(IndexOutOfBoundsException.class,()->{matrix_1.addValueToMatrixLine(value,lineNumber);});

    }

    @Test
    void addValueToMatrixLine_OutOfBoundsBecauseIsLowerThanZero() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int value = 8;
        int lineNumber = -1;

        //Act+Assert
        assertThrows(IndexOutOfBoundsException.class,()->{matrix_1.addValueToMatrixLine(value,lineNumber);});

    }

    @Test
    void addValueToMatrixLine_ValidLine() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int value = 8;
        int lineNumber = 1;

        //Expected Vector[]
        Vector vector_2_alt = new Vector(new int[]{-5,0,2,1,8});
        Vector[] expectedVector = {vector_1,vector_2_alt,vector_3};
        Matrix expectedMatrix = new Matrix(expectedVector);

        //Act
        matrix_1.addValueToMatrixLine(value,lineNumber);

        //Assert
        assertEquals(expectedMatrix.toVectorArray().length,matrix_1.toVectorArray().length);
        assertArrayEquals(expectedMatrix.toIntBiArray(),matrix_1.toIntBiArray());

    }

    @Test
    void removeValueFromMatrixAtFirstMatch_NoMatch() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int value = 10;
        Vector[] expected = {vector_1,vector_2,vector_3};
        Matrix expectedMatrix = new Matrix(expected);

        //Act
        boolean result = matrix_1.removeValueFromMatrixAtFirstMatch(value);

        //Assert
        assertFalse(result);
        assertTrue(expectedMatrix.isEqualToMatrix(matrix_1));

    }

    @Test
    void removeValueFromMatrixAtFirstMatch_OneMatch() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int value = -5;
        Vector vector_2_altered = new Vector(new int[]{0,2,1});
        Vector[] expected = {vector_1,vector_2_altered,vector_3};
        Matrix expectedMatrix = new Matrix(expected);

        //Act
        boolean result = matrix_1.removeValueFromMatrixAtFirstMatch(value);

        //Assert
        assertTrue(result);
        assertTrue(expectedMatrix.isEqualToMatrix(matrix_1));

    }

    @Test
    void removeValueFromMatrixAtFirstMatch_MultipleMatch() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,2,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int value = 2;
        Vector vector_1_altered = new Vector(new int[]{1,3});
        Vector[] expected = {vector_1_altered,vector_2,vector_3};
        Matrix expectedMatrix = new Matrix(expected);

        //Act
        boolean result = matrix_1.removeValueFromMatrixAtFirstMatch(value);

        //Assert
        assertTrue(result);
        assertTrue(expectedMatrix.isEqualToMatrix(matrix_1));

    }

    @Test
    void isEmptyMatrix_True() {

        //Arrange+Act
        Matrix matrix = new Matrix();

        //Assert
        assertTrue(matrix.isEmptyMatrix());

    }

    @Test
    void isEmptyMatrix_False() {

        //Arrange+Act
        Vector vector = new Vector(new int[]{1,2,3});
        Vector[] vectorList = {vector};
        Matrix matrix = new Matrix(vectorList);

        //Assert
        assertFalse(matrix.isEmptyMatrix());

    }

    @Test
    void highestElementOfMatrix_HighestValueIsUnique() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 6;

        //Act
        int result = matrix_1.highestElementOfMatrix();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void highestElementOfMatrix_HighestValueIsNotUnique() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,3,1});
        Vector vector_3 = new Vector(new int[]{2,3});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 3;

        //Act
        int result = matrix_1.highestElementOfMatrix();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void highestElementOfMatrix_LowestValueIsUnique() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = -5;

        //Act
        int result = matrix_1.lowestElementOfMatrix();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void highestElementOfMatrix_LowestValueIsNotUnique() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,-10,1});
        Vector vector_3 = new Vector(new int[]{2,-10});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = -10;

        //Act
        int result = matrix_1.lowestElementOfMatrix();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getAverageOfMatrix_MultipleLines() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        double expected = 1.11;

        //Act
        double result = matrix_1.getAverageOfMatrix();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfMatrix_OneLine() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector[] vectorList = {vector_1};
        Matrix matrix_1 = new Matrix(vectorList);

        double expected = 2;

        //Act
        double result = matrix_1.getAverageOfMatrix();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void vectorWithSumOfMatrixLines_EmptyMatrix() {

        //Arrange
        Matrix matrix_1 = new Matrix();
        Vector expected = new Vector();

        //Act
        Vector result = matrix_1.vectorWithSumOfMatrixLines();

        //Assert
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void vectorWithSumOfMatrixLines_MultipleLines() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{6,2,2});

        //Act
        Vector result = matrix_1.vectorWithSumOfMatrixLines();

        //Assert
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void vectorWithSumOfMatrixLines_OneLine() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector[] vectorList = {vector_1};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{6});

        //Act
        Vector result = matrix_1.vectorWithSumOfMatrixLines();

        //Assert
        assertArrayEquals(expected.toArray(),result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void vectorWithSumOfMatrixColumns_EmptyMatrix() {

        //Arrange
        Matrix matrix_1 = new Matrix();
        Vector expected = new Vector();

        //Act
        Vector result = matrix_1.vectorWithSumOfMatrixColumns();

        //Assert
        assertArrayEquals(expected.toArray(),result.toArray());

    }

    @Test
    void vectorWithSumOfMatrixColumns_SameNumberOfColumns() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0,10,-5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{-2,2,19,0});

        //Act
        Vector result = matrix_1.vectorWithSumOfMatrixColumns();

        //Assert
        assertArrayEquals(expected.toArray(),result.toArray());

    }

    @Test
    void vectorWithSumOfMatrixColumns_DifferentNumberOfColumns() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{-2,2,9,1});

        //Act
        Vector result = matrix_1.vectorWithSumOfMatrixColumns();

        //Assert
        assertArrayEquals(expected.toArray(),result.toArray());

    }

    @Test
    void getLineIndexWithHighestSum_EmptyMatrix() {

        //Arrange
        Matrix matrix_1 = new Matrix();
        int expected = -1;

        //Act
        int result = matrix_1.getLineIndexWithHighestSum();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getLineIndexWithHighestSum_MultipleLines() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 0;

        //Act
        int result = matrix_1.getLineIndexWithHighestSum();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getLineIndexWithHighestSum_OneLine() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector[] vectorList = {vector_1};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 0;

        //Act
        int result = matrix_1.getLineIndexWithHighestSum();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void isSquareMatrix_LinesHaveDifferentNumberOfColumns() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.isSquareMatrix();

        //Assert
        assertFalse(result);

    }

    @Test
    void isSquareMatrix_False() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,1});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0,1,4});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.isSquareMatrix();

        //Assert
        assertFalse(result);

    }

    @Test
    void isSquareMatrix_True() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{-5,0,6,1});
        Vector vector_3 = new Vector(new int[]{2,0,2,4});
        Vector vector_4 = new Vector(new int[]{33,21,34,4});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.isSquareMatrix();

        //Assert
        assertTrue(result);

    }

    @Test
    void isSquareSymmetricMatrix_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,1,-1});
        Vector vector_2 = new Vector(new int[]{1,2,0});

        Vector[] vectorList = {vector_1,vector_2};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.isSquareSymmetricMatrix();

        //Assert
        assertFalse(result);

    }

    @Test
    void isSquareSymmetricMatrix_True() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,1,-1});
        Vector vector_2 = new Vector(new int[]{1,2,0});
        Vector vector_3 = new Vector(new int[]{-1,0,5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.isSquareSymmetricMatrix();

        //Assert
        assertTrue(result);

    }

    @Test
    void isSquareSymmetricMatrix_False() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,3,-1});
        Vector vector_2 = new Vector(new int[]{1,2,0});
        Vector vector_3 = new Vector(new int[]{-1,1,5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.isSquareSymmetricMatrix();

        //Assert
        assertFalse(result);

    }

    @Test
    void getNumberOfNonZeroElementsFromMatrixMainDiagonal_NotASquareMatrix() {

        Vector vector_1 = new Vector(new int[]{0,3,-1});
        Vector vector_2 = new Vector(new int[]{1,0,0});

        Vector[] vectorList = {vector_1,vector_2};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = -1;

        //Act
        int result = matrix_1.getNumberOfNonZeroElementsFromMatrixMainDiagonal();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getNumberOfNonZeroElementsFromMatrixMainDiagonal_NoElements() {

        Vector vector_1 = new Vector(new int[]{0,3,-1});
        Vector vector_2 = new Vector(new int[]{1,0,0});
        Vector vector_3 = new Vector(new int[]{-1,1,0});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 0;

        //Act
        int result = matrix_1.getNumberOfNonZeroElementsFromMatrixMainDiagonal();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getNumberOfNonZeroElementsFromMatrixMainDiagonal_OneElement() {

        Vector vector_1 = new Vector(new int[]{0,3,-1});
        Vector vector_2 = new Vector(new int[]{1,0,0});
        Vector vector_3 = new Vector(new int[]{-1,1,5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 1;

        //Act
        int result = matrix_1.getNumberOfNonZeroElementsFromMatrixMainDiagonal();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getNumberOfNonZeroElementsFromMatrixMainDiagonal_MultipleElements() {

        Vector vector_1 = new Vector(new int[]{10,3,-1,2});
        Vector vector_2 = new Vector(new int[]{1,0,0,3});
        Vector vector_3 = new Vector(new int[]{-1,1,5,5});
        Vector vector_4 = new Vector(new int[]{22,0,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        int expected = 3;

        //Act
        int result = matrix_1.getNumberOfNonZeroElementsFromMatrixMainDiagonal();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void checkIfMainAndSecondaryDiagonalAreEqual_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,3,-1,2});
        Vector vector_2 = new Vector(new int[]{1,0,0,3});
        Vector vector_3 = new Vector(new int[]{-1,1,5,5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.checkIfMainAndSecondaryDiagonalAreEqual();

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfMainAndSecondaryDiagonalAreEqual_False() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,3,-1,2});
        Vector vector_2 = new Vector(new int[]{1,0,0,3});
        Vector vector_3 = new Vector(new int[]{-1,1,5,5});
        Vector vector_4 = new Vector(new int[]{22,0,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.checkIfMainAndSecondaryDiagonalAreEqual();

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfMainAndSecondaryDiagonalAreEqual_True() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,3,-1,10});
        Vector vector_2 = new Vector(new int[]{1,0,0,3});
        Vector vector_3 = new Vector(new int[]{-1,5,5,5});
        Vector vector_4 = new Vector(new int[]{34,0,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        //Act
        boolean result = matrix_1.checkIfMainAndSecondaryDiagonalAreEqual();

        //Assert
        assertTrue(result);

    }

    @Test
    void getElementsFromMatrixWithNrOfDigitsHigherThanAverage_NoElements() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,30,-10,10});
        Vector vector_2 = new Vector(new int[]{10,50,60,30});
        Vector vector_3 = new Vector(new int[]{-10,50,50,50});
        Vector vector_4 = new Vector(new int[]{34,11,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector();

        //Act
        Vector result = matrix_1.getElementsFromMatrixWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void getElementsFromMatrixWithNrOfDigitsHigherThanAverage_NotASquareMatrix_SeveralElements() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{100,3,-1,10});
        Vector vector_2 = new Vector(new int[]{100,0,0,3});
        Vector vector_3 = new Vector(new int[]{-1,5,5000,5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{100,5000});

        //Act
        Vector result = matrix_1.getElementsFromMatrixWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void getElementsFromMatrixWithNrOfDigitsHigherThanAverage_SquareMatrix_SeveralElements() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{100,3,-1,10});
        Vector vector_2 = new Vector(new int[]{100,0,0,3});
        Vector vector_3 = new Vector(new int[]{-1,5,5000,5});
        Vector vector_4 = new Vector(new int[]{34,0,12,340});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{100,5000,340});

        //Act
        Vector result = matrix_1.getElementsFromMatrixWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void getElementsFromMatrixWithPercentageOfEvensHigherThanAverage_SquareMatrix_NoElements() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{36,36,-18,12});
        Vector vector_2 = new Vector(new int[]{18,60,40,34});
        Vector vector_3 = new Vector(new int[]{-16,56,58,54});
        Vector vector_4 = new Vector(new int[]{34,14,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector();

        //Act
        Vector result = matrix_1.getElementsFromMatrixWithPercentageOfEvensHigherThanAverage();

        //Assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void getElementsFromMatrixWithPercentageOfEvensHigherThanAverage_NotASquareMatrix_SeveralElements() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{100,12,-1,12});
        Vector vector_2 = new Vector(new int[]{100,0,0,3});
        Vector vector_3 = new Vector(new int[]{-166,56,5000,5});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{12,-166,56});

        //Act
        Vector result = matrix_1.getElementsFromMatrixWithPercentageOfEvensHigherThanAverage();

        //Assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void getElementsFromMatrixWithPercentageOfEvensHigherThanAverage_SquareMatrix_SeveralElements() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{100,36,-1,12});
        Vector vector_2 = new Vector(new int[]{100,0,0,3});
        Vector vector_3 = new Vector(new int[]{-166,56,5000,5});
        Vector vector_4 = new Vector(new int[]{34,0,12,340});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector expected = new Vector(new int[]{36,12,-166,56,34,340});

        //Act
        Vector result = matrix_1.getElementsFromMatrixWithPercentageOfEvensHigherThanAverage();

        //Assert
        assertArrayEquals(expected.toArray(), result.toArray());
        assertTrue(expected.isEqualToVector(result));

    }

    @Test
    void invertOrderOfMatrixLines_EmptyMatrix() {

        //Arrange
        Matrix matrix_1 = new Matrix();
        Matrix matrixInv = matrix_1;

        //Act
        matrix_1.invertOrderOfMatrixLines();

        //Assert
        assertTrue(matrixInv.isEqualToMatrix(matrix_1));

    }

    @Test
    void invertOrderOfMatrixLines_SquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,30,-10,10});
        Vector vector_2 = new Vector(new int[]{10,50,60,30});
        Vector vector_3 = new Vector(new int[]{-10,50,50,50});
        Vector vector_4 = new Vector(new int[]{34,11,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_inv = new Vector(new int[]{10,-10,30,10});
        Vector vector_2_inv = new Vector(new int[]{30,60,50,10});
        Vector vector_3_inv = new Vector(new int[]{50,50,50,-10});
        Vector vector_4_inv = new Vector(new int[]{34,12,11,34});

        Vector[] vectorListInv = {vector_1_inv,vector_2_inv,vector_3_inv,vector_4_inv};
        Matrix matrixInv = new Matrix(vectorListInv);

        //Act
        matrix_1.invertOrderOfMatrixLines();

        //Assert
        assertTrue(matrixInv.isEqualToMatrix(matrix_1));

    }

    @Test
    void invertOrderOfMatrixLines_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,30,-10,10});
        Vector vector_2 = new Vector(new int[]{10,50});
        Vector vector_3 = new Vector(new int[]{-10,50,51,100});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_inv = new Vector(new int[]{10,-10,30,10});
        Vector vector_2_inv = new Vector(new int[]{50,10});
        Vector vector_3_inv = new Vector(new int[]{100,51,50,-10});

        Vector[] vectorListInv = {vector_1_inv,vector_2_inv,vector_3_inv};
        Matrix matrixInv = new Matrix(vectorListInv);

        //Act
        matrix_1.invertOrderOfMatrixLines();

        //Assert
        assertTrue(matrixInv.isEqualToMatrix(matrix_1));

    }

    @Test
    void invertOrderOfMatrixColumns_EmptyMatrix() {

        //Arrange
        Matrix matrix_1 = new Matrix();
        Matrix matrixInv = new Matrix();

        //Act
        matrix_1.invertOrderOfMatrixColumns();

        //Assert
        assertTrue(matrixInv.isEqualToMatrix(matrix_1));

    }

    @Test
    void invertOrderOfMatrixColumns_SquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,30,-10,10});
        Vector vector_2 = new Vector(new int[]{10,50,60,30});
        Vector vector_3 = new Vector(new int[]{-10,50,50,50});
        Vector vector_4 = new Vector(new int[]{34,11,12,34});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_inv = new Vector(new int[]{34,11,12,34});
        Vector vector_2_inv = new Vector(new int[]{-10,50,50,50});
        Vector vector_3_inv = new Vector(new int[]{10,50,60,30});
        Vector vector_4_inv = new Vector(new int[]{10,30,-10,10});

        Vector[] vectorListInv = {vector_1_inv,vector_2_inv,vector_3_inv,vector_4_inv};
        Matrix matrixInv = new Matrix(vectorListInv);

        //Act
        matrix_1.invertOrderOfMatrixColumns();

        //Assert
        assertTrue(matrixInv.isEqualToMatrix(matrix_1));

    }

    @Test
    void invertOrderOfMatrixColumns_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{10,30,-10,10});
        Vector vector_2 = new Vector(new int[]{10,50});
        Vector vector_3 = new Vector(new int[]{-10,50,51,100});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_inv = new Vector(new int[]{-10,50,51,100});
        Vector vector_2_inv = new Vector(new int[]{10,50});
        Vector vector_3_inv = new Vector(new int[]{10,30,-10,10});

        Vector[] vectorListInv = {vector_1_inv,vector_2_inv,vector_3_inv};
        Matrix matrixInv = new Matrix(vectorListInv);

        //Act
        matrix_1.invertOrderOfMatrixColumns();

        //Assert
        assertTrue(matrixInv.isEqualToMatrix(matrix_1));

    }

    @Test
    void rotateNinetyDegreesPositive_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{5,6,7,8});
        Vector vector_3 = new Vector(new int[]{9,10,11,12});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_rot = new Vector(new int[]{1,2,3,4});
        Vector vector_2_rot = new Vector(new int[]{5,6,7,8});
        Vector vector_3_rot = new Vector(new int[]{9,10,11,12});

        Vector[] vectorListRot = {vector_1_rot,vector_2_rot,vector_3_rot};
        Matrix matrixRot = new Matrix(vectorListRot);

        //Act
        matrix_1.rotateNinetyDegreesPositive();

        //Assert
        assertTrue(matrix_1.isEqualToMatrix(matrixRot));

    }
    @Test
    void rotateNinetyDegreesPositive_SquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{5,6,7,8});
        Vector vector_3 = new Vector(new int[]{9,10,11,12});
        Vector vector_4 = new Vector(new int[]{13,14,15,16});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_rot = new Vector(new int[]{13,9,5,1});
        Vector vector_2_rot = new Vector(new int[]{14,10,6,2});
        Vector vector_3_rot = new Vector(new int[]{15,11,7,3});
        Vector vector_4_rot = new Vector(new int[]{16,12,8,4});

        Vector[] vectorListRot = {vector_1_rot,vector_2_rot,vector_3_rot,vector_4_rot};
        Matrix matrixRot = new Matrix(vectorListRot);

        //Act
        matrix_1.rotateNinetyDegreesPositive();

        //Assert
        assertTrue(matrix_1.isEqualToMatrix(matrixRot));

    }

    @Test
    void rotateOneHundredAndEightyDegrees_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{5,6,7,8});
        Vector vector_3 = new Vector(new int[]{9,10,11,12});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_rot = new Vector(new int[]{1,2,3,4});
        Vector vector_2_rot = new Vector(new int[]{5,6,7,8});
        Vector vector_3_rot = new Vector(new int[]{9,10,11,12});

        Vector[] vectorListRot = {vector_1_rot,vector_2_rot,vector_3_rot};
        Matrix matrixRot = new Matrix(vectorListRot);

        //Act
        matrix_1.rotateOneHundredAndEightyDegrees();

        //Assert
        assertTrue(matrix_1.isEqualToMatrix(matrixRot));

    }

    @Test
    void rotateOneHundredAndEightyDegrees_SquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{5,6,7,8});
        Vector vector_3 = new Vector(new int[]{9,10,11,12});
        Vector vector_4 = new Vector(new int[]{13,14,15,16});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_rot = new Vector(new int[]{16,15,14,13});
        Vector vector_2_rot = new Vector(new int[]{12,11,10,9});
        Vector vector_3_rot = new Vector(new int[]{8,7,6,5});
        Vector vector_4_rot = new Vector(new int[]{4,3,2,1});

        Vector[] vectorListRot = {vector_1_rot,vector_2_rot,vector_3_rot,vector_4_rot};
        Matrix matrixRot = new Matrix(vectorListRot);

        //Act
        matrix_1.rotateOneHundredAndEightyDegrees();

        //Assert
        assertTrue(matrix_1.isEqualToMatrix(matrixRot));

    }

    @Test
    void rotateNinetyDegreesNegative_NotASquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{5,6,7,8});
        Vector vector_3 = new Vector(new int[]{9,10,11,12});

        Vector[] vectorList = {vector_1,vector_2,vector_3};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_rot = new Vector(new int[]{1,2,3,4});
        Vector vector_2_rot = new Vector(new int[]{5,6,7,8});
        Vector vector_3_rot = new Vector(new int[]{9,10,11,12});

        Vector[] vectorListRot = {vector_1_rot,vector_2_rot,vector_3_rot};
        Matrix matrixRot = new Matrix(vectorListRot);

        //Act
        matrix_1.rotateNinetyDegreesNegative();

        //Assert
        assertTrue(matrix_1.isEqualToMatrix(matrixRot));

    }

    @Test
    void rotateNinetyDegreesNegative_SquareMatrix() {

        //Arrange
        Vector vector_1 = new Vector(new int[]{1,2,3,4});
        Vector vector_2 = new Vector(new int[]{5,6,7,8});
        Vector vector_3 = new Vector(new int[]{9,10,11,12});
        Vector vector_4 = new Vector(new int[]{13,14,15,16});

        Vector[] vectorList = {vector_1,vector_2,vector_3,vector_4};
        Matrix matrix_1 = new Matrix(vectorList);

        Vector vector_1_rot = new Vector(new int[]{4,8,12,16});
        Vector vector_2_rot = new Vector(new int[]{3,7,11,15});
        Vector vector_3_rot = new Vector(new int[]{2,6,10,14});
        Vector vector_4_rot = new Vector(new int[]{1,5,9,13});

        Vector[] vectorListRot = {vector_1_rot,vector_2_rot,vector_3_rot,vector_4_rot};
        Matrix matrixRot = new Matrix(vectorListRot);

        //Act
        matrix_1.rotateNinetyDegreesNegative();

        //Assert
        assertTrue(matrix_1.isEqualToMatrix(matrixRot));


    }
}