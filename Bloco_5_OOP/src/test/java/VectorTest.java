import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VectorTest {

    @org.junit.jupiter.api.Test
    void createEmptyArray_EmptyConstructor () {

        //Arrange
        Vector vector = new Vector();
        int expected = 0;

        //Act
        int[] result = vector.toArray();

        //Assert
        assertEquals(expected,result.length);

    }

    @org.junit.jupiter.api.Test
    void createValidArray () {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int expected = 7;

        //Act
        int[] result = vector.toArray();

        //Assert
        assertEquals(expected,result.length);
        assertArrayEquals(values,result);
        assertNotSame(values,result);

    }

    @org.junit.jupiter.api.Test
    void createEmptyArray_NonEmptyConstructor () {

        //Arrange
        int[] array = null;

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()->{new Vector(array);});
    }

    @org.junit.jupiter.api.Test
    void createNullArray () {

        //Arrange
        int[] array = {};

        //Act+Assert
        assertThrows(IllegalArgumentException.class, ()->{new Vector(array);});
    }

    @Test
    void addValue() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int value = 8;
        int[] expected = {2,1,4,5,0,-2,1,8};

        //Act
        vector.addValue(value);
        int[] resultArray = vector.toArray();

        //Assert
        assertEquals(expected.length,resultArray.length);
        assertArrayEquals(expected,resultArray);

    }

    @Test
    void removeValueAtFirstMarch_NoMatch() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int value = 3;
        int[] expected = {2,1,4,5,0,-2,1};

        //Act
        vector.removeValueAtFirstMarch(value);
        int[] resultArray = vector.toArray();

        //Assert
        assertEquals(expected.length,resultArray.length);
        assertArrayEquals(expected,resultArray);

    }

    @Test
    void removeValueAtFirstMarch_MoreThanOneMatch() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int value = 1;
        int[] expected = {2,4,5,0,-2,1};

        //Act
        vector.removeValueAtFirstMarch(value);
        int[] resultArray = vector.toArray();

        //Assert
        assertEquals(expected.length,resultArray.length);
        assertArrayEquals(expected,resultArray);

    }

    @Test
    void removeValueAtFirstMarch_JustOneMatch() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int value = -2;
        int[] expected = {2,1,4,5,0,1};

        //Act
        vector.removeValueAtFirstMarch(value);
        int[] resultArray = vector.toArray();

        //Assert
        assertEquals(expected.length,resultArray.length);
        assertArrayEquals(expected,resultArray);

    }

    @Test
    void toArray_OneElement() {

        //Arrange
        int[] values = {3};
        Vector vector = new Vector(values);
        int[] expected = {3};

        //Act
        int[] result = vector.toArray();

        //Assert
        assertEquals(expected.length,result.length);
        assertArrayEquals(expected,result);
        assertNotSame(values,result);

    }

    @Test
    void toArray_SeveralElements() {

        //Arrange
        int[] values = {2,3,1,2};
        Vector vector = new Vector(values);
        int[] expected = {2,3,1,2};

        //Act
        int[] result = vector.toArray();

        //Assert
        assertEquals(expected.length,result.length);
        assertArrayEquals(expected,result);
        assertNotSame(values,result);

    }

    @Test
    void returnValueByIndex_IndexLargerThanArrayLength() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int index = 9;

        //Act+Assert
        assertThrows(ArrayIndexOutOfBoundsException.class, ()->{
            vector.returnValueByIndex(index);});

    }

    @Test
    void returnValueByIndex_IndexLowerThanArrayLength() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int index = -1;

        //Act+Assert
        assertThrows(ArrayIndexOutOfBoundsException.class, ()->{
            vector.returnValueByIndex(index);});

    }

    @Test
    void returnValueByIndex_IndexWithinArrayLength() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int index = 3;
        int expected = 5;

        //Act
        int result = vector.returnValueByIndex(index);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void returnNumberOfElements_OneElement() {

        //Arrange
        int[] values = {1};
        Vector vector = new Vector(values);
        int expected = 1;

        //Act
        int result = vector.returnNumberOfElements();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void returnNumberOfElements_SeveralElements() {

        //Arrange
        int[] values = {2,1,4,5};
        Vector vector = new Vector(values);
        int expected = 4;

        //Act
        int result = vector.returnNumberOfElements();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void arrayHasOneElement_True() {

        //Arrange
        int[] values = {2};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOneElement();

        //Assert
        assertTrue(result);

    }

    @Test
    void arrayHasOneElement_False() {

        //Arrange
        int[] values = {2,4};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOneElement();

        //Assert
        assertFalse(result);

    }

    @Test
    void highestElement_SeveralElements() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int expected = 5;

        //Act
        int result = vector.highestElement();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void lowestElement_SeveralElements() {

        //Arrange
        int[] values = {2,1,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int expected = -2;

        //Act
        int result = vector.lowestElement();

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getAverage_OneElement() {

        //Arrange
        int[] values = {12};
        Vector vector = new Vector(values);
        double expected = 12;

        //Act
        double result = vector.getAverage();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverage_SeveralElements() {

        //Arrange
        int[] values = {2,1,0,12,40,112};
        Vector vector = new Vector(values);
        double expected = 27.83;

        //Act
        double result = vector.getAverage();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverage_NegativeValues() {

        //Arrange
        int[] values = {2,1,0,-12,40,-112};
        Vector vector = new Vector(values);
        double expected = -13.5;

        //Act
        double result = vector.getAverage();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfEvens_NoElements() {

        //Arrange
        int[] values = {1,5,99,0,23};
        Vector vector = new Vector(values);

        //Act+Assert
        assertThrows(ArithmeticException.class,()->{vector.getAverageOfEvens();});

    }

    @Test
    void getAverageOfEvens_OneElement() {

        //Arrange
        int[] values = {1,6,99,0,23};
        Vector vector = new Vector(values);
        double expected = 6;

        //Act
        double result = vector.getAverageOfEvens();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfEvens_SeveralElements() {

        //Arrange
        int[] values = {1,6,22,0,102,113,5};
        Vector vector = new Vector(values);
        double expected = 43.33;

        //Act
        double result = vector.getAverageOfEvens();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfEvens_NegativeValues() {

        //Arrange
        int[] values = {1,6,-22,0,102,113,-5};
        Vector vector = new Vector(values);
        double expected = 28.66;

        //Act
        double result = vector.getAverageOfEvens();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfOdds_NoElements() {

        //Arrange
        int[] values = {10,6,98,0,232};
        Vector vector = new Vector(values);

        //Act+Assert
        assertThrows(ArithmeticException.class,()->{vector.getAverageOfOdds();});

    }

    @Test
    void getAverageOfOdds_OneElement() {

        //Arrange
        int[] values = {4,66,18,3,0};
        Vector vector = new Vector(values);
        double expected = 3;

        //Act
        double result = vector.getAverageOfOdds();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfOdds_SeveralElements() {

        //Arrange
        int[] values = {2,5,22,33,90,0,17};
        Vector vector = new Vector(values);
        double expected = 18.33;

        //Act
        double result = vector.getAverageOfOdds();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfOdds_NegativeValues() {

        //Arrange
        int[] values = {-2,5,22,-33,90,0,17};
        Vector vector = new Vector(values);
        double expected = -3.67;

        //Act
        double result = vector.getAverageOfOdds();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfMultiples_NoMultiples() {

        //Arrange
        int[] values = {25,11,24,15,7,3,8,5};
        Vector vector = new Vector(values);
        int value = 13;
        double expected = -1;

        //Act
        double result = vector.getAverageOfMultiples(value);

        //Act+Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfMultiples_OneMultiple() {

        //Arrange
        int[] values = {25,12,24,15,7,3,9,5};
        Vector vector = new Vector(values);
        int value = 9;
        double expected = 9;

        //Act
        double result = vector.getAverageOfMultiples(value);

        //Act+Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAverageOfMultiples_SeveralMultiples() {

        //Arrange
        int[] values = {25,12,24,15,7,3,9,5};
        Vector vector = new Vector(values);
        int value = 3;
        double expected = 12.6;

        //Act
        double result = vector.getAverageOfMultiples(value);

        //Act+Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getAscendingArray_NonRepeatingElements() {

        //Arrange
        int[] values = {2,3,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int[] expected = {-2,0,1,2,3,4,5};

        //Act
        int[] result = vector.getAscendingArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getAscendingArray_RepeatedElement() {

        //Arrange
        int[] values = {2,2,2,2,2,2,2};
        Vector vector = new Vector(values);
        int[] expected = {2,2,2,2,2,2,2};

        //Act
        int[] result = vector.getAscendingArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getDescendingArray_NonRepeatingElements() {

        //Arrange
        int[] values = {2,3,4,5,0,-2,1};
        Vector vector = new Vector(values);
        int[] expected = {5,4,3,2,1,0,-2};

        //Act
        int[] result = vector.getDescendingArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getDescendingArray_RepeatedElement() {

        //Arrange
        int[] values = {2,2,2,2,2,2,2};
        Vector vector = new Vector(values);
        int[] expected = {2,2,2,2,2,2,2};

        //Act
        int[] result = vector.getDescendingArray();

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void isEmptyArray_True() {

        //Arrange
        Vector vector = new Vector();

        //Act
        boolean result = vector.isEmptyArray();

        //Assert
        assertTrue(result);

    }

    @Test
    void isEmptyArray_False() {

        //Arrange
        int[] values = {2,1,3};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.isEmptyArray();

        //Assert
        assertFalse(result);

    }


    @Test
    void arrayHasOnlyEvenNumbers_True() {

        //Arrange
        int[] values = {2,6,2,44,12,26};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOnlyEvenNumbers();

        //Assert
        assertTrue(result);

    }

    @Test
    void arrayHasOnlyEvenNumbers_FalseBecauseItHasZero() {

        //Arrange
        int[] values = {2,6,0,44,12,26};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOnlyEvenNumbers();

        //Assert
        assertFalse(result);

    }

    @Test
    void arrayHasOnlyEvenNumbers_False() {

        //Arrange
        int[] values = {2,6,1,44,12,26};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOnlyEvenNumbers();

        //Assert
        assertFalse(result);

    }

    @Test
    void arrayHasOnlyOddNumbers_True() {

        //Arrange
        int[] values = {1,77,15,13};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOnlyOddNumbers();

        //Assert
        assertTrue(result);

    }

    @Test
    void arrayHasOnlyOddNumbers_False() {

        //Arrange
        int[] values = {1,77,15,16};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasOnlyOddNumbers();

        //Assert
        assertFalse(result);

    }

    @Test
    void arrayHasRepeatedValues_OnlyOneRepetition() {

        //Arrange
        int[] values = {2,1,9,-1,2};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasRepeatedValues();

        //Assert
        assertTrue(result);

    }

    @Test
    void arrayHasRepeatedValues_SeveralRepetitions() {

        //Arrange
        int[] values = {2,1,9,-1,2,4,9,1};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasRepeatedValues();

        //Assert
        assertTrue(result);

    }

    @Test
    void arrayHasRepeatedValues_NoRepetitions() {

        //Arrange
        int[] values = {2,1,9,-1,5};
        Vector vector = new Vector(values);

        //Act
        boolean result = vector.arrayHasRepeatedValues();

        //Assert
        assertFalse(result);

    }

    @Test
    void getElementsWithNrOfDigitsHigherThanAverage_NoElements() {

        //Arrange
        int[] values = {22,12,11,32,11,22,15,13};
        Vector vector = new Vector(values);
        int[] expected = {};

        //Act
        Vector result = vector.getElementsWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithNrOfDigitsHigherThanAverage_OnlyOneElement() {

        //Arrange
        int[] values = {22,1,0,123,12,3};
        Vector vector = new Vector(values);
        int[] expected = {123};

        //Act
        Vector result = vector.getElementsWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithNrOfDigitsHigherThanAverage_SeveralElements() {

        //Arrange
        int[] values = {22,1,0,123,12,3,221,500};
        Vector vector = new Vector(values);
        int[] expected = {123,221,500};

        //Act
        Vector result = vector.getElementsWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithNrOfDigitsHigherThanAverage_NegativeValues() {

        //Arrange
        int[] values = {22,1,0,123,-12,3,221,-500};
        Vector vector = new Vector(values);
        int[] expected = {123,221,-500};

        //Act
        Vector result = vector.getElementsWithNrOfDigitsHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithPercentageOfEvensHigherThanAverage_NoElements() {

        //Arrange
        int[] values = {22,4,66,22,88,222};
        Vector vector = new Vector(values);
        int[] expected = {};

        //Act
        Vector result = vector.getElementsWithPercentageOfEvensHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithPercentageOfEvensHigherThanAverage_SeveralElements() {

        //Arrange
        int[] values = {22,1,0,123,12,3};
        Vector vector = new Vector(values);
        int[] expected = {22,123,12};

        //Act
        Vector result = vector.getElementsWithPercentageOfEvensHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithPercentageOfEvensHigherThanAverage_NegativeValues() {

        //Arrange
        int[] values = {22,1,0,123,-12,3,221,-500};
        Vector vector = new Vector(values);
        int[] expected = {22,123,-12,221};

        //Act
        Vector result = vector.getElementsWithPercentageOfEvensHigherThanAverage();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithOnlyEvenDigits_NoElements() {

        //Arrange
        int[] values = {23,1,0,123,12,3};
        Vector vector = new Vector(values);
        int[] expected = {};

        //Act
        Vector result = vector.getElementsWithOnlyEvenDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithOnlyEvenDigits_OneElement() {

        //Arrange
        int[] values = {22,1,0,123,12,3};
        Vector vector = new Vector(values);
        int[] expected = {22};

        //Act
        Vector result = vector.getElementsWithOnlyEvenDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithOnlyEvenDigits_SeveralElements() {

        //Arrange
        int[] values = {22,1,44,123,286,3};
        Vector vector = new Vector(values);
        int[] expected = {22,44,286};

        //Act
        Vector result = vector.getElementsWithOnlyEvenDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithOnlyEvenDigits_NegativeValues() {

        //Arrange
        int[] values = {22,1,0,123,-62,3,221,-500};;
        Vector vector = new Vector(values);
        int[] expected = {22,-62};

        //Act
        Vector result = vector.getElementsWithOnlyEvenDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscending_OneElement() {

        //Arrange
        int[] values = {500,2,20,19,280};
        Vector vector = new Vector(values);
        int[] expected = {19};

        //Act
        Vector result = vector.getElementsThatAreAscending();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscending_SeveralElements() {

        //Arrange
        int[] values = {123,0,1,42,62,131,126,321,22};
        Vector vector = new Vector(values);
        int[] expected = {123,126};

        //Act
        Vector result = vector.getElementsThatAreAscending();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscending_NegativeValues() {

        //Arrange
        int[] values = {-4,0,12,160,23,-16};
        Vector vector = new Vector(values);
        int[] expected = {12,23,-16};

        //Act
        Vector result = vector.getElementsThatAreAscending();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatArePalindromes_NoElements() {

        //Arrange
        int[] values = {23,84,122,20,103,24,4666};
        Vector vector = new Vector(values);
        int[] expected = {};

        //Act
        Vector result = vector.getElementsThatArePalindromes();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatArePalindromes_OneElement() {

        //Arrange
        int[] values = {23,84,121,20,103,24,4666};
        Vector vector = new Vector(values);
        int[] expected = {121};

        //Act
        Vector result = vector.getElementsThatArePalindromes();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatArePalindromes_SeveralElements() {

       //Arrange
       int[] values = {22,84,121,9,103,24,44};
       Vector vector = new Vector(values);
       int[] expected = {22,121,9,44};

       //Act
       Vector result = vector.getElementsThatArePalindromes();

       //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatArePalindromes_NegativeElements() {

        //Arrange
        int[] values = {22,-84,121,-9,103,24,-44};
        Vector vector = new Vector(values);
        int[] expected = {22,121,-9,-44};

        //Act
        Vector result = vector.getElementsThatArePalindromes();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithTheSameDigits_NoElements() {

        //Arrange
        int[] values = {223,13,25,62,12};
        Vector vector = new Vector(values);
        int[] expected = {};

        //Act
        Vector result = vector.getElementsWithTheSameDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }


    @Test
    void getElementsWithTheSameDigits_OneElement() {

        //Arrange
        int[] values = {223,13,25,66,12};
        Vector vector = new Vector(values);
        int[] expected = {66};

        //Act
        Vector result = vector.getElementsWithTheSameDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithTheSameDigits_SeveralElements() {

        //Arrange
        int[] values = {222,13,25,66,12,4};
        Vector vector = new Vector(values);
        int[] expected = {222,66,4};

        //Act
        Vector result = vector.getElementsWithTheSameDigits();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithArmstrongNumbers_NoElements() {

        //Arrange
        int[] values = {5,123,1500,39,0};
        Vector vector = new Vector(values);
        int[] expected = {};

        //Act
        Vector result = vector.getElementsWithArmstrongNumbers();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithArmstrongNumbers_OneElement() {

        //Arrange
        int[] values = {5,123,153,39,0};
        Vector vector = new Vector(values);
        int[] expected = {153};

        //Act
        Vector result = vector.getElementsWithArmstrongNumbers();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithArmstrongNumbers_SeveralElements() {

        //Arrange
        int[] values = {5,123,153,39,407, 370,0};
        Vector vector = new Vector(values);
        int[] expected = {153,407,370};

        //Act
        Vector result = vector.getElementsWithArmstrongNumbers();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsWithArmstrongNumbers_NegativeValues() {

        //Arrange
        int[] values = {-5,123,-153,39,407, 370,0};
        Vector vector = new Vector(values);
        int[] expected = {-153,407,370};

        //Act
        Vector result = vector.getElementsWithArmstrongNumbers();

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscendingWithNDigits_NoElements() {

        //Arrange
        int[] values = {500,2,20,19,280};
        Vector vector = new Vector(values);
        int n = 3;
        int[] expected = {};

        //Act
        Vector result = vector.getElementsThatAreAscendingWithNDigits(n);

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscendingWithNDigits_OneElement() {

        //Arrange
        int[] values = {500,2,20,19,280};
        Vector vector = new Vector(values);
        int n = 2;
        int[] expected = {19};

        //Act
        Vector result = vector.getElementsThatAreAscendingWithNDigits(n);

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscendingWithNDigits_SeveralElements() {

        //Arrange
        int[] values = {500,2,123456,20,19,280,12,159,4689,291,18,357};
        Vector vector = new Vector(values);
        int n = 3;
        int[] expected = {123456,159,4689,357};

        //Act
        Vector result = vector.getElementsThatAreAscendingWithNDigits(n);

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void getElementsThatAreAscendingWithNDigits_NegativeValues() {

        //Arrange
        int[] values = {-500,2,20,-19,280};
        Vector vector = new Vector(values);
        int n = 2;
        int[] expected = {-19};

        //Act
        Vector result = vector.getElementsThatAreAscendingWithNDigits(n);

        //Assert
        assertArrayEquals(expected,result.toArray());

    }

    @Test
    void checkIfVectorsAreEqual_True() {

        //Arrange
        int[] values_1 = {4,1,3,5};
        Vector vector_1 = new Vector(values_1);
        int[] values_2 = {4,1,3,5};
        Vector vector_2 = new Vector(values_2);

        //Act
        boolean result = vector_1.isEqualToVector(vector_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfVectorsAreEqual_EmptyVectorsUsingEmptyConstructor() {

        //Arrange
        Vector vector_1 = new Vector();
        Vector vector_2 = new Vector();

        //Act
        boolean result = vector_1.isEqualToVector(vector_2);

        //Assert
        assertTrue(result);

    }

    @Test
    void checkIfVectorsAreEqual_False_DifferentLengths() {

        //Arrange
        int[] values_1 = {4,1,3,5};
        Vector vector_1 = new Vector(values_1);
        int[] values_2 = {4,1,3,5,2};
        Vector vector_2 = new Vector(values_2);

        //Act
        boolean result = vector_1.isEqualToVector(vector_2);

        //Assert
        assertFalse(result);

    }
    @Test
    void checkIfVectorsAreEqual_False_DifferentValues() {

        //Arrange
        int[] values_1 = {4,1,3,5};
        Vector vector_1 = new Vector(values_1);
        int[] values_2 = {4,1,3,-5};
        Vector vector_2 = new Vector(values_2);

        //Act
        boolean result = vector_1.isEqualToVector(vector_2);

        //Assert
        assertFalse(result);

    }

}