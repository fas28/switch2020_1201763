import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SombraTest {

    @Test
    void obterSombra() {

        //Arrange
        double sombra = 2;
        Sombra sombra_1 = new Sombra(sombra);
        double expected = 2;

        //Act
        double result = sombra_1.obterSombra();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void obterSombra_CheckIfErrorIsThrown() {

        //Arrange
        double sombra = 0;

        //Assert
        assertThrows(IllegalArgumentException.class, () -> {new Sombra(sombra);});

    }

    @Test
    void obterSombra_CheckErrorMessage() {

        //Arrange
        double sombra = -1;
        Exception exception = assertThrows(IllegalArgumentException.class,()->{new Sombra(sombra);});
        String expected = "Sombra tem de ser superior a zero.";

        //Act
        String result = exception.getMessage();

        //Assert
        assertEquals(expected,result);

    }

}