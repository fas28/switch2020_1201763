import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EdificioTest {

    @Test
    void obterAlturaEdificio() {

        //Arrange
        double sombraEdificio = 20;
        Sombra sombra_1 = new Sombra(sombraEdificio);
        double alturaPessoa = 1.63;
        Pessoa pessoa_1 = new Pessoa(alturaPessoa);
        double sombraPessoa = 2.40;
        Sombra sombra_2 = new Sombra(sombraPessoa);
        Edificio edificio_1 = new Edificio();
        double expected = 13.583;

        //Act
        double result = edificio_1.obterAlturaEdificio(sombra_1,pessoa_1,sombra_2);

        //Assert
        assertEquals(expected,result,0.001);

    }

}