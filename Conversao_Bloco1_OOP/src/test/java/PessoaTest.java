import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PessoaTest {

    @Test
    void obterAlturaPessoa() {

        //Arrange
        double alturaPessoa = 1.80;
        Pessoa pessoa_1 = new Pessoa(alturaPessoa);
        double expected = 1.80;

        //Act
        double result = pessoa_1.obterAlturaPessoa();

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void obterAlturaPessoa_CheckIfErrorIsThrown() {

        //Arrange
        double alturaPessoa = -1.80;

        //Assert
        assertThrows(IllegalArgumentException.class, () -> {new Pessoa(alturaPessoa);});

    }

    @Test
    void obterAlturaPessoa_CheckErrorMessage() {

        //Arrange
        double alturaPessoa = 0;
        Exception exception = assertThrows(IllegalArgumentException.class,()->{new Pessoa(alturaPessoa);});
        String expected = "Não pode ter valores iguais ou inferiores a zero.";

        //Act
        String result = exception.getMessage();

        //Assert
        assertEquals(expected,result);

    }

}