import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CilindroTest {

    @Test
    void obterVolume() {

        //Arrange
        double raio = 3;
        double altura = 5;
        Cilindro cilindro = new Cilindro(raio, altura);
        double expected = 141371.7;

        //Act
        double result = cilindro.obterVolume();

        //Assert
        assertEquals(expected, result, 0.1);

    }

    @Test
    void obterVolume_CheckIfErrorIsThrown() {

        //Arrange
        double raio = -3;
        double altura = 5;

        //Assert
        assertThrows(IllegalArgumentException.class, () -> {
            Cilindro cilindro = new Cilindro(raio, altura);
        });

    }

    @Test
    void obterVolume_CheckErrorMessage() {

        //Arrange
        double raio = -3;
        double altura = 5;
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            new Cilindro(raio, altura);
        });
        String expectedMessage = "Não pode ter valores iguais ou inferiores a zero.";

        //Act
        String actualMessage = exception.getMessage();

        //Assert
        assertEquals(expectedMessage, actualMessage);

    }
}