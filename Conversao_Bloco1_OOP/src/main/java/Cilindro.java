public class Cilindro {

    //Declarar variáveis com inicialização de valores de default que são assumidos quando se inicia um objeto através de um construtor vazio

    private double raio = -1;
    private double altura = -1;

    //Construtor

    public Cilindro(double raio, double altura) {
        //Antes da atribuição, fazer um método de validação dos valores

        if (raio <= 0 || altura <= 0) {
            throw new IllegalArgumentException("Não pode ter valores iguais ou inferiores a zero.");
        } else {
            this.raio = raio;
            this.altura = altura;
        }
    }

    //Métodos

    public double obterVolume() {
        double areaBase = obterAreaBase(); //ou this.obterAreaBase();
        double volume = areaBase*altura;
        return obterVolumeEmLitros(volume);
    }

    private double obterAreaBase() { //porque não interessa chamarmos a partir do main
        return Math.PI*(raio*raio);
    }

    private double obterVolumeEmLitros (double volume) {
        return volume*1000;
    }
}
