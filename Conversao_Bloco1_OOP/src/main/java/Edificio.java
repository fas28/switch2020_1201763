public class Edificio {

    //Construtor
    public Edificio () {};

    //Métodos
    public double obterAlturaEdificio (Sombra sombraEdificio, Pessoa pessoa, Sombra sombraPessoa) {

        double alturaEdificio = (sombraEdificio.obterSombra()*pessoa.obterAlturaPessoa())/sombraPessoa.obterSombra();

        return alturaEdificio;
    }

}
