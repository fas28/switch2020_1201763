public class Main {

    public static void main(String[] args) {

        //Bloco 1 - Ex.3)
        Cilindro myCilindro = new Cilindro(3,5);
        System.out.println(myCilindro.obterVolume());

        //Bloco 1 - Ex.6)
        Pessoa pessoa_1 = new Pessoa(1.68);
        Edificio edificio_1 = new Edificio();
        Sombra sombraPessoa = new Sombra(2);
        Sombra sombraEdificio = new Sombra(20);
        System.out.println("Altura do edifício é: " + edificio_1.obterAlturaEdificio(sombraEdificio,pessoa_1,sombraPessoa));

    }

}
