import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_6Test {

    @Test
    void trimArray_PositiveN() {

        //Arrange
        int[] array = {1, 2, 3, 4, 5, 6};
        int N = 4;
        int[] expected = {1, 2, 3, 4};

        //Act
        int[] result = Bloco_4_ex_6.trimArray(array,N);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void trimArray_NegativeN() {

        //Arrange
        int[] array = {1, 2, 3, 4, 5, 6};
        int N = -6;

        //Act
        int[] result = Bloco_4_ex_6.trimArray(array,N);

        //Assert
        assertNull(result);

    }

    @Test
    void trimArray_NEqualsZero() {

        //Arrange
        int[] array = {1, 2, 3, 4, 5, 6};
        int N = 0;

        //Act
        int[] result = Bloco_4_ex_6.trimArray(array,N);

        //Assert
       assertNull(result);

    }


}