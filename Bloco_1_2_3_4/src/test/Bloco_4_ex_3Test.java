import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_3Test {

    @Test
    void getArraySum_PositiveIntegersArray() {

        //Arrange
        double[] array = new double[] {1,2,3,4,5,6};
        double expected = 21;

        //Act
        double result = Bloco_4_ex_3.getArraySum(array);

        //Arrange
        assertEquals(expected,result,0.01);

    }

    @Test
    void getArraySum_NegativeIntegersArray() {

        //Arrange
        double[] array = new double[] {-1,-3,-4};
        double expected = -8;

        //Act
        double result = Bloco_4_ex_3.getArraySum(array);

        //Arrange
        assertEquals(expected,result,0.01);

    }

    @Test
    void getArraySum_PositiveDecimalsArray() {

        //Arrange
        double[] array = new double[] {0.6,9,6.5,0.5};
        double expected = 16.6;

        //Act
        double result = Bloco_4_ex_3.getArraySum(array);

        //Arrange
        assertEquals(expected,result, 0.01);

    }

}