import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_2Test {

    @org.junit.jupiter.api.Test
    void getMediaPesada() {

        double expected = 13.1;
        double result = Bloco_2.getMediaPesada(15, 12, 13, 30, 50, 20);
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_II_Processamento() {

        int[] expected = {5, 4, 7};
        int[] result = Bloco_2.exercicio_II_Processamento(547);
        assertArrayEquals(expected, result);

    }

    @Test
    void parOuImpar() {

        String expected = "Ímpar";
        String result = Bloco_2.parOuImpar(547);
        assertEquals(expected, result);
    }

    @Test
    void getSqrt() {

        double expected = 11.6297;
        double result = Bloco_2.getSqrt(10, 5, 12.5, 2);
        assertEquals(expected, result, 0.0001);

    }

    @Test
    void exercicio_IV_Processamento_Teste1() {

        double expected = -2.5;
        double result = Bloco_2.exercicio_IV_Processamento(-2.5);
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_IV_Processamento_Teste2() {

        double expected = 0;
        double result = Bloco_2.exercicio_IV_Processamento(0);
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_IV_Processamento_Teste3() {

        double expected = 3;
        double result = Bloco_2.exercicio_IV_Processamento(3);
        assertEquals(expected, result, 0.01);

    }

    @Test
    void exercicio_V_Processamento_Teste1() {

        double expected = 6.08581;
        double result = Bloco_2.exercicio_V_Processamento(20);
        assertEquals(expected, result, 0.00001);
    }

    @Test
    void exercicio_V_Processamento_Teste2() {

        double expected = 2.828;
        double result = Bloco_2.exercicio_V_Processamento(12);
        assertEquals(expected, result, 0.001);

    }

    @Test
    void exercicio_V_Processamento_Teste3() {

        double expected = 1;
        double result = Bloco_2.exercicio_V_Processamento(6);
        assertEquals(expected, result, 0.001);

    }

    @Test
    void exercicio_V_Processamento_Teste4() {

        double expected = -1;
        double result = Bloco_2.exercicio_V_Processamento(-35);
        assertEquals(expected, result, 0.0001);

    }

    @Test
    void obterClassificacaoCubo() {

        String expected = "Grande";
        String result = Bloco_2.obterClassificacaoCubo(20);
        assertEquals(expected, result);

    }

    @Test
    void exercicio_VI_Processamento_Teste1() {

        String expected = "2:13:20 - Boa noite!";
        String result = Bloco_2.exercicio_VI_Processamento(8000);
        assertEquals(expected, result);

    }

    @Test
    void exercicio_VI_Processamento_Teste2() {

        String expected = "6:56:40 - Bom dia!";
        String result = Bloco_2.exercicio_VI_Processamento(25000);
        assertEquals(expected, result);

    }

    @Test
    void exercicio_VI_Mensagem_Teste1() {

        String expected = "Bom dia!";
        String result = Bloco_2.exercicio_VI_Mensagem(6, 30, 2);

    }

    @Test
    void exercicio_VI_Mensagem_Teste2() {

        String expected = "Bom dia!";
        String result = Bloco_2.exercicio_VI_Mensagem(12, 0, 0);

    }

    @Test
    void exercicio_VI_Mensagem_Teste3() {

        String expected = "Bom dia!";
        String result = Bloco_2.exercicio_VI_Mensagem(20, 0, 0);

    }

    @Test
    void exercicio_VI_Mensagem_Teste4() {

        String expected = "Boa noite!";
        String result = Bloco_2.exercicio_VI_Mensagem(04, 0, 0);

    }

    @Test
    void exercicio_VII_CustoMaoObra_Teste1() {

        double expected = 320;
        double result = Bloco_2.exercicio_VII_CustoMaoObra(150,64);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_VII_CustoMaoObra_Teste2() {

        double expected = 855;
        double result = Bloco_2.exercicio_VII_CustoMaoObra(400,95);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_VII_CustoTinta_Teste1() {

        double expected = 37.5;
        double result = Bloco_2.exercicio_VII_CustoTinta(150,20,5);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_VII_CustoTinta_Teste2() {

        double expected = 226.67;
        double result = Bloco_2.exercicio_VII_CustoTinta(400,15,8.5);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_VIII_Processamento_Teste1() {

        String expected = "12 é múltiplo de 6, e 6 é divisor de 12";
        String result = Bloco_2.exercicio_VIII_Processamento(12,6);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_VIII_Processamento_Teste2() {

        String expected = "12 e 12 são múltiplos e divisores um do outro";
        String result = Bloco_2.exercicio_VIII_Processamento(12,12);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_VIII_Processamento_Teste3() {

        String expected = "12 é múltiplo de 6, e 6 é divisor de 12";
        String result = Bloco_2.exercicio_VIII_Processamento(6,12);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_VIII_Processamento_Teste4() {

        String expected = "Nenhum dos números é múltiplo/divisor do outro";
        String result = Bloco_2.exercicio_VIII_Processamento(6,7);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_IX_Processamento_Teste1() {

        String expected = "Dígitos estão em ordem crescente";
        String result = Bloco_2.exercicio_IX_Processamento(235);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_IX_Processamento_Teste2() {

        String expected = "Dígitos não estão em ordem crescente";
        String result = Bloco_2.exercicio_IX_Processamento(225);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_IX_Processamento_Teste3() {

        String expected = "Dígitos não estão em ordem crescente";
        String result = Bloco_2.exercicio_IX_Processamento(215);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_IX_Processamento_Teste4() {

        String expected = "Dígitos não estão em ordem crescente";
        String result = Bloco_2.exercicio_IX_Processamento(315);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_IX_Processamento_Teste5() {

        String expected = "Dígitos não estão em ordem crescente";
        String result = Bloco_2.exercicio_IX_Processamento(311);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_X_Processamento_Teste1() {

        double expected = 100;
        double result = Bloco_2.exercicio_X_Processamento(250);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_X_Processamento_Teste2() {

        double expected = 120;
        double result = Bloco_2.exercicio_X_Processamento(200);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_X_Processamento_Teste3() {

        double expected = 70;
        double result = Bloco_2.exercicio_X_Processamento(100);
        assertEquals(expected,result,0.01);

    }


    @Test
    void exercicio_X_Processamento_Teste4() {

        double expected = 40;
        double result = Bloco_2.exercicio_X_Processamento(50);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_X_Processamento_Teste5() {

        double expected = 0;
        double result = Bloco_2.exercicio_X_Processamento(-1);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_X_Processamento_Teste6() {

        double expected = 0;
        double result = Bloco_2.exercicio_X_Processamento(0);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_X_Processamento_Teste7() {

        double expected = 56.175;
        double result = Bloco_2.exercicio_X_Processamento(80.25);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_XI_Processamento_Teste1() {

        String expected = "Turma Boa";
        String result = Bloco_2.exercicio_XI_Processamento(0.7,0.1,0.2,0.5,0.8);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XI_Processamento_Teste2() {

        String expected = "Valor inválido";
        String result = Bloco_2.exercicio_XI_Processamento(-1,0.1,0.2,0.5,0.8);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XII_Processamento_Teste1() {

        String expected = "O índice de poluição é aceitável.";
        String result = Bloco_2.exercicio_XII_Processamento(0.3);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XII_Processamento_Teste2() {

        String expected = "O índice de poluição não é aceitável. As empresas dos três grupos devem paralisar as suas atividades.";
        String result = Bloco_2.exercicio_XII_Processamento(0.511);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XII_Processamento_Teste3() {

        String expected = "O índice de poluição não é aceitável. As empresas do 1º e do 2º grupo devem paralisar as suas atividades.";
        String result = Bloco_2.exercicio_XII_Processamento(0.50);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XII_Processamento_Teste4() {

        String expected = "O índice de poluição não é aceitável. As empresas do 1º grupo devem paralisar as suas atividades.";
        String result = Bloco_2.exercicio_XII_Processamento(0.40);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XIII_NumeroHoras_Teste1() {

        double expected = 54;
        double result = Bloco_2.exercicio_XIII_NumeroHoras(500,30,60);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XIII_NumeroHoras_Teste2() {

        double expected = 105;
        double result = Bloco_2.exercicio_XIII_NumeroHoras(1000,80,55);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XIII_CustoTotal_Teste1() {

        double expected = 7040.00;
        double result = Bloco_2.exercicio_XIII_CustoTotal(500,30,60,54);
        assertEquals(expected,result, 0.01);

    }

    @Test
    void exercicio_XIII_CustoTotal_Teste2() {

        double expected = 13475.00;
        double result = Bloco_2.exercicio_XIII_CustoTotal(1000,80,55,105);
        assertEquals(expected,result, 0.01);

    }

    @Test
    void exercicio_XIV_Processamento_Teste1() {

        double expected = 3.218;
        double result = Bloco_2.exercicio_XIV_Processamento(2,2,2,2,2);
        assertEquals(expected,result,0.001);

    }

    @Test
    void exercicio_XIV_Processamento_Teste2() {

        double expected = 6.597;
        double result = Bloco_2.exercicio_XIV_Processamento(8,5,4,1,2.5);
        assertEquals(expected,result,0.001);

    }

    @Test
    void exercicio_XIV_Processamento_Teste3() {

        double expected = 4.264;
        double result = Bloco_2.exercicio_XIV_Processamento(3,7,0.25,1,2);
        assertEquals(expected,result,0.001);

    }

    @Test
    void exercicio_XV_Verificacao_Teste1() {

        String expected = "possível";
        String result = Bloco_2.exercicio_XV_Verificacao(5,6,8);
        assertEquals(expected,result);
    }


    @Test
    void exercicio_XV_Verificacao_Teste2() {

        String expected = "impossível";
        String result = Bloco_2.exercicio_XV_Verificacao(5,6,12);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_XV_Classificacao_Teste1() {

        String expected = "equilátero";
        String result = Bloco_2.exercicio_XV_Classificacao(5,5,5);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_XV_Classificacao_Teste2() {

        String expected = "isósceles";
        String result = Bloco_2.exercicio_XV_Classificacao(5,6,5);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_XV_Classificacao_Teste3() {

        String expected = "escaleno";
        String result = Bloco_2.exercicio_XV_Classificacao(5,3,4);
        assertEquals(expected,result);
    }

    @Test
    void exercicio_XVI_Classificacao_Teste1() {

        String expected = "rectângulo";
        String result = Bloco_2.exercicio_XVI_Classificacao(15,90,75);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVI_Classificacao_Teste2() {

        String expected = "acutângulo";
        String result = Bloco_2.exercicio_XVI_Classificacao(75,35,70);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVI_Classificacao_Teste3() {

        String expected = "obtusângulo";
        String result = Bloco_2.exercicio_XVI_Classificacao(110,65,5);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVI_Verificacao_Teste1() {

        String expected = "possível";
        String result = Bloco_2.exercicio_XVI_Verificacao(60,60,60);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVI_Verificacao_Teste2() {

        String expected = "impossível";
        String result = Bloco_2.exercicio_XVI_Verificacao(-60,60,60);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVI_Verificacao_Teste3() {

        String expected = "impossível";
        String result = Bloco_2.exercicio_XVI_Verificacao(60,40,60);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVII_Processamento_Teste1() {

        String expected = "O comboio chega às 2:30 de amanhã.";
        String result = Bloco_2.exercicio_XVII_Processamento(16,0,10,30);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVII_Processamento_Teste2() {

        String expected = "O comboio chega às 18:27 de hoje.";
        String result = Bloco_2.exercicio_XVII_Processamento(9,22,9,5);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVIII_Processamento_Teste1() {

        String expected = "A tarefa termina às 16:19:30 de amanhã.";
        String result = Bloco_2.exercicio_XVIII_Processamento(15,19,30,90000);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XVIII_Processamento_Teste2() {

        String expected = "A tarefa termina às 22:31:35 de hoje.";
        String result = Bloco_2.exercicio_XVIII_Processamento(21,8,15,5000);
        assertEquals(expected,result);

    }

    @Test
    void exercicio_XIX_Processamento_Teste1() {

        double expected = 262.5;
        double result = Bloco_2.exercicio_XIX_Processamento(35);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_XIX_Processamento_Teste2() {

        double expected = 310;
        double result = Bloco_2.exercicio_XIX_Processamento(40);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_XIX_Processamento_Teste3() {

        double expected = 425;
        double result = Bloco_2.exercicio_XIX_Processamento(48);
        assertEquals(expected,result,0.01);
    }

    @Test
    void exercicio_XX_CustoKit_Teste1() {

        double expected = 50;
        double result = Bloco_2.exercicio_XX_CustoKit(2,2);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_XX_CustoKit_Teste2() {

        double expected = 140;
        double result = Bloco_2.exercicio_XX_CustoKit(3,8);
        assertEquals(expected,result,0.01);

    }

    @Test
    void exercicio_XX_CustoKm() {

        double expected = 10;
        double result = Bloco_2.exercicio_XX_CustoKm(5);
        assertEquals(expected,result,0.01);
    }
}