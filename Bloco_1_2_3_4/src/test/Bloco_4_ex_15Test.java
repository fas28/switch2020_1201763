import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_15Test {

    @Test
    void getLowestValueMatrix_SquareMatrix() {

        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3}};
        int expected = -5;
        int result = Bloco_4_ex_15.getLowestValueMatrix(matrix);
        assertEquals(expected, result);

    }

    @Test
    void getLowestValueMatrix_RectangularMatrix() {

        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3},{1,-7,1}};
        int expected = -7;
        int result = Bloco_4_ex_15.getLowestValueMatrix(matrix);
        assertEquals(expected, result);

    }

    @Test
    void getLowestValueMatrix_RectangularMatrixDifferentNumberOfColumns() {

        int[][] matrix = {{2,1}, {2,3,4}, {0},{1,5,1}};
        int expected = 0;
        int result = Bloco_4_ex_15.getLowestValueMatrix(matrix);
        assertEquals(expected, result);

    }

    @Test
    void getHighestValueMatrix_SquareMatrix() {

        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3}};
        int expected = 5;
        int result = Bloco_4_ex_15.getHighestValueMatrix(matrix);
        assertEquals(expected, result);

    }

    @Test
    void getHighestValueMatrix_RectangularMatrix() {

        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3},{1,-7,1}};
        int expected = 5;
        int result = Bloco_4_ex_15.getHighestValueMatrix(matrix);
        assertEquals(expected, result);

    }

    @Test
    void getHighestValueMatrix_RectangularMatrixDifferentNumberOfColumns() {

        int[][] matrix = {{2,1}, {2,3,4}, {0},{1,5,1}};
        int expected = 5;
        int result = Bloco_4_ex_15.getHighestValueMatrix(matrix);
        assertEquals(expected, result);

    }

    @Test
    void getAverageValueMatrix_SquareMatrix() {

        //Arrange
        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3}};
        double expected = 1.67;

        //Act
        double result = Bloco_4_ex_15.getAverageValueMatrix(matrix);

        //Arrange
        assertEquals(expected, result, 0.01);

    }

    @Test
    void getAverageValueMatrix_RectangularMatrix() {

        //Arrange
        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3},{1,-7,1}};
        double expected = 0.83;

        //Act
        double result = Bloco_4_ex_15.getAverageValueMatrix(matrix);

        //Arrange
        assertEquals(expected, result, 0.01);

    }

    @Test
    void getAverageValueMatrix_RectangularMatrixDifferentNumberOfColumns() {

        //Arrange
        int[][] matrix = {{2,1}, {2,3,4}, {0},{1,5,1}};
        double expected = 2.11;

        //Act
        double result = Bloco_4_ex_15.getAverageValueMatrix(matrix);

        //Arrange
        assertEquals(expected, result, 0.01);

    }

    @Test
    void getProductValueMatrix_SquareMatrix() {

        //Arrange
        int[][] matrix = {{2,1,5}, {2,3,4}, {0,-5,3}};
        double expected = 0;

        //Act
        double result = Bloco_4_ex_15.getProductValueMatrix(matrix);

        //Arrange
        assertEquals(expected, result, 0.01);

    }

    @Test
    void getProductValueMatrix_RectangularMatrix() {

        //Arrange
        int[][] matrix = {{2,1,5}, {2,3,4}, {6,-5,3},{1,-7,1}};
        double expected = 151200;

        //Act
        double result = Bloco_4_ex_15.getProductValueMatrix(matrix);

        //Arrange
        assertEquals(expected, result, 0.01);

    }

    @Test
    void getProductValueMatrix_RectangularMatrixDifferentNumberOfColumns() {

        //Arrange
        int[][] matrix = {{2,1}, {2,3,4}, {-2},{1,5,1}};
        double expected = -480;

        //Act
        double result = Bloco_4_ex_15.getProductValueMatrix(matrix);

        //Arrange
        assertEquals(expected, result, 0.01);

    }

    @Test
    void getNonRepeatedValuesInMatrix_GeneralUse() {

        //Arrange
        int[][] matrix = {{1,3,4},{1,2,3},{2,6,8},{-2,1,9}};
        int[] expected = {1,3,4,2,6,8,-2,9};

        //Act
        int[] result = Bloco_4_ex_15.getNonRepeatedValuesInMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getNonRepeatedValuesInMatrix_OnlyOneValue() {

        //Arrange
        int[][] matrix = {{1,1,1},{1,1,1},{1,1,1},{1,1,1}};
        int[] expected = {1};

        //Act
        int[] result = Bloco_4_ex_15.getNonRepeatedValuesInMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getNonRepeatedValuesInMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_15.getNonRepeatedValuesInMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void findValueInArray_FindsValue() {

        //Arrange
        int value = 5;
        int[] array = {4,2,5,6};

        //Act
        boolean result = Bloco_4_ex_15.findValueInArray(value, array);

        //Assert
        assertTrue(result);

    }

    @Test
    void findValueInArray_DoesNotFindValue() {

        //Arrange
        int value = 2;
        int[] array = {1,-2,4,6,0};

        //Act
        boolean result = Bloco_4_ex_15.findValueInArray(value, array);

        //Assert
        assertFalse(result);

    }

    @Test
    void findValueInArray_ArrayIsEmpty() {

        //Arrange
        int value = 1;
        int[] array = {};

        //Act
        boolean result = Bloco_4_ex_15.findValueInArray(value, array);

        //Assert
        assertFalse(result);

    }

    @Test
    void findValueInArray_ArrayIsNull() {

        //Arrange
        int value = 1;
        int[] array = null;

        //Act
        boolean result = Bloco_4_ex_15.findValueInArray(value, array);

        //Assert
        assertFalse(result);

    }

    @Test
    void addValueToArray_GeneralUse() {

        //Arrange
        int value = 1;
        int[] array = {4,5,1,2,0,-2};
        int[] expected = {4,5,1,2,0,-2,1};

        //Act
        int[] result = Bloco_4_ex_15.addValueToArray(value, array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void addValueToArray_ArrayIsEmpty() {

        //Arrange
        int value = 1;
        int[] array = {};
        int[] expected = {1};

        //Act
        int[] result = Bloco_4_ex_15.addValueToArray(value, array);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void addValueToArray_ArrayIsNull() {

        //Arrange
        int value = 1;
        int[] array = null;

        //Act
        int[] result = Bloco_4_ex_15.addValueToArray(value, array);

        //Assert
        assertNull(result);

    }

    @Test
    void matrixToArray_SameNumberOfColumnsInMatrix() {

        //Arrange
        int[][] matrix = {{1,3,4},{1,2,3},{2,6,8},{-2,1,9}};
        int[] expected = {1,3,4,1,2,3,2,6,8,-2,1,9};

        //Act
        int[] result = Bloco_4_ex_15.matrixToArray(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void matrixToArray_DifferentNumberOfColumnsInMatrix() {

        //Arrange
        int[][] matrix = {{1,3},{1,2,3},{2},{-2,1,9}};
        int[] expected = {1,3,1,2,3,2,-2,1,9};

        //Act
        int[] result = Bloco_4_ex_15.matrixToArray(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }


    @Test
    void matrixToArray_MatrixIsNull() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_15.matrixToArray(matrix);

        //Assert
        assertNull(result);

    }


    @Test
    void getUniqueValuesInMatrix_GeneralUse() {

        //Arrange
        int[][] matrix = {{1,3,4},{1,2,3},{2,6,8},{-2,1,9}};
        int[] expected = {4,6,8,-2,9};

        //Act
        int[] result = Bloco_4_ex_15.getUniqueValuesInMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }


    @Test
    void getUniqueValuesInMatrix_OnlyOneValue() {

        //Arrange
        int[][] matrix = {{1,1,1},{1,1,1},{1,1,1},{1,1,1}};

        //Act
        int[] result = Bloco_4_ex_15.getUniqueValuesInMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getUniqueValuesInMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_15.getUniqueValuesInMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void intToDoubleMatrix_SameNumberOfColumns() {

        //Arrange
        int[][] matrix = {{1,3,4},{1,2,3},{2,6,8},{-2,1,9}};
        double[][] expected = {{1,3,4},{1,2,3},{2,6,8},{-2,1,9}};

        //Act
        double[][] result = Bloco_4_ex_15.intToDoubleMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void intToDoubleMatrix_DifferentNumberOfColumns() {

        //Arrange
        int[][] matrix = {{1,3,4},{1},{2,6},{-2,1,9}};
        double[][] expected = {{1,3,4},{1},{2,6},{-2,1,9}};

        //Act
        double[][] result = Bloco_4_ex_15.intToDoubleMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getPrimeNumbersInMatrix_WithPrimeNumbers() {

        //Arrange
        int[][] matrix = {{1,3,4},{7,13,5},{11,2,6},{7,5,0}};
        int[] expected = {3,7,13,5,11,2};

        //Act
        int[] result = Bloco_4_ex_15.getPrimeNumbersInMatrix(matrix);

        //Assert
        assertArrayEquals(expected, result);

    }

    @Test
    void getPrimeNumbersInMatrix_WithoutPrimeNumbers() {

        //Arrange
        int[][] matrix = {{1,15,4},{72,16,50},{10,20,6},{72,50,0}};

        //Act
        int[] result = Bloco_4_ex_15.getPrimeNumbersInMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getPrimeNumbersInMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_15.getPrimeNumbersInMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getMainDiagonalOfMatrix_SquareMatrix() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        int[] expected = {1,6,11,16};

        //Act
        int[] result = Bloco_4_ex_15.getMainDiagonalOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getMainDiagonalOfMatrix_RectangularMatrix_MoreLinesThanColumns() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        int[] expected = {1,6,11,4};

        //Act
        int[] result = Bloco_4_ex_15.getMainDiagonalOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getMainDiagonalOfMatrix_RectangularMatrix_MoreColumnsThanLines() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        int[] expected = {1,6,11};

        //Act
        int[] result = Bloco_4_ex_15.getMainDiagonalOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getMainDiagonalOfMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_15.getMainDiagonalOfMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getCountOfDiagonalInRectangularMatrix_MoreLinesThanColumns() {

        //Arrange
        int[][] matrix = {{1,2,3},{0,8,0},{5,3,-1},{0,0,2},{3,2,2}};
        int expected = 3;

        //Act
        int result = Bloco_4_ex_15.getCountOfDiagonalInRectangularMatrix(matrix);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getCountOfDiagonalInRectangularMatrix_MoreColumnsThanLines() {

        //Arrange
        int[][] matrix = {{1,2,3,4,5},{6,7,8,9,10},{1,2,3,4,5},{2,3,4,1,3}};
        int expected = 4;

        //Act
        int result = Bloco_4_ex_15.getCountOfDiagonalInRectangularMatrix(matrix);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getCountOfDiagonalInRectangularMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        Integer result = Bloco_4_ex_15.getCountOfDiagonalInRectangularMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void getSecondDiagonalOfMatrix_SquareMatrix() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        int[] expected = {4,7,10,13};

        //Act
        int[] result = Bloco_4_ex_15.getSecondDiagonalOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getSecondDiagonalOfMatrix_RectangularMatrix_MoreLinesThanColumns() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        int[] expected = {4,7,10,1};

        //Act
        int[] result = Bloco_4_ex_15.getSecondDiagonalOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getSecondDiagonalOfMatrix_RectangularMatrix_MoreColumnsThanLines() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        int[] expected = {4,7,10};

        //Act
        int[] result = Bloco_4_ex_15.getSecondDiagonalOfMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getSecondDiagonalOfMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[] result = Bloco_4_ex_15.getSecondDiagonalOfMatrix(matrix);

        //Assert
        assertNull(result);

    }

    @Test
    void isIdentityMatrix_ReturnsTrue() {

        //Arrange
        int[][] matrix = {{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};

        //Act
        boolean result = Bloco_4_ex_15.isIdentityMatrix(matrix);

        //Assert
        assertTrue(result);

    }

    @Test
    void isIdentityMatrix_NotSquareMatrix() {

        //Arrange
        int[][] matrix = {{1,0,0},{0,1,0},{0,0,1},{1,2,3}};

        //Act
        boolean result = Bloco_4_ex_15.isIdentityMatrix(matrix);

        //Assert
        assertFalse(result);

    }

    @Test
    void isIdentityMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        boolean result = Bloco_4_ex_15.isIdentityMatrix(matrix);

        //Assert
        assertFalse(result);

    }

    @Test
    void getTransposeMatrix_SquareMatrix() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        int[][] expected = {{1,5,9,13},{2,6,10,14},{3,7,11,15},{4,8,12,16}};

        //Act
        int[][] result = Bloco_4_ex_15.getTransposeMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getTransposeMatrix_RectangularMatrix_MoreLinesThanColumns() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        int[][] expected = {{1,5,9,1,5,9},{2,6,10,2,6,10},{3,7,11,3,7,11},{4,8,12,4,8,12}};

        //Act
        int[][] result = Bloco_4_ex_15.getTransposeMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getTransposeMatrix_RectangularMatrix_MoreColumnsThanLines() {

        //Arrange
        int[][] matrix = {{1,2,3,4},{5,6,7,8},{9,10,11,12}};
        int[][] expected = {{1,5,9},{2,6,10},{3,7,11},{4,8,12}};

        //Act
        int[][] result = Bloco_4_ex_15.getTransposeMatrix(matrix);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getTransposeMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        int[][] result = Bloco_4_ex_15.getTransposeMatrix(matrix);

        //Assert
        assertNull(result);

    }


    @Test
    void getInvertibleMatrix_2X2() {

        //Arrange
        int[][] matrix = {{6,1},
                          {13,5}};

        double[][] expected = {{0.294,-0.059},{-0.765,0.353}};

        //Act
        double[][] result = Bloco_4_ex_15.getInverseMatrix(matrix);

        //Assert
        int i, j, dimension = 2;

        for (i = 0; i < dimension; i++) {
            for (j = 0; j < dimension; j++) {
                assertEquals(expected[i][j], result[i][j],0.001);
            }
        }

    }

    @Test
    void getInvertibleMatrix_3X3() {

        //Arrange
        int[][] matrix = {{1,2,3},
                         {0,1,4},
                         {0,0,1}};

        double[][] expected = {{1,-2,5},
                           {0,1,-4},
                           {0,0,1}};

        //Act
        double[][] result = Bloco_4_ex_15.getInverseMatrix(matrix);

        //Assert
        int i, j, dimension = 3;

        for (i = 0; i < dimension; i++) {
            for (j = 0; j < dimension; j++) {
                assertEquals(expected[i][j], result[i][j],0.001);
            }
        }

    }

    @Test
    void getInvertibleMatrix_4X4() {

        //Arrange
        int[][] matrix = {{1,2,3,4},
                         {-5,9,2,6},
                         {5,3,5,3},
                         {2,0,2,0}};

        double[][] expected = {{(-0.15),(-0.10),(0.4),(-0.675)},
                            {(-0.35),(0.1),(0.267),(-0.242)},
                            {(0.15),(0.1),(-0.4),(1.175)},
                            {(0.35),(-0.1),(0.067),(-0.592)}};

        //Act
        double[][] result = Bloco_4_ex_15.getInverseMatrix(matrix);

        //Assert
        int i, j, dimension = 4;

        for (i = 0; i < dimension; i++) {
            for (j = 0; j < dimension; j++) {
                assertEquals(expected[i][j], result[i][j],0.001);
            }
        }

    }


    @Test
    void getInvertibleMatrix_5X5() {

        //Arrange
        int[][] matrix = {{1,2,3,4,8},
                         {1,4,-2,3,10},
                         {-5,9,2,6,1},
                         {5,3,5,3,4},
                         {2,0,2,0,1}};

        double[][] expected = {{-0.126,0.03,-0.094,0.326,-0.502},
                               {-0.23,0.15,0.13,-0.103,0.623},
                               {0.094,-0.07,0.086,-0.227,0.771},
                               {0.198,-0.19,-0.138,0.535,-1.687},
                               {0.064,0.08,0.016,-0.197,0.461}};

        //Act
        double[][] result = Bloco_4_ex_15.getInverseMatrix(matrix);

        //Assert
        int i, j, dimension = 5;

        for (i = 0; i < dimension; i++) {
            for (j = 0; j < dimension; j++) {
                assertEquals(expected[i][j], result[i][j],0.001);
            }
        }

    }

    @Test
    void getInvertibleMatrix_NullMatrix() {

        //Arrange
        int[][] matrix = null;

        //Act
        double[][] result = Bloco_4_ex_15.getInverseMatrix(matrix);

        //Assert
        assertNull(result);


    }

    @Test
    void getInvertibleMatrix_NonSquareMatrix() {

        //Arrange
        int[][] matrix = {{1,2,3},{4,5,6}};

        //Act
        double[][] result = Bloco_4_ex_15.getInverseMatrix(matrix);

        //Assert
        assertNull(result);


    }

    }