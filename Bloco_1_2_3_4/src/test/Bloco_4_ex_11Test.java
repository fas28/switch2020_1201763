import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_11Test {

    @Test
    void getScalarProduct_PositiveValues() {

        //Arrange
        double[] array_1 = {5,2};
        double[] array_2 = {3,1};
        Double expected = 17.0;

        //Act
        Double result = Bloco_4_ex_11.getScalarProduct(array_1, array_2);

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getScalarProduct_NegativeValuesAndZero() {

        //Arrange
        double[] array_1 = {3,-1,2,5};
        double[] array_2 = {1.5,2,0,200};
        Double expected = 1002.5;

        //Act
        Double result = Bloco_4_ex_11.getScalarProduct(array_1,array_2);

        //Assert
        assertEquals(expected,result,0.01);

    }

    @Test
    void getScalarProduct_ReturnsNull() {

        //Arrange
        double[] array_1 = {1,2,3};
        double[] array_2 = {5,3};

        //Act
        Double result = Bloco_4_ex_11.getScalarProduct(array_1,array_2);

        //Assert
        assertNull(result);

    }
}