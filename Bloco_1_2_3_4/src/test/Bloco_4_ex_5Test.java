import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_4_ex_5Test {

    @Test
    void getSumOfEvens_PositiveInteger() {

        //Arrange
        int expected = 12;

        //Act
        int result = Bloco_4_ex_5.getSumOfEvens(123456);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfEvens_PositiveIntegerWithZero() {

        //Arrange
        int expected = 14;

        //Act
        int result = Bloco_4_ex_5.getSumOfEvens(500638);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfEvens_PositiveIntegerWithoutEvens() {

        //Arrange
        int expected = 0;

        //Act
        int result = Bloco_4_ex_5.getSumOfEvens(13595);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfEvens_Zero() {

        //Arrange
        int expected = 0;

        //Act
        int result = Bloco_4_ex_5.getSumOfEvens(0);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfEvens_Negative() {

        //Arrange
        int expected = 0;

        //Act
        int result = Bloco_4_ex_5.getSumOfEvens(-51381);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfUnevens_PositiveInteger() {

        //Arrange
        int expected = 9;

        //Act
        int result = Bloco_4_ex_5.getSumOfUnevens(123456);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfUnevens_PositiveIntegerWithZero() {

        //Arrange
        int expected = 8;

        //Act
        int result = Bloco_4_ex_5.getSumOfUnevens(500638);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfUnevens_PositiveIntegerWithoutUnevens() {

        //Arrange
        int expected = 0;

        //Act
        int result = Bloco_4_ex_5.getSumOfUnevens(2468);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfUnevens_Zero() {

        //Arrange
        int expected = 0;

        //Act
        int result = Bloco_4_ex_5.getSumOfUnevens(0);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getSumOfUnevens_Negative() {

        //Arrange
        int expected = 0;

        //Act
        int result = Bloco_4_ex_5.getSumOfUnevens(-51381);

        //Assert
        assertEquals(expected,result);

    }


    }