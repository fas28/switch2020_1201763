import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_3_ex_19Test {

    @Test
    void exercise_XIX_Run_Test1() {

        String expected = "642531";
        String result = Bloco_3_ex_19.exercise_XIX_Run(123456);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIX_Run_Test2() {

        String expected = "286511";
        String result = Bloco_3_ex_19.exercise_XIX_Run(10068125);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIX_Run_Test3() {

        String expected = "884657";
        String result = Bloco_3_ex_19.exercise_XIX_Run(6487085);
        assertEquals(expected,result);

    }

    @Test
    void exercise_XIX_Run_Test4() {

        String expected = "6485559";
        String result = Bloco_3_ex_19.exercise_XIX_Run(8946555);
        assertEquals(expected,result);

    }
}