import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Bloco_3_ex_18Test {

    @Test
    void exercise_XVIII_Run_Test1() {

        boolean result = Bloco_3_ex_18.exercise_XVIII_Run(13511355, 5);
        assertTrue(result);
    }

    @Test
    void exercise_XVIII_Run_Test2() {

        boolean result = Bloco_3_ex_18.exercise_XVIII_Run(11111111, 9);
        assertFalse(result);
    }

    @Test
    void exercise_XVIII_Run_Test3() {

        boolean result = Bloco_3_ex_18.exercise_XVIII_Run(13511356, 3);
        assertTrue(result);
    }
}