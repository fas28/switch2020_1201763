import java.util.Scanner;

public class Bloco_4_ex_1 {

    /**
     * Bloco 4, Ex. 1 - return the number of digits of a positive integer
     * @param number positive integer
     * @return number of digits of a positive integer
     */

    public static int getNumberOfDigits(int number) {

        int partial = number, count = 0;

        if (number > 0) {
            while (partial > 0) {
                count += 1;
                partial /= 10;
            }
            return count;
        } else {
            return -1;
        }

    }
}
