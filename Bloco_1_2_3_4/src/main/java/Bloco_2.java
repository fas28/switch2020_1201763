import java.sql.SQLOutput;
import java.util.Scanner;

public class Bloco_2 {
    public static void main(String[] args) {

        //EXERCÍCIO 1 - MÉDIA PESADA
        exercicio_I();

        //EXERCÍCIO 2 - SEPARAÇÃO DE DÍGITOS
        exercicio_II();

        //EXERCÍCIO 3 - DISTÂNCIA
        exercicio_III();

        //EXERCÍCIO 4 - FUNÇÃO F(X)
        exercicio_IV();

        //EXERCÍCIO 5 - VOLUME CUBO
        exercicio_V();

        //EXERCÍCIO 6 - HORAS DO DIA
        exercicio_VI();

        //EXERCÍCIO 7 - PINTOR
        exercicio_VII();

        //EXERCÍCIO 8 - PAR OU ÍMPAR
        exercicio_VIII();

        //EXERCÍCIO 9 - ORDEM CRESCENTE
        exercicio_IX();

        //EXERCÍCIO 10 - SALDOS
        exercicio_X();

        //EXERCÍCIO 11 - TURMAS
        exercicio_XI();

        //EXERCÍCIO 12 - POLUIÇÃO
        exercicio_XII();

        //EXERCÍCIO 13 - JARDIM
        exercicio_XIII();

        //EXERCÍCIO 14 - ESTAFETA
        exercicio_XIV();

        //EXERCÍCIO 15 - TRIÂNGULOS
        exercicio_XV();

        //EXERCÍCIO 16 - TRIÂNGULOS 2
        exercicio_XVI();

        //EXERCÍCIO 17 - COMBOIOS
        exercicio_XVII();

        //EXERCÍCIO 18 - MÁQUINA
        exercicio_XVIII();

        //EXERCÍCIO 19 - SALÁRIO
        exercicio_XIX();

        //EXERCÍCIO 20 - KIT JARDINAGEM
        exercicio_XX();

    }

    public static void exercicio_I() {

        Scanner ler = new Scanner(System.in);

        int nota1, nota2, nota3, peso1, peso2, peso3;
        double mediaPesada;

        System.out.println("Insira o valor da primeira nota: ");
        nota1 = ler.nextInt();
        System.out.println("Qual o peso da primeira nota? (de 0 a 100)");
        peso1 = ler.nextInt();
        System.out.println("Insira o valor da segunda nota: ");
        nota2 = ler.nextInt();
        System.out.println("Qual o peso da segunda nota? (de 0 a 100)");
        peso2 = ler.nextInt();
        System.out.println("Insira o valor da terceira nota: ");
        nota3 = ler.nextInt();
        System.out.println("Qual o peso da terceira nota? (de 0 a 100)");
        peso3 = ler.nextInt();

        mediaPesada = getMediaPesada(nota1, nota2, nota3, peso1, peso2, peso3);

        if (mediaPesada >= 8.0) {
            System.out.println("A média ponderada é " + mediaPesada);
            System.out.println("Cumpre a nota mínima exigida!");
        } else {
            System.out.println("A média ponderada é " + mediaPesada);
            System.out.println("Não cumpre a nota mínima exigida!");
        }

    }

    public static void exercicio_II() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira um número de três dígitos: ");

        int numero = ler.nextInt();
        int[] digitos = exercicio_II_Processamento(numero);
        String parOuImpar = parOuImpar(numero);

        System.out.println(digitos[0] + " " + digitos[1] + " " + digitos[2] + " - o número é " + parOuImpar);
    }

    public static void exercicio_III() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o valor de x1: ");
        double x1 = ler.nextDouble();
        System.out.println("Insira o valor de x2: ");
        double x2 = ler.nextDouble();
        System.out.println("Insira o valor de y1: ");
        double y1 = ler.nextDouble();
        System.out.println("Insira o valor de y2: ");
        double y2 = ler.nextDouble();

        double d = getSqrt(x1, x2, y1, y2);

        System.out.println("O valor da distância entre os dois pontos é " + d);

    }

    public static void exercicio_IV() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o valor de x: ");
        double x = ler.nextDouble();

        double f_x = exercicio_IV_Processamento(x);

        System.out.println("O valor de f(x) é :" + f_x);

    }

    public static void exercicio_V() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a área do cubo: ");
        double area = ler.nextDouble();

        double volume = exercicio_V_Processamento(area);
        String classificacao = obterClassificacaoCubo(volume);

        if (volume > 0) {
            System.out.println("O volume do cubo é: " + volume);
            System.out.println("É um cubo: " + classificacao);
        } else {
            System.out.println("Erro no cálculo do volume");
        }

    }

    public static void exercicio_VI() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira os segundos: ");
        int segundos = ler.nextInt();

        String resultado = exercicio_VI_Processamento(segundos);

        System.out.println("São " + resultado);

    }

    public static void exercicio_VII() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Se quiser saber apenas o custo de mão de obra escreva 1; se apenas o custo da tinta " +
                "escreva 2; se quiser o custo total escreva 3");
        int escolha = ler.nextInt();
        double area, salarioDia, custoMaoObra, rendLitro, custoLitro, custoTinta;

        if (escolha == 1) {
            System.out.println("Insira o valor da área a ser pintada, em m^2: ");
            area = ler.nextDouble();
            System.out.println("Insira o valor do salário diário de um pintor, em €: ");
            salarioDia = ler.nextDouble();
            custoMaoObra = exercicio_VII_CustoMaoObra(area, salarioDia);
            System.out.println("O custo da mão de obra é de " + custoMaoObra + "€.");

        } else if (escolha == 2) {
            System.out.println("Insira o valor da área a ser pintada, em m^2: ");
            area = ler.nextDouble();
            System.out.println("Quantos m^2 dá para pintar com 1 litro de tinta? ");
            rendLitro = ler.nextDouble();
            System.out.println("Qual o custo unitário de 1 litro? ");
            custoLitro = ler.nextDouble();
            custoTinta = exercicio_VII_CustoTinta(area, rendLitro, custoLitro);
            System.out.println("O custo da tinta é de " + custoTinta + "€.");
        } else if (escolha == 3) {
            System.out.println("Insira o valor da área a ser pintada, em m^2: ");
            area = ler.nextDouble();
            System.out.println("Insira o valor do salário diário de um pintor, em €: ");
            salarioDia = ler.nextDouble();
            custoMaoObra = exercicio_VII_CustoMaoObra(area, salarioDia);
            System.out.println("Quantos m^2 dá para pintar com 1 litro de tinta? ");
            rendLitro = ler.nextDouble();
            System.out.println("Qual o custo unitário de 1 litro? ");
            custoLitro = ler.nextDouble();
            custoTinta = exercicio_VII_CustoTinta(area, rendLitro, custoLitro);
            System.out.println("O custo total da obra é de " + (custoMaoObra + custoTinta) + "€: custo de mão de obra" +
                    " (" + custoMaoObra + "€) + custo da tinta (" + custoTinta + "€).");
        }

    }

    public static void exercicio_VIII() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o valor de X: ");
        int x = ler.nextInt(); //considero apenas valores inteiros
        System.out.println("Insira o valor de Y: ");
        int y = ler.nextInt(); //considero apenas valores inteiros

        String resultado = exercicio_VIII_Processamento(x, y);

        System.out.println(resultado);

    }

    public static void exercicio_IX() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira um número de três dígitos: ");
        int numero = ler.nextInt();
        String resultado = exercicio_IX_Processamento(numero);
        System.out.println(resultado);

    }

    public static void exercicio_X() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o preço inicial do produto: ");
        double precoInicial = ler.nextDouble();

        double precoFinal = exercicio_X_Processamento(precoInicial);

        System.out.println("O preço final do produto, aplicando o desconto, é de " + precoFinal + "€.");

    }

    public static void exercicio_XI() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza a taxa de aprovação da turma: ");
        double aprovados = ler.nextDouble();
        System.out.println("Introduza a taxa de aprovação mínima de uma turma Má: ");
        double tMa = ler.nextDouble();
        System.out.println("Introduza a taxa de aprovação mínima de uma turma Fraca: ");
        double tFra = ler.nextDouble();
        System.out.println("Introduza a taxa de aprovação mínima de uma turma Razoável: ");
        double tRaz = ler.nextDouble();
        System.out.println("Introduza a taxa de aprovação mínima de uma turma Boa: ");
        double tBoa = ler.nextDouble();

        String classificacao = exercicio_XI_Processamento(aprovados, tMa, tFra, tRaz, tBoa);

        System.out.println(classificacao);

    }

    public static void exercicio_XII() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o índice de poluição: ");
        double indice = ler.nextDouble();

        String notificacao = exercicio_XII_Processamento(indice);

        System.out.println(notificacao);

    }

    public static void exercicio_XIII() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Em quantos m^2 vai ser plantada a grama? ");
        int grama = ler.nextInt();
        System.out.println("Quantas árvores vão ser plantadas? ");
        int arvores = ler.nextInt();
        System.out.println("Quantos arbustos vão ser plantados? ");
        int arbustos = ler.nextInt();

        double numeroHoras = exercicio_XIII_NumeroHoras(grama, arvores, arbustos);

        System.out.println("O número de horas de trabalho de construção é de " + numeroHoras);

        double custoTotal = exercicio_XIII_CustoTotal(grama, arvores, arbustos, numeroHoras);

        System.out.println("O custo total do trabalho de construção é de " + custoTotal + "€");

    }

    public static void exercicio_XIV() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Quantas milhas percorreu o estafeta na 2ª feira? ");
        double milhasSeg = ler.nextDouble();
        System.out.println("Quantas milhas percorreu o estafeta na 3ª feira? ");
        double milhasTer = ler.nextDouble();
        System.out.println("Quantas milhas percorreu o estafeta na 4ª feira? ");
        double milhasQua = ler.nextDouble();
        System.out.println("Quantas milhas percorreu o estafeta na 5ª feira? ");
        double milhasQui = ler.nextDouble();
        System.out.println("Quantas milhas percorreu o estafeta na 6ª feira? ");
        double milhasSex = ler.nextDouble();

        double avgKM = exercicio_XIV_Processamento(milhasSeg, milhasTer, milhasQua, milhasQui, milhasSex);

        System.out.println("O estafeta percorreu em média " + String.format("%.2f", avgKM) + " km por dia.");

    }

    public static void exercicio_XV() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o valor de a: ");
        double a = ler.nextDouble();
        System.out.println("Insira o valor de b: ");
        double b = ler.nextDouble();
        System.out.println("Insira o valor de c: ");
        double c = ler.nextDouble();

        String verificação = exercicio_XV_Verificacao(a, b, c);
        String classificacao = exercicio_XV_Classificacao(a, b, c);

        if (verificação == "possível") {
            System.out.println("O triângulo é " + verificação + ", e é um triângulo " + classificacao);
        } else {
            System.out.println("O triângulo é impossível");
        }

    }

    public static void exercicio_XVI() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o valor do ângulo a em º: ");
        double a = ler.nextDouble();
        System.out.println("Insira o valor do ângulo b em º ");
        double b = ler.nextDouble();
        System.out.println("Insira o valor do ângulo c em º: ");
        double c = ler.nextDouble();

        String verificação = exercicio_XVI_Verificacao(a, b, c);
        String classificacao = exercicio_XVI_Classificacao(a, b, c);

        if (verificação == "possível") {
            System.out.println("O triângulo é " + verificação + ", e é um triângulo " + classificacao);
        } else {
            System.out.println("O triângulo é impossível");
        }

    }

    public static void exercicio_XVII() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a hora de chegada (relógio 24 hors): ");
        int horasPartida = ler.nextInt();
        System.out.println("Indique os minutos de chegada: ");
        int minPartida = ler.nextInt();
        System.out.println("Indique o número de horas de viagem: ");
        int horasViagem = ler.nextInt();
        System.out.println("Indique o número de minutos de viagem: ");
        int minViagem = ler.nextInt();

        String resultado = exercicio_XVII_Processamento(horasPartida,minPartida,horasViagem,minViagem);

        System.out.println(resultado);

    }

    public static void exercicio_XVIII() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Indique a hora de início da tarefa (relógio 24 hors): ");
        int horasInicio = ler.nextInt();
        System.out.println("Indique os minutos de início da tarefa: ");
        int minInicio = ler.nextInt();
        System.out.println("Indique os segundos de início da tarefa: ");
        int segInicio = ler.nextInt();
        System.out.println("Indique os segundos de duração da tarefa: ");
        int segTarefa = ler.nextInt();

        String resultado = exercicio_XVIII_Processamento(horasInicio,minInicio,segInicio,segTarefa);

        System.out.println(resultado);

    }

    public static void exercicio_XIX() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de horas que trabalhou esta semana: ");
        int horasSemana = ler.nextInt();

        double salarioSemanal = exercicio_XIX_Processamento(horasSemana);

        System.out.println("O salário semanal do trabalhador é de " + salarioSemanal + "€.");

    }

    public static void exercicio_XX() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o dia da semana em que quer receber o kit (Domingo-1, 2ªf-2,..., Sábado-7, Feriado-8): ");
        int dia = ler.nextInt();
        System.out.println("Insira o tipo de kit pretendido, em número (A-1, B-2, C-3): ");
        int tipo = ler.nextInt();
        System.out.println("A quantos km fica a sua casa da empresa?");
        double distancia = ler.nextDouble();
        double custoKit = exercicio_XX_CustoKit(tipo,dia);
        double custoKm = exercicio_XX_CustoKm(distancia);

        if (custoKit==0) {
            System.out.println("Inserção errada de dia da semana e/ou de tipo de kit.");
        } else {
            System.out.println("O preço total é de " + (custoKit+custoKm) + "€.");
        }

    }

    public static double getMediaPesada(double nota1, double nota2, double nota3, double peso1, double peso2,
                                        double peso3) {
        return ((nota1 * peso1) + (nota2 * peso2) + (nota3 * peso3)) / (peso1 + peso2 + peso3);
    }

    public static int[] exercicio_II_Processamento(int numero) {

        int[] digitos = new int[3];

        if (numero < 100 || numero > 999) {
            System.out.println("Número não tem três digitos.");
        } else {
            digitos[2] = numero % 10;
            digitos[1] = (numero / 10) % 10;
            digitos[0] = (numero / 100) % 10;
        }

        return digitos;

    }

    public static String parOuImpar(int numero) {
        if (numero % 2 == 0) {
            return "Par";
        } else {
            return "Ímpar";
        }
    }

    public static double getSqrt(double x1, double x2, double y1, double y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static double exercicio_IV_Processamento(double x) {

        double f_x = 0;

        if (x < 0) {
            f_x = x;
        } else if (x == 0) {
            f_x = 0;
        } else if (x > 0) {
            f_x = Math.pow(x, 2) - (2 * x);
        }

        return f_x;

    }

    public static double exercicio_V_Processamento(double area) {

        if (area > 0) {
            double aresta = Math.sqrt(area / 6.0);
            double volume = Math.pow(aresta, 3);
            return volume;
        } else {
            return -1;
        }

    }

    public static String obterClassificacaoCubo(double volume) {

        if (volume < 0) {
            return "Área/Volume errados";
        } else if (volume <= 1) {
            return "Pequeno";
        } else if (volume > 1 && volume <= 2) {
            return "Médio";
        } else {
            return "Grande";
        }
    }

    public static String obterClassificaçãoUsandoArea(double area) {

        double volume = exercicio_V_Processamento(area);
        return obterClassificacaoCubo(volume);

    }

    public static String exercicio_VI_Processamento(int segundos) {

        int horasCompletas, minCompletos, segCompletos;
        String resultado, tardeDiaNoite;

        horasCompletas = segundos / 3600;
        minCompletos = (segundos % 3600) / 60;
        segCompletos = (segundos % 3600) % 60;

        resultado =
                horasCompletas + ":" + minCompletos + ":" + segCompletos + " - " + exercicio_VI_Mensagem(horasCompletas, minCompletos, segCompletos);

        return resultado;
    }

    public static String exercicio_VI_Mensagem(int horas, int min, int seg) {

        if (horas >= 6 && horas < 12 || (horas == 12 && min == 0 && seg == 0)) {
            return "Bom dia!";
        } else if (horas >= 12 && horas < 20 || (horas == 20 && min == 0 & seg == 0)) {
            return "Boa tarde!";
        } else {
            return "Boa noite!";
        }

    }

    public static double exercicio_VII_CustoMaoObra(double area, double salarioDia) {

        int numPintor = 0;

        if (area > 0 && area < 100) {
            numPintor = 1;
        } else if (area >= 100 && area < 300) {
            numPintor = 2;
        } else if (area >= 300 && area < 1000) {
            numPintor = 3;
        } else if (area >= 1000) {
            numPintor = 4;
        }

        double diasTrabalho = Math.ceil((area / (2 * numPintor)) / 8); // Considerando rendimento de 2m^2/h e 8h
        // trabalho/dia

        return diasTrabalho * salarioDia;

    }

    public static double exercicio_VII_CustoTinta(double area, double rendLitro, double custoLitro) {

        return (area / rendLitro) * custoLitro;

    }

    public static String exercicio_VIII_Processamento(int x, int y) {

        String resultado = "";

        if (x % y == 0 || y % x == 0) {
            if (x > y) {
                resultado = x + " é múltiplo de " + y + ", e " + y + " é divisor de " + x;
            } else if (x < y) {
                resultado = y + " é múltiplo de " + x + ", e " + x + " é divisor de " + y;
            } else if (x == y) {
                resultado = x + " e " + y + " são múltiplos e divisores um do outro";
            }
        } else {
            resultado = "Nenhum dos números é múltiplo/divisor do outro";
        }

        return resultado;
    }

    public static String exercicio_IX_Processamento(int numero) {

        int dig1, dig2, dig3;

        dig3 = numero % 10;
        dig2 = (numero / 10) % 10;
        dig1 = numero / 100;

        if (dig2 > dig1 && dig3 > dig2) {
            return "Dígitos estão em ordem crescente";
        } else {
            return "Dígitos não estão em ordem crescente";
        }

    }

    public static double exercicio_X_Processamento(double precoInicial) {

        double precoFinal = 0;

        if (precoInicial <= 0) {
            precoFinal = 0;
        } else if (precoInicial <= 50) {
            precoFinal = precoInicial * (1 - 0.20);
        } else if (precoInicial <= 100) {
            precoFinal = precoInicial * (1 - 0.30);
        } else if (precoInicial <= 200) {
            precoFinal = precoInicial * (1 - 0.40);
        } else if (precoInicial > 200) {
            precoFinal = +precoInicial * (1 - 0.60);
        }

        return precoFinal;
    }

    public static String exercicio_XI_Processamento(double aprovados, double tMa, double tFra, double tRaz,
                                                    double tBoa) {

        String classificacao;

        if (aprovados < 0 || aprovados > 1) {
            classificacao = "Valor inválido";
        } else if (aprovados < tMa) {
            classificacao = "Turma Má";
        } else if (aprovados < tFra) {
            classificacao = "Turma Fraca";
        } else if (aprovados < tRaz) {
            classificacao = "Turma Razoável";
        } else if (aprovados < tBoa) {
            classificacao = "Turma Boa";
        } else {
            classificacao = "Turma Excelente";
        }

        return classificacao;
    }

    public static String exercicio_XII_Processamento(double indice) {

        String notificacao = "";

        if (indice >= 0 && indice <= 0.3) {
            notificacao = "O índice de poluição é aceitável.";
        }
        if (indice > 0.5) {
            notificacao = "O índice de poluição não é aceitável. As empresas dos três grupos devem paralisar as suas " +
                    "atividades.";
        } else if (indice > 0.4) {
            notificacao = "O índice de poluição não é aceitável. As empresas do 1º e do 2º grupo devem paralisar as " +
                    "suas atividades.";
        } else if (indice > 0.3) {
            notificacao = "O índice de poluição não é aceitável. As empresas do 1º grupo devem paralisar as suas " +
                    "atividades.";
        }

        return notificacao;
    }

    public static double exercicio_XIII_NumeroHoras(int grama, int arvores, int arbustos) {

        double horasGrama = Math.ceil((300 * (float) grama) / (60 * 60));
        double horasArvores = Math.ceil((600 * (float) arvores) / (60 * 60));
        double horasArbustos = Math.ceil((400 * (float) arbustos) / (60 * 60));

        double numeroHoras = horasGrama + horasArvores + horasArbustos;

        return numeroHoras;

    }

    public static double exercicio_XIII_CustoTotal(int grama, int arvores, int arbustos, double numeroHoras) {

        double custoTotal = (10 * grama) + (20 * arvores) + (15 * arbustos) + (numeroHoras * 10);

        return custoTotal;

    }

    public static double exercicio_XIV_Processamento(double milhasSeg, double milhasTer, double milhasQua,
                                                     double milhasQui, double milhasSex) {

        double avgKm = ((milhasSeg + milhasTer + milhasQua + milhasQui + milhasSex) / 5) * 1.609;

        return avgKm;

    }

    public static String exercicio_XV_Verificacao(double a, double b, double c) {

        String verificacao = "";

        if (a > 0 && b > 0 && c > 0) {
            if ((a < (b + c)) && (b < (a + c)) && (c < (a + b))) {
                verificacao = "possível";
            } else {
                verificacao = "impossível";
            }
        } else {
            verificacao = "impossível";
        }

        return verificacao;

    }

    public static String exercicio_XV_Classificacao(double a, double b, double c) {

        String classificacao = "";

        if (a == b && b == c) {
            classificacao = "equilátero";
        } else if (a == b || b == c || a == c) {
            classificacao = "isósceles";
        } else {
            classificacao = "escaleno";
        }

        return classificacao;

    }

    public static String exercicio_XVI_Classificacao(double a, double b, double c) {

        String classificacao = "";

       if (a < 90 && b < 90 && c < 90) {
           classificacao = "acutângulo";
       } else if (a == 90 || b == 90 || c == 90) {
           classificacao = "rectângulo";
       } else if (a > 90 || b > 90 || c > 90) {
           classificacao = "obtusângulo";
       }

        return classificacao;

    }

    public static String exercicio_XVI_Verificacao(double a, double b, double c) {

        String verificacao = "";

        if (a > 0 && b > 0 && c > 0) {
            if (a + b + c == 180) {
                verificacao = "possível";
            } else {
                verificacao = "impossível";
            }
        } else {
            verificacao = "impossível";
        }

        return verificacao;
    }

    public static String exercicio_XVII_Processamento(int horasPartida, int minPartida, int horasViagem, int minViagem) {

        int minTotalChegada = (horasPartida*60) + minPartida + (horasViagem*60) + minViagem;
        int horasChegada = minTotalChegada/60; //divisão de dois int vai dar int (arredondado para baixo)
        int minChegada = minTotalChegada%60;
        String dia = "";

        if (horasChegada > 24) {
            return "O comboio chega às " + (horasChegada-24) + ":" + minChegada + " de amanhã.";
        } else {
            return "O comboio chega às " + horasChegada + ":" + minChegada + " de hoje.";
        }

    }

    public static String exercicio_XVIII_Processamento(int horasInicio, int minInicio, int segInicio, int segTarefa) {

        int segTotalInicio = (horasInicio*60*60) + (minInicio*60) + segInicio; //55170
        int segTotalFinal = segTotalInicio + segTarefa; //145170
        int horasFinal = segTotalFinal/(60*60); //40
        int minFinal = (segTotalFinal%(60*60))/60; //19
        int segFinal = ((segTotalFinal%(60*60))%60); //30

        if (horasFinal>24) {
            return "A tarefa termina às " + (horasFinal-24) + ":" + minFinal + ":" + segFinal + " de amanhã.";
        } else {
            return "A tarefa termina às " + horasFinal + ":" + minFinal + ":" + segFinal + " de hoje.";
        }

    }

    public static double exercicio_XIX_Processamento(int horasSemana) {

        double salarioSemanal;

        if (horasSemana <= 36) {
            salarioSemanal = horasSemana*7.5;
        } else if (horasSemana <= 41) {
            salarioSemanal = (36*7.5) + (horasSemana-36)*10;
        } else {
            salarioSemanal = (36*7.5) + (5*10) + (horasSemana-41)*15;
        }

        return salarioSemanal;

    }

    public static double exercicio_XX_CustoKit(int tipo, int dia) {

        double custoKit;

        if (dia==2 || dia==3 || dia==4 || dia==5 || dia==6) {
            if (tipo==1) {
                custoKit = 30;
            } else if (tipo==2) {
                custoKit = 50;
            } else if (tipo==3) {
                custoKit = 100;
            } else {
                custoKit = 0;
            }
        } else if (dia==7 || dia==8 || dia==1) {
            if (tipo==1) {
                custoKit = 40;
            } else if (tipo==2) {
                custoKit = 70;
            } else if (tipo==3) {
                custoKit = 140;
            } else {
                custoKit = 0;
            }
        } else {
            custoKit = 0;
        }

        return custoKit;

    }

    public static double exercicio_XX_CustoKm(double distancia) {
        return (distancia*2);
    }
}
