import java.util.Scanner;

public class Bloco_3_ex_20 {

    public static void main(String[] args) {

        exercise_XX();

    }

    public static void exercise_XX() {

        int number;
        String classification;
        Scanner read = new Scanner(System.in);

        System.out.println("Insert a positive integer: ");
        number = read.nextInt();

        if (number > 0) {
            classification = exercise_XX_Run(number);
            System.out.println(number + " is " + classification);
        } else {
            System.out.println("The number has to be greater than 0.");
        }
    }

    public static String exercise_XX_Run(int number) {

        int i, sum = 0;
        String classification;

        for (i = 1; i<= number; i++) {
            if (number%i == 0 && i!=number) {
                sum+=i;
            }
        }

        if (number == sum) {
            classification = "perfeito";
        } else if (number > sum) {
            classification = "reduzido";
        } else {
            classification = "abundante";
        }

        return classification;

    }
}
