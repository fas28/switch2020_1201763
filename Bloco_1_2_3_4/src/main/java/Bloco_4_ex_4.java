import java.util.Arrays;
import java.util.Scanner;

public class Bloco_4_ex_4 {

    /**
     * Bloco 4, Ex.4 - obtain an array with the even numbers of an array
     *
     * @param array - a 1D array of int type
     * @return - a 1D array of int type that contains the even numbers of an array
     */

    public static int[] getArrayOfEvens(int[] array) {

        int i, j = 0, count = 0;

        //Check if array is {0}
        if (array.length == 1 && array[0] == 0) {
            int[] evenArray = new int[] {0};
            return evenArray;
        } else {

        //Count the even numbers of the array
        for (i = 0; i < array.length; i++) {
            if (isEvenNumber(array[i])) {
                count += 1;
            }
        }

        //Create an array with the dimension equal to the count of even numbers
        int[] evenArray = new int[count];

        //Fill in the created array with the even numbers of the original array
        for (i = 0; i < array.length; i++) {
            if (isEvenNumber(array[i])) {
                evenArray[j] = array[i];
                j++;
            }

        }
            return evenArray;
        }

    }

    /**
     * Determines if a number is even
     *
     * @param number an integer
     * @return true if number is even, false if number is not even
     */

    public static boolean isEvenNumber(int number) {

        if (number % 2 == 0 && number != 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Bloco 4, Ex.4 - obtain an array with the uneven numbers of an array
     *
     * @param array - a 1D array of int type
     * @return - a 1D array of int type that contains the uneven numbers of an array
     */

    public static int[] getArrayOfUnevens(int[] array) {

        int i, j = 0, count = 0;

        //Check if array is {0}
        if (array.length == 1 && array[0] == 0) {
            int[] unevenArray = new int[]{0};
            return unevenArray;
        } else {

            //Count the uneven numbers of the array
            for (i = 0; i < array.length; i++) {
                if (isUnevenNumber(array[i])) {
                    count += 1;
                }
            }

            //Create an array with the dimension equal to the count of uneven numbers
            int[] unevenArray = new int[count];

            //Fill in the created array with the uneven numbers of the original array
            for (i = 0; i < array.length; i++) {
                if (isUnevenNumber(array[i])) {
                    unevenArray[j] = array[i];
                    j++;
                }

            }
            return unevenArray;

        }
    }
    /**
     * Determines if a number is uneven
     *
     * @param number an integer
     * @return true if number is uneven, false if number is not uneven
     */

    public static boolean isUnevenNumber(int number) {

        if (number % 2 != 0 && number != 0) {
            return true;
        } else {
            return false;
        }

    }

}

