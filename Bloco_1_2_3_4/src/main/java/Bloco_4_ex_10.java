import java.util.Arrays;

public class Bloco_4_ex_10 {

    /**
     * Bloco 4, Ex.10-a) given an array of integers, return the lowest element in it
     * @param array 1D array of int type
     * @return an integer that is the lowest element of the given array
     */

    public static int getLowest(int[] array) {

        int lowest = array[0], i, len = array.length;

        for (i = 0; i < len; i++) {
            if (array[i] < lowest) {
                lowest = array[i];
            }
        }

        return lowest;
    }

    /**
     * Bloco 4, Ex.10-b) given an array of integers, return the highest element in it
     * @param array 1D array of int type
     * @return an integer that is the highest element of the given array
     */

    public static int getHighest(int[] array) {

        int highest = array[0], i, len = array.length;

        for (i = 0; i < len; i++) {
            if (array[i] > highest) {
                highest = array[i];
            }
        }

        return highest;
    }

    /**
     *  Bloco 4, Ex.10-c) given an array of integers, return the average of its values
     * @param array 1D array of int type
     * @return a double that is the average of the values of the array
     */

    public static double getAverage(int[] array) {

        int len = array.length, i;
        double sum = 0;

        for (i = 0; i < len; i++) {
            sum += array[i];
        }

        return (sum / len);

    }

    /**
     *  Bloco 4, Ex.10-d) given an array of integers, return the product of its values
     * @param array 1D array of int type
     * @return an integer that is the product of the values of the array
     */

    public static int getProduct(int[] array) {

        int len = array.length, i, product = 1;

        for (i = 0; i < len; i++) {
            product *= array[i];
        }

        return product;

    }

    /**
     * Bloco 4, Ex.10-e) given an array of integers, return an array with its unique elements
     * @param array 1D array of int type
     * @return a 1D array of int type that contains the unique elements of the original array
     */

    public static int[] getUnique(int[] array) {

        int len = array.length, i, j, k = 0, count = 0;
        int[] unique = new int[len];

        for (i = 0; i < len; i++) {
            count = 0;
            //Check how many times the number appears in the array
            for (j = 0; j < len; j++) {
                if (array[i] == array[j]) {
                    count += 1;
                }
            }
            //If it appears only once, the number is added to the unique array; otherwise -1 is added
            if (count == 1) {
                unique[k] = array[i];
                k++;
            } else {
                unique[k] = -1;
                k++;
            }
        }

        //Reduce the array so that -1 values are removed
        int[] unique_2 = Bloco_4_ex_8.reduceArray(unique, -1);

        return unique_2;

    } //Improve so that -1 counts as a unique number when it is unique

    /**
     * Bloco 4, Ex.10-f) returns the reverse of a given array
     * @param array a 1D int array
     * @return a 1D int array that is the reverse of the original array
     */

    public static int[] getReverse(int[] array) {

        int[] reverse = Bloco_4_ex_9.getReverseArray(array);

        return reverse;

    }

    /**
     * Bloco 4, Ex.10-g) creates an array with the prime numbers found in a given array
     * @param array a 1D int array
     * @return a 1D int array that contains the prime numbers found in a given array
     */

    public static int[] getPrimeNumbers(int[] array) {

        int i, j, len = array.length, count = 0, k = 0, l;
        int[] primeNumbers = new int[len];

        for (i = 0; i < len; i++) {
            count = 0;
            //Check how many divisors the number (array[i]) has
            for (j = 1; j <= array[i]; j++) {
                if (array[i] % j == 0 && array[i] != 1 && array[i] > 0) {
                    count += 1;
                }
            }
            //If it has only 2 divisors, it's a prime number and it's added to the prime numbers array; otherwise -1 is added
            if (count == 2) {
                if (Bloco_4_ex_15.findValueInArray(array[i],primeNumbers)) {
                    primeNumbers[k] = -1;
                    k++;
                } else {
                    primeNumbers[k] = array[i];
                    k++;
                }
            } else {
                primeNumbers[k] = -1;
                k++;
            }
        }

        //Reduce the array so that -1 values are removed
        int[] primeNumbers_2 = Bloco_4_ex_8.reduceArray(primeNumbers,-1);

        return primeNumbers_2;

    }

}