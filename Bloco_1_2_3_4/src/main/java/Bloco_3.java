import java.util.Scanner;

public class Bloco_3 {

    public static void main(String[] args) {

        //exercise_I();
        //exercise_II();
        //exercise_III();
        //exercise_IV_a();
        //exercise_IV_b();
        //exercise_IV_c();
        //exercise_IV_d();
        //exercise_IV_e();
        //exercise_V_a();
        //exercise_V_b();
        //exercise_V_c();
        //exercise_V_d();
        //exercise_V_e();
        //exercise_V_f();
        //exercise_V_g();
        //exercise_V_h();
        //exercise_VI_a();
        //exercise_VI_b();
        //exercise_VI_c();
        //exercise_VI_d();
        //exercise_VI_e();
        //exercise_VI_f();
        //exercise_VI_g();
        //exercise_VI_h();
        //exercise_VI_i();
        //exercise_VI_j();
        //exercise_VII_a();
        //exercise_VII_b();
        //exercise_VII_c();
        //exercise_VII_d();
        //exercise_VII_e();
        //exercise_VIII();
        //exercise_IX();
        //exercise_X();
        //exercise_XI();
        //exercise_XII();
        //exercise_XIII();
        //exercise_XIV();
        //exercise_XV();
        exercise_XVI();

    }

    public static void exercise_I() {

        System.out.println("Insira um número inteiro: ");
        Scanner ler = new Scanner(System.in);
        int num = ler.nextInt();

        if (num > 0) {
            int res = exercicio_I_Processamento(num);
            System.out.println("O resultado do fatorial de " + num + " é " + res);
        } else {
            System.out.println("Introduza um número inteiro positivo.");
        }

    }

    public static void exercise_II() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de alunos da turma: ");
        int N = ler.nextInt();
        double[] notas = new double[N];

        for (int i = 0; i < notas.length; i++) {
            System.out.println("Insira a nota obtida pelo aluno nº " + (i + 1));
            notas[i] = ler.nextDouble();
        }

        double percentagem = exercicio_II_Percentagem(notas);
        double media = exercicio_II_Media(notas);

        System.out.println("A percentagem de notas positivas é " + String.format("%.2f", percentagem) + "% e a média " +
                "de notas negativas é " + String.format("%.2f", media));

    }

    public static void exercise_II_WithoutArrays() {

    }

    public static void exercise_III() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Introduza o número de elementos da sequência: ");
        int N = ler.nextInt();
        int[] sequencia = new int[N];

        for (int i = 0; i < sequencia.length; i++) {
            System.out.println("Insira o próximo número da sequência: ");
            sequencia[i] = ler.nextInt();
        }

        double percentagem = exercicio_III_Percentagem(sequencia);
        double media = exercicio_III_Media(sequencia);

        System.out.println("A percentagem de valores pares é: " + String.format("%.2f", percentagem * 100) + "%, e a " +
                "média dos valores ímpares é " + String.format("%.2f", media));

    }

    public static void exercise_IV_a() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();

        int nr = exercicio_IV_a_Processamento(min, max);

        System.out.println("Existem " + nr + " múltiplos de 3 neste intervalo.");

    }

    public static void exercise_IV_b() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int num = ler.nextInt();

        int nr = exercicio_IV_b_Processamento(min, max, num);

        System.out.println("Existem " + nr + " múltiplos de " + num + " neste intervalo.");

    }

    public static void exercise_IV_c() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();

        int nr_3 = exercicio_IV_b_Processamento(min, max, 3);
        int nr_5 = exercicio_IV_b_Processamento(min, max, 5);

        System.out.println("Existem neste intervalo " + nr_3 + " múltiplos de 3 e " + nr_5 + " múltiplos de 5.");


    }

    public static void exercise_IV_d() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int num_1 = ler.nextInt();
        System.out.println("Selecione outro número pertencente a esse intervalo: ");
        int num_2 = ler.nextInt();

        int nr_1 = exercicio_IV_b_Processamento(min, max, num_1);
        int nr_2 = exercicio_IV_b_Processamento(min, max, num_2);

        System.out.println("Existem neste intervalo " + nr_1 + " múltiplos de " + num_1 + ", e " + nr_2 + " múltiplos" +
                " de " + num_2 + ".");

    }

    public static void exercise_IV_e() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int num_1 = ler.nextInt();
        System.out.println("Selecione outro número pertencente a esse intervalo: ");
        int num_2 = ler.nextInt();

        int soma = exercicio_IV_e_Processamento(min, max, num_1, num_2);

        System.out.println("A soma dos múltiplos de " + num_1 + " e de " + num_2 + " é de " + soma);

    }

    public static void exercise_V_a() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();

        int somaPares = exercicio_V_SomaParOuImpar(min, max, "par");

        System.out.println("A soma dos números pares deste intervalo é igual a " + somaPares + ".");

    }

    public static void exercise_V_b() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();

        int qtdPares = exercicio_V_QtdParOuImpar(min, max, "par");

        System.out.println("A quantidade de números pares deste intervalo é de " + qtdPares + ".");

    }

    public static void exercise_V_c() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();

        int somaImpares = exercicio_V_SomaParOuImpar(min, max, "ímpar");

        System.out.println("A soma de números ímpares deste intervalo é de " + somaImpares + ".");

    }

    public static void exercise_V_d() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira o limite inferior do intervalo: ");
        int min = ler.nextInt();
        System.out.println("Insira o limite superior do intervalo: ");
        int max = ler.nextInt();

        int qtdImpares = exercicio_V_QtdParOuImpar(min, max, "ímpar");

        System.out.println("A quantidade de números ímpares deste intervalo é de " + qtdImpares + ".");

    }

    public static void exercise_V_e() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira um dos limites do intervalo: ");
        int lim_1 = ler.nextInt();
        System.out.println("Insira o outro limite do intervalo: ");
        int lim_2 = ler.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int num = ler.nextInt();

        if (lim_1 >= 0 && lim_2 >= 0) {
            int soma = exercicio_V_e_Processamento(lim_1, lim_2, num);
            System.out.println("A soma dos múltiplos de " + num + " é igual a " + soma + ".");
        } else {
            System.out.println("Os limites do intervalo devem ser inteiros maiores ou iguais a zero.");
        }
    }

    public static void exercise_V_f() {

        Scanner ler = new Scanner(System.in);
        System.out.println("Insira um dos limites do intervalo: ");
        int lim_1 = ler.nextInt();
        System.out.println("Insira o outro limite do intervalo: ");
        int lim_2 = ler.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int num = ler.nextInt();

        if (lim_1 >= 0 && lim_2 >= 0) {
            int soma = exercicio_V_f_Processamento(lim_1, lim_2, num);
            System.out.println("A soma dos múltiplos de " + num + " é igual a " + soma + ".");
        } else {
            System.out.println("Os limites do intervalo devem ser inteiros maiores ou iguais a zero.");
        }

    }

    public static void exercise_V_g() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insira um dos limites do intervalo: ");
        int lim_1 = read.nextInt();
        System.out.println("Insira o outro limite do intervalo: ");
        int lim_2 = read.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int num = read.nextInt();

        if (lim_1 >= 0 && lim_2 >= 0) {
            double avg = exercise_V_g_Run(lim_1, lim_2, num);
            System.out.println("A média dos múltiplos de " + num + " é igual a " + String.format("%.2f", avg) + ".");
        } else {
            System.out.println("Os limites do intervalo devem ser inteiros maiores ou iguais a zero.");
        }

    }

    public static void exercise_V_h() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insira um dos limites do intervalo: ");
        int lim_1 = read.nextInt();
        System.out.println("Insira o outro limite do intervalo: ");
        int lim_2 = read.nextInt();
        System.out.println("Selecione um número pertencente a esse intervalo: ");
        int x = read.nextInt();
        System.out.println("Selecione outro número pertencente a esse intervalo: ");
        int y = read.nextInt();

        if (lim_1 >= 0 && lim_2 >= 0) {
            double avg_x = exercise_V_g_Run(lim_1, lim_2, x);
            double avg_y = exercise_V_g_Run(lim_1, lim_2, y);
            System.out.println("A média dos múltiplos de " + x + " é igual a " + String.format("%.2f", avg_x) + ".");
            System.out.println("A média dos múltiplos de " + y + " é igual a " + String.format("%.2f", avg_y) + ".");
        } else {
            System.out.println("Os limites do intervalo devem ser inteiros maiores ou iguais a zero.");
        }

    }

    public static void exercise_VI_a() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        int nDigits = exercise_VI_a_Run(num);

        System.out.println("The number " + num + " has " + nDigits + " digits.");

    }

    public static void exercise_VI_b() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        int nEven = exercise_VI_b_Run(num);

        System.out.println("The number " + num + " has " + nEven + " even digits.");

    }

    public static void exercise_VI_c() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        int nUneven = exercise_VI_c_Run(num);

        System.out.println("The number " + num + " has " + nUneven + " uneven digits.");

    }

    public static void exercise_VI_d() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        int sumDigits = exercise_VI_d_Run(num);

        System.out.println("The sum of the digits of number " + num + " is " + sumDigits);

    }

    public static void exercise_VI_e() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        int sumEvenDigits = exercise_VI_e_Run(num);

        System.out.println("The sum of the even digits of number " + num + " is " + sumEvenDigits);

    }

    public static void exercise_VI_f() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        int sumUnevenDigits = exercise_VI_f_Run(num);

        System.out.println("The sum of the uneven digits of number " + num + " is " + sumUnevenDigits);

    }

    public static void exercise_VI_g() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        double avgDigits = exercise_VI_g_Run(num);

        System.out.println("The average of the digits of number " + num + " is " + avgDigits);
    }

    public static void exercise_VI_h() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        double avgEvenDigits = exercise_VI_h_Run(num);

        System.out.println("The average of the even digits of number " + num + " is " + avgEvenDigits);
    }

    public static void exercise_VI_i() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        double avgUnevenDigits = exercise_VI_i_Run(num);

        System.out.println("The average of the uneven digits of number " + num + " is " + avgUnevenDigits);

    }

    public static void exercise_VI_j() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        long num = read.nextLong();

        long reverseNumber = exercise_VI_j_Run(num);

        System.out.println("The reverse of number " + num + " is " + reverseNumber);

    } //Se o número terminar em 0, não deixar colocar zero no início do inverso

    public static void exercise_VII_a() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        long num = read.nextLong();

        String result = exercise_VII_a_Run(num);

        System.out.println(result);

    }

    public static void exercise_VII_b() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a long integer: ");
        int num = read.nextInt();

        String result = exercise_VII_b_Run(num);

        if (result == "yes") {
            System.out.println(num + " is an Armstrong number.");
        } else {
            System.out.println(num + " is not an Armstrong number.");
        }

    }

    public static void exercise_VII_c() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert one of the limits of the interval: ");
        long num_1 = read.nextLong();
        System.out.println("Insert the other limit of the interval: ");
        long num_2 = read.nextLong();

        long firstPalindrome = exercise_VII_c_Run(num_1, num_2);

        System.out.println("The first palindrome in the interval is " + firstPalindrome);

    }

    public static void exercise_VII_d() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert one of the limits of the interval: ");
        long num_1 = read.nextLong();
        System.out.println("Insert the other limit of the interval: ");
        long num_2 = read.nextLong();

        long biggestPalindrome = exercise_VII_d_Run(num_1, num_2);

        System.out.println("The biggest palindrome in the interval is " + biggestPalindrome);

    }

    public static void exercise_VII_e() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert one of the limits of the interval: ");
        long num_1 = read.nextLong();
        System.out.println("Insert the other limit of the interval: ");
        long num_2 = read.nextLong();

        int nrPalindrome = exercise_VII_e_Run(num_1, num_2);

        System.out.println("The number of palindromes in the interval is " + nrPalindrome);

    }

    public static void exercise_VII_f() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert one of the limits of the interval: ");
        int num_1 = read.nextInt();
        System.out.println("Insert the other limit of the interval: ");
        int num_2 = read.nextInt();

        int firstArmstrong = exercise_VII_f_Run(num_1, num_2);

        System.out.println("The first Armstrong number in the interval is " + firstArmstrong);

    }

    public static void exercise_VII_g() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert one of the limits of the interval: ");
        int num_1 = read.nextInt();
        System.out.println("Insert the other limit of the interval: ");
        int num_2 = read.nextInt();

        int nrArmstrong = exercise_VII_g_Run(num_1, num_2);

        System.out.println("The number of Armstrong numbers in the interval is " + nrArmstrong);

    }

    public static void exercise_VIII() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a number: ");
        int num = read.nextInt();

        int lowestNumber = exercise_VIII_Run(num);

        System.out.println("The lowest number is " + lowestNumber);

    }

    public static void exercise_IX() {

        double baseIncome, extraHours = 0, monthIncome, sumIncome = 0;
        int countIncome = 0;
        Scanner read = new Scanner(System.in);

        do {
            System.out.println("Insert the employee's base income: ");
            baseIncome = read.nextDouble();
            if (baseIncome > 0) {
                System.out.println("Insert the employee's extra hours: ");
                extraHours = read.nextDouble();
                if (extraHours == -1 || extraHours >= 0) {
                    monthIncome = exercise_IX_Run(baseIncome, extraHours);
                    System.out.println("The income of the employee is " + monthIncome + "€.");
                    sumIncome += monthIncome;
                    countIncome += 1;
                } else {
                    System.out.println("Extra hours can only be -1, 0 or positive numbers.");
                }
            } else {
                System.out.println("Base salary has to be greater than zero.");
            }
        } while (extraHours != -1);

        System.out.println("The average month income of the company is " + String.format("%.2f",
                sumIncome / countIncome) + "€.");

    } //Como separar a leitura de dados do processamento?

    public static void exercise_X() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert a number: ");
        int num = read.nextInt();

        int highestNumber = exercise_X_Run(num);

        System.out.println("The highest number is " + highestNumber);

    }

    public static void exercise_XI() {

        Scanner read = new Scanner(System.in);
        System.out.println("Insert an integer between 1 and 20: ");
        int n = read.nextInt();
        if (n >= 0 && n <= 10) {
            double nrWays = exercise_XI_Run(n);
            System.out.println("There are " + String.format("%.0f", nrWays) + " ways to obtain " + n + " using two " +
                    "integers between 0 and 10.");
        } else {
            System.out.println("You should insert an integer between 1 and 20.");
        }

    }

    public static void exercise_XII() {

        double a, b, c;
        String result, answer = "yes";
        Scanner read = new Scanner(System.in);

        while (answer.equals("yes")) {
            System.out.println("Insert the value of a: ");
            a = read.nextDouble();
            System.out.println("Insert the value of b: ");
            b = read.nextDouble();
            System.out.println("Insert the value of c: ");
            c = read.nextDouble();
            result = exercise_XII_Run(a, b, c);
            System.out.println(result);
            System.out.println("Do you wish to solve another 2nd degree equation? (yes/no)");
            answer = read.next();
        }

    }

    public static void exercise_XIII() {

        int code = 0;
        String classification;
        Scanner read = new Scanner(System.in);

        do {
            System.out.println("Insert the code: ");
            code = read.nextInt();
            classification = exercise_XIII_Run(code);
            System.out.println(classification);
        } while (code != 0);

    }

    public static void exercise_XIV() {

        double value = 0, convertedValue;
        String currency;
        Scanner read = new Scanner(System.in);

        while (value >= 0) {
            System.out.println("Insert the value in €: ");
            value = read.nextDouble();
            if (value >= 0) {
                System.out.println("Insert the currency you wish to convert to (D-Dólar; L-Libra; I-Iene; CS-Coroa " +
                        "Sueca; FS-Franco Suíço): ");
                currency = read.next();
                convertedValue = exercise_XIV_Run(value, currency);
                System.out.println(value + "€ is equal to " + String.format("%.2f", convertedValue) + currency);
            } else {
                System.out.println("Finished.");
            }
        }

    }

    public static void exercise_XV() {

        int mark = 0;
        String qualitative;
        Scanner read = new Scanner(System.in);

        while (mark >= 0) {
            System.out.println("Insert a student's mark (an integer): ");
            mark = read.nextInt();
            if (mark >= 0) {
                qualitative = exercise_XV_Run(mark);
                if (qualitative.equals("A mark cannot be greater than 20.")) {
                    System.out.println(qualitative);
                } else {
                    System.out.println(mark + " is " + qualitative);
                }
                } else {
                    System.out.println("Finished.");
            }
        }
    }

    public static void exercise_XVI() {

        double baseIncome, netIncome;
        Scanner read = new Scanner(System.in);

        System.out.println("Insert the employee's base salary: ");
        baseIncome = read.nextDouble();
        netIncome = exercise_XVI_Run(baseIncome);

        if (baseIncome > 0) {
            System.out.println("The employee's net income is: " + String.format("%.2f",netIncome) + "€.");
        } else {
            System.out.println("The base income has to be greater than zero.");
        }

    }

    public static int exercicio_I_Processamento(int num) {

        int x, res = 1;

        for (x = num; x >= 1; x--) {
            res *= x;
        }

        return res;
    }

    public static double exercicio_II_Percentagem(double notas[]) {

        double nPositivas = 0;
        double percentagemPositivas;

        for (int i = 0; i < notas.length; i++) {
            if (notas[i] >= 10) {
                nPositivas += 1;
            }
        }

        return percentagemPositivas = (nPositivas / notas.length) * 100.0;
    }

    public static double exercicio_II_Media(double notas[]) {

        int nNegativas = 0;
        double somaNegativas = 0, mediaNegativas;

        for (int i = 0; i < notas.length; i++) {
            if (notas[i] < 10) {
                nNegativas += 1;
                somaNegativas += notas[i];
            }
        }

        return mediaNegativas = somaNegativas / nNegativas;

    }

    public static double exercicio_III_Percentagem(int[] sequencia) {

        int i, posNeg = 0;
        double contador = 0;

        for (i = 0; sequencia[i] >= 0; i++) {
            posNeg += 1;
        }

        for (i = 0; i < posNeg; i++) {
            if (sequencia[i] % 2 == 0) {
                contador += 1;
            }
        }
        return contador / (posNeg);
    }

    public static double exercicio_III_Media(int[] sequencia) {

        int i, posNeg = 0;
        double contador = 0, soma = 0;

        for (i = 0; sequencia[i] >= 0; i++) {
            posNeg += 1;
        }

        for (i = 0; i < posNeg; i++) {
            if (sequencia[i] % 2 != 0) {
                contador += 1;
                soma += sequencia[i];
            }
        }

        return soma / contador;
    }

    public static int exercicio_IV_a_Processamento(int min, int max) {

        int i = 0, nr = 0;

        for (i = min; i <= max; i++) {
            if (i % 3 == 0) {
                nr += 1;
            }
        }

        return nr;
    }

    public static int exercicio_IV_b_Processamento(int min, int max, int num) {

        int i = 0, nr = 0;

        for (i = min; i <= max; i++) {
            if (i % num == 0) {
                nr += 1;
            }
        }

        return nr;

    }

    public static int exercicio_IV_e_Processamento(int min, int max, int num_1, int num_2) {

        int i = 0, soma_1 = 0, soma_2 = 0;

        for (i = min; i <= max; i++) {
            if (i % num_1 == 0) {
                soma_1 += i;
            }
        }

        for (i = min; i <= max; i++) {
            if (i % num_2 == 0) {
                soma_2 += i;
            }
        }

        return soma_1 + soma_2;
    }

    public static int exercicio_V_SomaParOuImpar(int min, int max, String tipo) {

        int soma = 0;

        switch (tipo) {
            case "par":
                for (int i = min; i <= max; i++) {
                    if (i % 2 == 0 & i != 0) {
                        soma += i;
                    }
                }
                break;
            case "ímpar":
                for (int i = min; i <= max; i++) {
                    if (i % 2 != 0 && i != 0) {
                        soma += i;
                    }
                }
                break;
        }

        return soma;
    }

    public static int exercicio_V_QtdParOuImpar(int min, int max, String tipo) {

        int soma = 0;

        switch (tipo) {
            case "par":
                for (int i = min; i <= max; i++) {
                    if (i % 2 == 0 && i != 0) {
                        soma += 1;
                    }
                }
                break;
            case "ímpar":
                for (int i = min; i <= max; i++) {
                    if (i % 2 != 0 && i != 0) {
                        soma += 1;
                    }
                }
                break;
        }

        return soma;

    }

    public static int exercicio_V_e_Processamento(int lim_1, int lim_2, int num) {

        int max, min, i = 0, soma = 0;

        if (lim_1 > lim_2) {
            max = lim_1;
            min = lim_2;

            for (i = min; i <= max; i++) {
                if (i % num == 0 && i != 0) {
                    soma += i;
                }
            }
        } else if (lim_2 > lim_1) {
            max = lim_2;
            min = lim_1;

            for (i = min; i <= max; i++) {
                if (i % num == 0 && i != 0) {
                    soma += i;
                }
            }
        }

        return soma;

    }

    public static int exercicio_V_f_Processamento(int lim_1, int lim_2, int num) {

        int max, min, i = 0, product = 1;

        if (lim_1 > lim_2) {
            max = lim_1;
            min = lim_2;

            for (i = min; i <= max; i++) {
                if (i % num == 0 && i != 0) {
                    product *= i;
                }
            }
        } else if (lim_2 > lim_1) {
            max = lim_2;
            min = lim_1;

            for (i = min; i <= max; i++) {
                if (i % num == 0 && i != 0) {
                    product *= i;
                }
            }
        }

        return product;

    }

    public static double exercise_V_g_Run(int lim_1, int lim_2, int num) {

        int max, min, i = 0;
        double sum = 0, count = 0, avg = 0;

        if (lim_1 > lim_2) {
            max = lim_1;
            min = lim_2;

            for (i = min; i <= max; i++) {
                if (i % num == 0 && i != 0) {
                    count += 1;
                    sum += i;
                }
            }
            avg = sum / count;
        } else if (lim_2 > lim_1) {
            max = lim_2;
            min = lim_1;

            for (i = min; i <= max; i++) {
                if (i % num == 0 && i != 0) {
                    count += 1;
                    sum += i;
                }
            }
            avg = sum / count;
        }

        return avg;
    }

    public static int exercise_VI_a_Run(int num) {

        int count = 0, partial = num;

        while (partial > 0) {
            partial /= 10.0;
            count += 1;
        }

        return count;

    }

    public static int exercise_VI_b_Run(int num) {

        int countEven = 0, partial = num, remainder = 0;

        if ((num % 10) % 2 == 0 && (num % 10) != 0) {
            countEven += 1;
        }

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            if (remainder % 2 == 0 && remainder != 0) {
                countEven += 1;
            }
        }

        return countEven;

    }

    public static int exercise_VI_c_Run(int num) {

        int countUneven = 0, partial = num, remainder = 0;

        if ((num % 10) % 2 != 0 && (num % 10) != 0) {
            countUneven += 1;
        }

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            if (remainder % 2 != 0 && remainder != 0) {
                countUneven += 1;
            }
        }

        return countUneven;

    }

    public static int exercise_VI_d_Run(int num) {

        int sum, partial = num, remainder;

        remainder = partial % 10; //Para obter o primeiro algarismo
        sum = remainder;

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            sum += remainder;
        }

        return sum;

    }

    public static int exercise_VI_e_Run(int num) {

        int sumEven = 0, partial = num, remainder = 0;

        if ((num % 10) % 2 == 0 && (num % 10) != 0) {
            sumEven += num % 10;
        }

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            if (remainder % 2 == 0 && remainder != 0) {
                sumEven += remainder;
            }
        }

        return sumEven;

    }

    public static int exercise_VI_f_Run(int num) {

        int sumUneven = 0, partial = num, remainder = 0;

        if ((num % 10) % 2 != 0 && (num % 10) != 0) {
            sumUneven += num % 10;
        }

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            if (remainder % 2 != 0 && remainder != 0) {
                sumUneven += remainder;
            }
        }

        return sumUneven;
    }

    public static double exercise_VI_g_Run(int num) {

        double count = 0, sum;
        int partial = num, remainder;

        remainder = partial % 10; //Para obter o primeiro algarismo
        sum = remainder;

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            sum += remainder;
            count += 1;
        }

        return sum / count;
    }

    public static double exercise_VI_h_Run(int num) {

        int partial = num, remainder = 0;
        double count = 0, sum = 0;

        if ((num % 10) % 2 == 0 && (num % 10) != 0) {
            sum += num % 10;
            count += 1;
        }

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            if (remainder % 2 == 0 && remainder != 0) {
                sum += remainder;
                count += 1;
            }
        }

        return sum / count;
    }

    public static double exercise_VI_i_Run(int num) {

        int partial = num, remainder = 0;
        double count = 0, sum = 0;

        if ((num % 10) % 2 != 0 && (num % 10) != 0) {
            sum += num % 10;
            count += 1;
        }

        while (partial > 0) {
            partial /= 10.0;
            remainder = partial % 10;
            if (remainder % 2 != 0 && remainder != 0) {
                sum += remainder;
                count += 1;
            }
        }

        return sum / count;

    }

    public static long exercise_VI_j_Run(long num) {

        long reverse = 0L, partial = num, remainder = 0L;

        while (partial > 0L) {
            remainder = partial % 10L;
            reverse = reverse * 10L + remainder;
            partial /= 10L;
        }

        return reverse;

    }

    public static String exercise_VII_a_Run(long num) {

        long reverse = exercise_VI_j_Run(num);

        if (reverse == num) {
            return "Number " + num + " is a palindrome.";
        } else {
            return "Number " + num + " is not a palindrome.";
        }
    }

    public static String exercise_VII_b_Run(int num) {

        int partial = num, remainder, sum_pow = 0;

        while (partial > 0) {
            remainder = partial % 10;
            partial /= 10.0;
            sum_pow += Math.pow(remainder, 3);
        }

        if (sum_pow == num) {
            return "yes";
        } else {
            return "no";
        }

    }

    public static long exercise_VII_c_Run(long num_1, long num_2) {

        long temp = 0L, min, max = 0L, i, reverse = 0L, firstPalindrome = 0L;
        boolean isPalindrome = false;


        if (num_1 > num_2) {
            max = num_1;
            min = num_2;

        } else {
            max = num_2;
            min = num_1;
        }

        i = min;

        while (i <= max && isPalindrome == false) {
            reverse = exercise_VI_j_Run(i);
            if (reverse == i) {
                firstPalindrome = i;
                isPalindrome = true;
            }
            i++;
        }

        return firstPalindrome;
    }

    public static long exercise_VII_d_Run(long num_1, long num_2) {

        long min, max = 0L, i, reverse = 0L, biggestPalindrome = 0L;
        boolean isPalindrome = false;

        if (num_1 > num_2) {
            max = num_1;
            min = num_2;

        } else {
            max = num_2;
            min = num_1;
        }

        i = max;

        while (i >= min && isPalindrome == false) {
            reverse = exercise_VI_j_Run(i);
            if (reverse == i) {
                biggestPalindrome = i;
                isPalindrome = true;
            }
            i--;
        }

        return biggestPalindrome;

    }

    public static int exercise_VII_e_Run(long num_1, long num_2) {

        long min, max = 0L, i, reverse = 0L;
        int nrPalindrome = 0;

        if (num_1 > num_2) {
            max = num_1;
            min = num_2;

        } else {
            max = num_2;
            min = num_1;
        }

        i = min;

        while (i <= max) {
            reverse = exercise_VI_j_Run(i);
            if (reverse == i) {
                nrPalindrome += 1;
            }
            i++;
        }

        return nrPalindrome;
    }

    public static int exercise_VII_f_Run(int num_1, int num_2) {

        int min, max = 0, i, firstArmstrong = 0;
        String isArmstrong;
        boolean check = false;

        if (num_1 > num_2) {
            max = num_1;
            min = num_2;

        } else {
            max = num_2;
            min = num_1;
        }

        i = min;

        while (i <= max && check == false) {
            isArmstrong = exercise_VII_b_Run(i);
            if (isArmstrong == "yes") {
                firstArmstrong = i;
                check = true;
            }
            i++;
        }

        return firstArmstrong;

    }

    public static int exercise_VII_g_Run(int num_1, int num_2) {

        int min, max = 0, i, nrArmstrong = 0;
        String isArmstrong;

        if (num_1 > num_2) {
            max = num_1;
            min = num_2;

        } else {
            max = num_2;
            min = num_1;
        }

        i = min;

        while (i <= max) {
            isArmstrong = exercise_VII_b_Run(i);
            if (isArmstrong == "yes") {
                nrArmstrong += 1;
            }
            i++;
        }

        return nrArmstrong;
    }

    public static int exercise_VIII_Run(int num) {

        int shortestNumber = 0, sum = 0, readNum;
        Scanner read = new Scanner(System.in);

        while (sum <= num) {
            System.out.println("Insert a positive number: ");
            readNum = read.nextInt();
            sum += readNum;

            if (shortestNumber == 0) {
                shortestNumber = readNum;
            }

            if (readNum < shortestNumber) {
                shortestNumber = readNum;
            }
        }

        return shortestNumber;

    } //Como testar este método?

    public static double exercise_IX_Run(double baseIncome, double extraHours) {

        double monthIncome = baseIncome + (0.02 * baseIncome * extraHours);

        return monthIncome;

    }

    public static int exercise_X_Run(int num) {

        int highestNumber = 0, sum = 0, readNum;
        Scanner read = new Scanner(System.in);

        while (sum <= num) {
            System.out.println("Insert a positive number: ");
            readNum = read.nextInt();
            sum += readNum;

            if (highestNumber == 0) {
                highestNumber = readNum;
            }

            if (readNum > highestNumber) {
                highestNumber = readNum;
            }
        }

        return highestNumber;

    } //Como testar este método?

    public static double exercise_XI_Run(int num) {

        int i, j;
        double count = 0;

        for (i = 0; i <= 10; i++) {
            for (j = 0; j <= 10; j++) {
                if (i + j == num) {
                    if (i == j) {
                        count += 1;
                    } else {
                        count += 0.5;
                    }
                }
            }
        }

        return count;
    }

    public static String exercise_XII_Run(double a, double b, double c) {

        String x = "";
        double x1, x2;
        double partial = Math.pow(b, 2) - (4 * a * c);

        if (partial >= 0) {
            x1 = (-b + Math.sqrt(partial)) / (2 * a);
            x2 = (-b - Math.sqrt(partial)) / (2 * a);
            if (x1 != x2) {
                x = "Solutions: " + String.format("%.4f", x1) + " and " + String.format("%.4f", x2);
            } else {
                x = "Solution: " + String.format("%.4f", x1);
            }

        } else {
            x = "Only imaginary solutions that can't be processed in this program.";
        }

        return x;

    }

    public static String exercise_XIII_Run(int code) {

        String classification = "";

        switch (code) {
            case 0:
                classification = "";
                break;
            case 1:
                classification = "Alimento não perecível";
                break;
            case 2:
            case 3:
            case 4:
                classification = "Alimento perecível";
                break;
            case 5:
            case 6:
                classification = "Vestuário";
                break;
            case 7:
                classification = "Higiene pessoal";
                break;
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                classification = "Limpeza e utensílios domésticos";
                break;
            default:
                classification = "Código inválido";
        }

        return classification;

    }

    public static double exercise_XIV_Run(double value, String currency) {

        double convertedValue = 0;

        switch (currency) {
            case "D":
                convertedValue = value * 1.534;
                break;
            case "L":
                convertedValue = value * 0.774;
                break;
            case "I":
                convertedValue = value * 161.480;
                break;
            case "CS":
                convertedValue = value * 9.593;
                break;
            case "FS":
                convertedValue = value * 1.601;
                break;
        }

        return convertedValue;

    }

    public static String exercise_XV_Run(int mark) {

        String qualitative = "";

        if (mark <= 20 && mark >= 18) {
            qualitative = "Muito Bom";
        } else if (mark >= 14 && mark <= 17) {
            qualitative = "Bom";
        } else if (mark >= 10 && mark <= 13) {
            qualitative = "Suficiente";
        } else if (mark >= 5 && mark <= 9) {
            qualitative = "Medíocre";
        } else if (mark >= 0 && mark <= 4) {
            qualitative = "Mau";
        } else {
            qualitative = "A mark cannot be greater than 20.";
        }

        return qualitative;
    }

    public static double exercise_XVI_Run(double baseIncome) {

        double netSalary, i;

        if (baseIncome <= 500) {
            netSalary = (1-0.1)*baseIncome;
        } else if (baseIncome <=1000) {
            netSalary = (1-0.1)*500 + (1-0.15)*(baseIncome-500);
        } else {
            netSalary = (1-0.1)*500 + (1-0.15)*1000 + (1-0.2)*(baseIncome-1000);
        }

        return netSalary;
    }
}



