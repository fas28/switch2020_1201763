public class change {

    public static void main(String[] args) {
        int a = 2;
        System.out.println(a); //prints 2
        int b = changeValue(a);
        System.out.println(a + "," + b); //prints 2,6
        int[] numbers = {7,2};
        System.out.println(numbers[0]); // prints 7
        int c = changeElement(numbers);
        System.out.println(c + " , " + numbers[0]); //prints 26,26!!!

    }

    public static int changeValue(int a) {
        a = 3*a + 5;
        return a;
    }

    public static int changeElement(int[] v) {
        v[0] = v[0]*3 + 5;
        return v[0];
    }
}
