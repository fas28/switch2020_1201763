public class CopyArray {
/*
    public static int[] copyArray(int[] original, int size) {

        //Define tests (ex.):
        // what if size is greater than the original array length? The test should deliver a copy of the original array (not the original array!)
        // what if the size is negative or zero?


        if (size < 0 || original == null) {
            return null;
        } else if (size > original.length) {
            size = original.length;
        }

        int[] clone = new int[size];

        for (int index = 0; index < size; index++) {
            clone[index] = original[index];
        }

        return clone;

    }



    }

    Tests:

    int[] original = {3,5,7};
    int[] expected = {3,5};

    int[] result = copyArray.copyArray(original,2);
    assert.assertArrayEquals(expected, result);
    assert.assertEquals(expected.length, result.length);
    assert.assertNotSame(result, original);
    */
}
