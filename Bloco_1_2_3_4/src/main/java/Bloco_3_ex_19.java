import java.util.Scanner;

public class Bloco_3_ex_19 {

    public static void main(String[] args) {
        exercise_XIX();
    }

    public static void exercise_XIX() {

        int number;
        String newNumber;
        Scanner read = new Scanner(System.in);

        System.out.println("Insert the number: ");
        number = read.nextInt();

        newNumber = exercise_XIX_Run(number);

        System.out.println(newNumber);

    }

    public static String exercise_XIX_Run(int number) {

        int partial = number, digit, even = 0, uneven = 0, zero = 0;
        boolean hasZeros = false;
        String result;

        while (partial > 0) {
            digit = partial%10;
            if (digit != 0) {
                if (digit%2 == 0) {
                    even = even*10 + digit;
                } else {
                    uneven = uneven*10 + digit;
                }
            }
            partial/=10;
        }

        result = even + "" + uneven;

        return result;



    }
}
