public class Bloco_4_ex_12 {

    /**
     * Bloco 4, Ex. 12 - verifies if all the lines of a 2D matrix have the same number of columns
     * @param matrix a 2D matrix of double type
     * @return if all the lines have the same number of columns, returns the number of columns; otherwise, returns a negative number
     */

    public static int checkIfLinesHaveSameNumberOfColumns_DoubleMatrix(double[][] matrix) {

        int i, numLines = matrix.length, numColumns = matrix[0].length;
        boolean result = true;

        for (i = 0; i < numLines; i++) {
            if (matrix[i].length != numColumns && result) {
              numColumns = -1;
              result = false;
            }
        }

        return numColumns;

    }

    public static int checkIfLinesHaveSameNumberOfColumns_IntMatrix(int[][] matrix) {

        int i, numLines = matrix.length, numColumns = matrix[0].length;
        boolean result = true;

        for (i = 0; i < numLines; i++) {
            if (matrix[i].length != numColumns && result) {
                numColumns = -1;
                result = false;
            }
        }

        return numColumns;

    }


}
