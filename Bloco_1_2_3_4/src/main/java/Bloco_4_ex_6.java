public class Bloco_4_ex_6 {

    /**
     * Bloco 4, Ex.6 - trim an array of integers up to a given index
     * @param array a 1D int array
     * @param N a positive integer
     * @return the trimmed array until index N
     */

    public static int[] trimArray(int[] array, int N) {

        int[] shortArray = null;

        if (N > 0) {
            shortArray = new int[N];
            int i;

            for (i = 0; i < N; i++) {
                shortArray[i] = array[i];
            }

        }

        return shortArray;

    }

}
