public class Bloco_4_ex_13 {

    /**
     * Bloco 4, Ex.13 - checks if a matrix of double type is square
     * @param matrix a 2D matrix of double type
     * @return true if it is a square matrix, false if it's not (boolean type)
     */

    public static boolean isSquareMatrix_DoubleMatrix(double[][] matrix) {

        int numLines = matrix.length;
        int numColumns = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_DoubleMatrix(matrix);
        boolean isSquareMatrix = false;

        if (numColumns != -1) {
            if (numColumns == numLines) {
                isSquareMatrix = true;
            }
        }

        return isSquareMatrix;

    }


    /**
     * Bloco 4, Ex.13 - checks if a matrix of int type is square
     * @param matrix a 2D matrix of int type
     * @return true if it is a square matrix, false if it's not (boolean type)
     */

    public static boolean isSquareMatrix_IntMatrix (int[][] matrix) {

        int numLines = matrix.length;
        int numColumns = Bloco_4_ex_12.checkIfLinesHaveSameNumberOfColumns_IntMatrix(matrix);
        boolean isSquareMatrix = false;

        if (numColumns != -1) {
            if (numColumns == numLines) {
                isSquareMatrix = true;
            }
        }

        return isSquareMatrix;

    }

}
