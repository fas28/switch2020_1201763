import java.util.Scanner;

public class Bloco_3_ex_18 {

    public static void main(String[] args) {
        exercise_XVIII();
    }

    public static void exercise_XVIII() {

        int CC, lastNumber;
        boolean isValid;
        Scanner read = new Scanner(System.in);

        System.out.println("Please insert your Cartão de Cidadão number (8 digits): ");
        CC = read.nextInt();

        System.out.println("Please insert the number to the right of your CC number (control digit): ");
        lastNumber = read.nextInt();

        isValid = exercise_XVIII_Run(CC,lastNumber);

        if (isValid) {
            System.out.println(CC + " is a valid CC number.");
        } else {
            System.out.println(CC + " isn't a valid CC number.");
        }

    }

    public static boolean exercise_XVIII_Run(int CC, int lastNumber) {

        int partial = CC, digit, multiplication = 1, sum = 0, nrDigits_1 = 0, nrDigits_2 = 0;
        boolean isValid = false;

        while (partial>0) {
            digit = partial%10;
            nrDigits_1+=1;
            multiplication+=1;
            sum+=(digit*multiplication);
            partial/=10;
        }

        sum+=(lastNumber);

        if(lastNumber/10==0) {
            nrDigits_2 = 1;
        }

        if (sum%11 == 0 && nrDigits_1 == 8 && nrDigits_2 == 1) {
            isValid = true;
        }

        return isValid;

    }
}
