import java.util.Arrays;

public class Bloco_4_ex_17 {

    /**
     * Bloco 4, Ex.17-a) obtain the product between a matrix and a scalar
     * @param matrix a 2D int matrix
     * @param number an Integer number
     * @return a 2D int matrix that is the product between a matrix and a scalar
     */

    public static int[][] getProductOfMatrixAndScalar_IntMatrixAndScalar(int[][] matrix, Integer number) {

        int[][] newMatrix = null;

        if (matrix != null && number != null) {
            newMatrix = new int[matrix.length][matrix[0].length];
            int i, j;

            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[0].length; j++) {
                    newMatrix[i][j] = number * matrix[i][j];
                }
            }
        }

        return newMatrix;

    }

    /**
     * Method to obtain the product between a matrix and a scalar
     * @param matrix a 2D double matrix
     * @param number a Double number
     * @return a 2D double matrix that is the product between a matrix and a scalar
     */

    public static double[][] getProductOfMatrixAndScalar_DoubleMatrixAndScalar(double[][] matrix, Double number) {

        double[][] newMatrix = null;

        if (matrix != null && number != null) {
            newMatrix = new double[matrix.length][matrix[0].length];
            int i, j;

            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[0].length; j++) {
                    newMatrix[i][j] = number * matrix[i][j];
                }
            }
        }

        return newMatrix;

    }

    /**
     * Bloco 4, Ex.17-b) obtains the sum of two matrixes
     * @param matrix_1 a 2D int matrix
     * @param matrix_2 a 2D int matrix
     * @return a 2D int matrix that is the sum of two given matrixes
     */

    public static int[][] getSumOfMatrixes(int[][] matrix_1, int[][] matrix_2) {

        int[][] newMatrix = null;

        if (matrix_1 != null && matrix_2 != null) {
            if (checkIfMatrixesHaveSameDimensions(matrix_1, matrix_2)) {
                newMatrix = new int[matrix_1.length][matrix_1[0].length];
                int i, j;
                for (i = 0; i < matrix_1.length; i++) {
                    for (j = 0; j < matrix_1[0].length; j++) {
                        newMatrix[i][j] = matrix_1[i][j] + matrix_2[i][j];
                    }
                }
            }
        }

        return newMatrix;

    }

    /**
     * Bloco 4, Ex.17-c) obtains the product between two matrixes
     * @param matrix_1 a 2D int matrix
     * @param matrix_2 a 2D int matrix
     * @return a 2D int matrix that is the product between two given matrixes
     */

    public static int[][] getProductOfMatrixes(int[][] matrix_1, int[][] matrix_2) {

        int[][] newMatrix = null;

        if (matrix_1 != null && matrix_2 != null) {
            if (checkIfMatrixesCanBeMultiplied(matrix_1, matrix_2)) {
                int numLinesM1 = matrix_1.length;
                int numColumnsM1 = matrix_1[0].length;
                int numLinesM2 = matrix_2.length;
                int numColumnsM2 = matrix_2[0].length;

                newMatrix = new int[numLinesM1][numColumnsM2];
                int i, j, k = 0, l = 0;

                for (i = 0; i < numLinesM1; i++) {
                    for (j = 0; j < numColumnsM2; j++) {

                        while (k < numColumnsM1 && l < numLinesM2) {
                            newMatrix[i][j] += matrix_1[i][k] * matrix_2[l][j];
                            k++;
                            l++;
                        }
                        k = 0;
                        l = 0;
                    }
                }

                }
            }

        return newMatrix;

        }

    /**
     * Method that determines whether two matrixes have the same dimensions (number of lines and number of columns)
      * @param matrix_1 a 2D int matrix
     * @param matrix_2 a 2D int matrix
     * @return true if the matrixes have the same dimensions, false if they haven't
     */

    public static boolean checkIfMatrixesHaveSameDimensions(int[][] matrix_1, int[][] matrix_2) {

        boolean result = false;

        if (matrix_1 != null && matrix_2 != null) {
            if (matrix_1.length == matrix_2.length && matrix_1[0].length == matrix_2[0].length) {
                result = true;
            }
        }

        return result;

    }

    /**
     * Method that determines whether two matrixes can be multiplied
     * @param matrix_1 a 2D int matrix
     * @param matrix_2 a 2D int matrix
     * @return true if the matrixes can be multiplied, false if they can't
     */

    public static boolean checkIfMatrixesCanBeMultiplied(int[][] matrix_1, int[][] matrix_2) {

        boolean result = false;

        if (matrix_1 != null && matrix_2 != null) {
            if (matrix_1[0].length == matrix_2.length) {
                result = true;
            }
        }

        return result;

    }

}
