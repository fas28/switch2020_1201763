import java.util.Scanner;

public class Bloco_1 {

    //ÓTICA DO UTILIZADOR PARA LEITURA E RETORNO DE DADOS

    public static void main(String[] args) {

        //Inicialização para leitura de dados
        Scanner ler = new Scanner(System.in);


        //EXERCÍCIO 1 - RAPAZES E RAPARIGAS

        //Estrutura de dados
        double rapazes, raparigas;
        double[] percentagemGenero;

        //Leitura de dados
        System.out.println("Introduza o número de rapazes: ");
        rapazes = ler.nextDouble();
        System.out.println("Introduza o número de raparigas: ");
        raparigas = ler.nextDouble();

        //Processamento
        percentagemGenero = exercicio_I_Processamento(rapazes,raparigas);

        //Saída de dados
        System.out.println("Percentagem de rapazes: " + percentagemGenero[0] + "%.");
        System.out.println("Percentagem de raparigas: " + percentagemGenero[1] + "%.");


        //EXERCÍCIO 2 - FLORES

        //Estrutura de dados
        int nRosas, nTulipas;
        double puRosas, puTulipas, pFinal;

        //Leitura de dados
        System.out.println("Introduza o número de rosas: ");
        nRosas = ler.nextInt();
        System.out.println("Introduza o número de tulipas: ");
        nTulipas = ler.nextInt();
        System.out.println("Introduza o preço unitário de rosas: ");
        puRosas = ler.nextDouble();
        System.out.println("Introduza o preço unitário de tulipas: ");
        puTulipas = ler.nextDouble();

        //Processamento
        pFinal = exercicio_II_Processamento(nRosas,nTulipas,puRosas,puTulipas);

        //Saída de dados
        System.out.println("O preço final é de: " + pFinal + "€.");


        //EXERCÍCIO 3 - VOLUME CILINDRO

        //Estrutura de dados
        double raio, altura, volume;

        //Leitura de dados
        System.out.println("Introduza o raio do cilindro em metros:");
        raio = ler.nextDouble();
        System.out.println("Introduza a altura do cilindro em metros:");
        altura = ler.nextDouble();

        //Processamento
        volume = exercicio_III_Processamento(raio, altura);

        //Saída de dados
        System.out.println("O número de litros do cilindro é " + String.format("%.2f", volume) + " litros.");


        //EXERCÍCIO 4 - TROVOADA

        //Estrutura de dados
        int segundos;
        double distancia;

        //Leitura de dados
        System.out.println("Qual o intervalo de segundos entre o relâmpago e o trovão?");
        segundos = ler.nextInt();

        //Processamento
        distancia = exercicio_IV_Processamento(segundos);

        //Saída de dados
        System.out.println("A trovoada encontra-se a uma distância de " + distancia + "metros, ou seja a " + (distancia/1000) + "km.");


        //EXERCÍCIO 5 - LANÇAMENTO DE PEDRA
        //Estrutura de dados
        int segundosPedra;
        double alturaEdificio, vInicial;
        final double GRAVIDADE = 9.8;

        //Leitura de dados
        System.out.println("Qual a velocidade inicial da pedra em m/s?");
        vInicial = ler.nextDouble();
        System.out.println("Quantos segundos demora a pedra a atingir o chão?");
        segundosPedra = ler.nextInt();

        //Processamento
        alturaEdificio = exercicio_V_Processamento(segundosPedra, vInicial, GRAVIDADE);

        //Saída de dados
        System.out.println("A altura do edifício é de " + alturaEdificio + "metros.");


        //EXERCÍCIO 6 - SOMBRAS

        //Estrutura de dados
        double alturaEdificio6, alturaPessoa, sombraEdificio, sombraPessoa;

        //Leitura de dados
        System.out.println("Insira a sua altura: ");
        alturaPessoa = ler.nextDouble();
        System.out.println("Insira o comprimento da sua sombra: ");
        sombraPessoa = ler.nextDouble();
        System.out.println("Insira o comprimento da sombra do edifício: ");
        sombraEdificio = ler.nextDouble();

        //Processamento
        alturaEdificio6 = exercicio_VI_Processamento(alturaPessoa, sombraEdificio, sombraPessoa);

        //Saída de dados
        System.out.println("A altura do edifício é de " + alturaEdificio6 + " m.");


        //EXERCÍCIO 7 - MARATONA

        //Estrutura de dados
        double distManel, distZe;
        int horasManel, minManel, segManel, horasZe, minZe, segZe;

        //Leitura de dados
        System.out.println("Que distância correu o Manel?");
        distManel = ler.nextDouble();
        System.out.println("Quantas horas correu o Manel?");
        horasManel = ler.nextInt();
        System.out.println("Quantos minutos correu o Manel?");
        minManel = ler.nextInt();
        System.out.println("Quantos segundos correu o Manel?");
        segManel = ler.nextInt();
        System.out.println("Quantas horas correu o Zé?");
        horasZe = ler.nextInt();
        System.out.println("Quantos minutos correu o Zé?");
        minZe = ler.nextInt();
        System.out.println("Quantos segundos correu o Zé?");
        segZe = ler.nextInt();

        //Processamento
        distZe = exercicio_VII_Processamento(distManel, horasManel, minManel, segManel, horasZe, minZe, segZe);

        System.out.println("O Zé correu " + distZe + " km.");


        //EXERCICIO 8 - TRIGONOMETRIA

        //Estrutura de dados
        double compAB, compAC, compCB, angulo;

        //Leitura de dados
        System.out.println("Insira o comprimento AC em metros: ");
        compAC = ler.nextDouble();
        System.out.println("Insira o comprimento CB em metros: ");
        compCB = ler.nextDouble();
        System.out.println("Insira o ângulo em graus: ");
        angulo = ler.nextDouble();

        //Processamento
        compAB = exercicio_VIII_Processamento(compAC, compCB, angulo);

        //Saída de dados
        System.out.println("O comprimento do lado AB é de " + compAB + " metros.");


        //EXERCÍCIO 9 - PERÍMETRO RECTÂNGULO

        //Estrutura de dados
        double A, B, perimetro;

        //Leitura de dados
        System.out.println("Insira o lado A do rectângulo em metros: ");
        A = ler.nextDouble();
        System.out.println("Insira o lado B do rectângulo em metros: ");
        B = ler.nextDouble();

        //Processamento
        perimetro = exercicio_IX_Processamento(A, B);

        //Saída de dados
        System.out.println("O perímetro do rectângulo é de " + perimetro + " metros.");


        //EXERCÍCIO 10 - HIPOTENUSA

        //Estrutura de dados
        double c1, c2, h;

        //Leitura de dados
        System.out.println("Insira o valor do cateto 1 em metros: ");
        c1 = ler.nextDouble();
        System.out.println("Insira o valor do cateto 2 em metros: ");
        c2 = ler.nextDouble();

        //Processamento
        h = exercicio_X_Processamento(c1,c2);

        //Saída de dados
        System.out.println("O valor da hipotenusa é de " + h + " metros.");


        //EXERCÍCIO 11 - EQUAÇÃO DE 2º GRAU

        //Estrutura de dados
        double a,b,c;
        double[] x;

        //Leitura de dados
        System.out.println("Insira o valor de a: ");
        a = ler.nextDouble();
        System.out.println("Insira o valor de b: ");
        b = ler.nextDouble();
        System.out.println("Insira o valor de c: ");
        c = ler.nextDouble();

        //Processamento
        x = exercicio_XI_Processamento(a,b,c);

        //Saída de dados
        System.out.println("O valor de x é " + x[0] + " ou " + x[1]);


        //EXERCÍCIO 12 - TEMPERATURA

        //Estrutura de dados
        double celsius, fahrenheit;

        //Leitura de dados
        System.out.println("Insira a temperatura em graus Celsius: ");
        celsius = ler.nextDouble();

        //Processamento
        fahrenheit = exercicio_XII_Processamento(celsius);

        //Saída de dados
        System.out.println("A temperatura em Fahrenheit é " + fahrenheit + " graus.");


        //EXERCÍCIO 13 - MINUTOS

        //Estrutura de dados
        int H, M, totalMin;

        //Leitura de dados
        System.out.println("Insira as horas (em formato 0-24): ");
        H = ler.nextInt();
        System.out.println("Insira os minutos: ");
        M = ler.nextInt();

        //Processamento
        totalMin = exercicio_XIII_Processamento(H,M);

        //Saída de dados
        System.out.println("Desde as " + H + ":" + M + " passaram " + totalMin + " minutos.");

    }


    //FUNÇÕES DE PROCESSAMENTO DE DADOS

    public static double[] exercicio_I_Processamento (double rapazes, double raparigas) {

        double[] percentagemGenero = new double[2];
        double total = rapazes+raparigas;

        percentagemGenero[0] = (rapazes/total)*100;
        percentagemGenero[1] = (raparigas/total)*100;

        return percentagemGenero;

    }

    public static double exercicio_II_Processamento (int nRosas, int nTulipas, double puRosas, double puTulipas) {

        double pFinal = (nRosas * puRosas) + (nTulipas * puTulipas);

        return pFinal;
    }

    public static double exercicio_III_Processamento(double raio, double altura) {

        double volume;

        volume = Math.PI * raio * raio * altura * 1000;

        return volume;

    }

    public static double exercicio_IV_Processamento(double segundos) {

        double distancia;

        distancia = (1224000 * segundos)/(60 * 60);

        return distancia;

    }

    public static double exercicio_V_Processamento(int segundosPedra, double vInicial, double GRAVIDADE) {

        return (vInicial * segundosPedra) + ((GRAVIDADE * segundosPedra * segundosPedra) / 2);

    }

    public static double exercicio_VI_Processamento(double alturaPessoa, double sombraEdificio, double sombraPessoa) {

        return (sombraEdificio*alturaPessoa)/sombraPessoa;

    }

    public static double exercicio_VII_Processamento(double distManel, int horasManel, int minManel, int segManel, int horasZe, int minZe, int segZe) {

        double totalTempoManel = (horasManel*60*60) + (minManel*60) + segManel;
        double aceleracao = distManel/totalTempoManel;
        double totalTempoZe = (horasZe*60*60) + (minZe*60) + segZe;
        double distZe = (aceleracao*totalTempoZe)/1000;

        return distZe;

    }

    public static double exercicio_VIII_Processamento(double compAC, double compCB, double angulo) {

        double compAB;

        compAB = Math.sqrt(Math.pow(compCB,2)+Math.pow(compAC,2)-2*compCB*compAC*Math.cos(Math.toRadians(angulo)));

        return compAB;

    }

    public static double exercicio_IX_Processamento(double A, double B) {

        double perimetro;

        perimetro = (2*A)+(2*B);

        return perimetro;

    }

    public static double exercicio_X_Processamento(double c1, double c2) {

        double h;

        h = Math.sqrt(Math.pow(c1,2) + Math.pow(c2,2));

        return h;

    }

    public static double[] exercicio_XI_Processamento(double a, double b, double c) {

        double[] x = new double[2];

        x[0] = (-b+Math.sqrt(Math.pow(b,2)-(4*a*c)))/(2*a);
        x[1] = (-b-Math.sqrt(Math.pow(b,2)-(4*a*c)))/(2*a);

        return x;

    }

    public static double exercicio_XII_Processamento (double celsius) {

        double fahrenheit = 32+((celsius)*(9.0/5.0));

        return fahrenheit;

    }

    public static int exercicio_XIII_Processamento (int H, int M) {

        int totalMin = (H*60) + M;

        return totalMin;

    }
}


