import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SolutionTest {

    @org.junit.jupiter.api.Test
    void getMax_P2Max() {

        //Arrange
        int p1 = 5;
        int p2 = 7;
        int expected = 7;

        //Act
        int result = Solution.getMax(p1,p2);

        //Assert
        assertEquals(expected,result);

    }

    @org.junit.jupiter.api.Test
    void getMax_P1Max() {

        //Arrange
        int p1 = 7;
        int p2 = 6;
        int expected = 7;

        //Act
        int result = Solution.getMax(p1,p2);

        //Assert
        assertEquals(expected,result);

    }

    @org.junit.jupiter.api.Test
    void getMax_P1EqualsP2() {

        //Arrange
        int p1 = 7;
        int p2 = 7;
        int expected = 7;

        //Act
        int result = Solution.getMax(p1,p2);

        //Assert
        assertEquals(expected,result);

    }

    @org.junit.jupiter.api.Test
    void getMax_InvalidValues() {

        //Arrange
        int p1 = 0;
        int p2 = 6;
        int expected = -1;

        //Act
        int result = Solution.getMax(p1,p2);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getMaxDistance_ValidNumbers() {

        //Arrange
        int p1 = 7;
        int p2 = 7;
        int expected = 2;

        //Act
        int result = Solution.getMaxDistance(p1,p2);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getMaxDistance_InvalidNumbers() {

        //Arrange
        int p1 = 0;
        int p2 = 7;
        int expected = 0;

        //Act
        int result = Solution.getMaxDistance(p1,p2);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getJLimits_WithinBoundaries() {

        //Arrange
        int i = 3;
        int p2 = 7;
        int maxDistance = 2;
        int[] expected = {1,5};

        //Act
        int[] result = Solution.getJLimits(i,p2,maxDistance);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getJLimits_OutOfLeftBoundary() {

        //Arrange
        int i = 0;
        int p2 = 7;
        int maxDistance = 2;
        int[] expected = {0,2};

        //Act
        int[] result = Solution.getJLimits(i,p2,maxDistance);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getJLimits_OutOfRightBoundary() {

        //Arrange
        int i = 5;
        int p2 = 7;
        int maxDistance = 2;
        int[] expected = {3,6};

        //Act
        int[] result = Solution.getJLimits(i,p2,maxDistance);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void getWordSimilarity() {

        //Arrange
        String word_1 = "amarrar";
        String word_2 = "abarcar";
        double expected =  0.7429;

        //Act
        double result = Solution.getWordSimilarity(word_1,word_2);

        //Assert
        assertEquals(expected,result,0.0001);

    }

    @Test
    void checkIfFoundCharacterIsRepeated_EmptyAux() {

        //Arrange
        int j = 0;
        char character = 'a';
        char[] aux = {};

        //Act
        boolean result = Solution.checkIfFoundCharacterIsRepeated(j,character,aux);

        //Assert
        assertFalse(result);

    }

    @Test
    void checkIfFoundCharacterIsRepeated_NonEmptyAux() {

        //Arrange
        int j = 0;
        char character = 'a';
        char[] aux = {'a',(char)0};

        //Act
        boolean result = Solution.checkIfFoundCharacterIsRepeated(j,character,aux);

        //Assert
        assertTrue(result);

    }

    @Test
    void removeElementsFromArray_GeneralUse() {

        //Arrange
        char[] array = {'a',(char)(0),'a','r','r','a',(char)0};
        char element = (char)(0);
        char[] expected = {'a','a','r','r','a'};

        //Act
        char[] result = Solution.removeElementsFromArray(array,element);

        //Assert
        assertArrayEquals(expected,result);

    }

    @Test
    void removeElementsFromArray_NullArray() {

        //Arrange
        char[] array = null;
        char element = (char)(0);

        //Act
        char[] result = Solution.removeElementsFromArray(array,element);

        //Assert
        assertNull(result);

    }

    @Test
    void removeElementsFromArray_CountIsZero() {

        //Arrange
        char[] array = {(char)(0),(char)(0),(char)(0),(char)(0),(char)(0),(char)(0),(char)0};
        char element = (char)(0);

        //Act
        char[] result = Solution.removeElementsFromArray(array,element);

        //Assert
        assertNull(result);

    }

    @Test
    void getT_TDifferentFromZero() {

        //Arrange
        char[] correspondence_1 = {'a','a','r','a','r'};
        char[] correspondence_2 = {'a','a','r','r','a'};
        double expected = 2;

        //Act
        double result = Solution.getT(correspondence_1,correspondence_2);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getT_TEqualsZero() {

        //Arrange
        char[] correspondence_1 = {'a','a','r','r','a'};
        char[] correspondence_2 = {'a','a','r','r','a'};
        double expected = 0;

        //Act
        double result = Solution.getT(correspondence_1,correspondence_2);

        //Assert
        assertEquals(expected,result);

    }

    @Test
    void getWordSimilarityIndex_MPositive() {

        //Arrange
        int p1 = 7;
        int p2 = 7;
        int m = 5;
        int t = 1;
        double expected =  0.7429;

        //Act
        double result = Solution.getWordSimilarityIndex(p1,p2,m,t);

        //Assert
        assertEquals(expected,result,0.0001);

    }

    @Test
    void getWordSimilarityIndex_MEqualsZero() {

        //Arrange
        int p1 = 7;
        int p2 = 7;
        int m = 0;
        int t = 1;
        double expected =  0;

        //Act
        double result = Solution.getWordSimilarityIndex(p1,p2,m,t);

        //Assert
        assertEquals(expected,result,0.0001);

    }

    @Test
    void getWordSimilarityIndex_P1Zero() {

        //Arrange
        int p1 = 0;
        int p2 = 7;
        int m = 5;
        int t = 1;
        double expected =  0;

        //Act
        double result = Solution.getWordSimilarityIndex(p1,p2,m,t);

        //Assert
        assertEquals(expected,result,0.0001);

    }
}