public class Solution {


    public static double getWordSimilarity(String word_1, String word_2) {

        double wordSimilarity = 0;

        //Convert strings to lowercase
        String word_1_Lower = word_1.toLowerCase();
        String word_2_Lower = word_2.toLowerCase();

        //Convert words to char Arrays
        char[] word_1_Array = word_1_Lower.toCharArray();
        char[] word_2_Array = word_2_Lower.toCharArray();

        //Determine p1, p2 and maxDistance
        int p1 = word_1_Array.length;
        int p2 = word_2_Array.length;
        int maxDistance = getMaxDistance(p1, p2);

        //Cycle to compare the words
        char[] correspondence_1 = new char[p1];
        char[] correspondence_2 = new char[p2];
        char[] aux = new char[p2]; //array that stores the positions of word_2_Array that have been used as
        // correspondence

        int i, j, k = 0, m = 0;
        double t = 0;
        boolean found = false;

        for (i = 0; i < p1; i++) {
            found = false;
            j = getJLimits(i, p2, maxDistance)[0];
            while (j <= getJLimits(i, p2, maxDistance)[1] && !found) {
                if (word_1_Array[i] == word_2_Array[j]) {
                    if (!checkIfFoundCharacterIsRepeated(j, word_1_Array[i], aux)) {
                        aux[k] = word_1_Array[i];
                        correspondence_1[i] = word_1_Array[i];
                        correspondence_2[j] = word_2_Array[j];
                        m++;
                        k++;
                        found = true; //if it finds a correspondence it doesn't have to iterate through the next j's
                    } else {
                        correspondence_1[i] = (char) 0;
                        correspondence_2[j] = (char) 0;
                        aux[k] = (char) 0;
                        k++;
                        j++;
                    }
                }
            }
        }

        //Determine t
        t = getT(correspondence_1, correspondence_2);

        wordSimilarity =

                getWordSimilarityIndex(p1, p2, m, t);

        return wordSimilarity;

    }


    public static int getMaxDistance(int p1, int p2) {

        int maxDistance = 0;

        if (p1 != 0 && p2 != 0) {
            if (p1 != 0 && p2 != 0) {
                maxDistance = (getMax(p1, p2) / 2) - 1;
            }
        }

        return maxDistance;

    }

    public static int getMax(int p1, int p2) {

        int max = -1; //impossible value

        if (p1 != 0 && p2 != 0) {
            max = p1;

            if (p1 != 0 && p2 != 0) {
                if (p2 > p1) {
                    max = p2;
                }
            }

        }

        return max;

    }

    public static int[] getJLimits(int i, int p2, int maxDistance) {

        int[] jLimits = new int[2];

        jLimits[0] = i - maxDistance;
        jLimits[1] = i + maxDistance;

        if (jLimits[0] < 0) {
            jLimits[0] = 0;
        }

        if (jLimits[1] >= p2) {
            jLimits[1] = p2 - 1;
        }

        return jLimits;

    }

    public static boolean checkIfFoundCharacterIsRepeated(int j, char character, char[] aux) {

        boolean repeated = false;
        int i;

        for (i = 0; i < aux.length; i++) {
            if (character == aux[i] && j == i) {
                repeated = true;
            }

        }

        return repeated;
    }

    public static double getT(char[] correspondence_1, char[] correspondence_2) {

        int i, j;
        double t = 0;
        char[] correspondence_1Clean = removeElementsFromArray(correspondence_1, (char) 0);
        char[] correspondence_2Clean = removeElementsFromArray(correspondence_2, (char) 0);

        for (i = 0; i < correspondence_1.length; i++) {
            if (correspondence_1Clean[i] != correspondence_2Clean[i]) {
                t += 1;
            }
        }

        return (t / 2);

    }

    public static char[] removeElementsFromArray(char[] array, char element) {

        char[] reducedArray = null;

        if (array != null) {
            int i, j = 0, count = 0;

            //Find the length of the reduced array
            for (i = 0; i < array.length; i++) {
                if (array[i] != element) {
                    count += 1;
                }
            }

            if (count != 0) {
                //Fill in the reduced array
                reducedArray = new char[count];

                for (i = 0; i < array.length; i++) {
                    if (array[i] != 0) {
                        reducedArray[j] = array[i];
                        j++;
                    }
                }
            }

        }

        return reducedArray;

    }

    public static double getWordSimilarityIndex(int p1, int p2, int m, double t) {

        double wordSimilarity = 0;

        if (p1 != 0 && p2 != 0) {
            //Determine word similarity
            if (m == 0) {
                wordSimilarity = 0;
            } else {
                wordSimilarity =
                        (1.0 / 3.0) * (double) (((double) m / (double) p1) + ((double) m / (double) p2) + (((double) m - t) / (double) m));
            }
        }

        return wordSimilarity;

    }

}
